#!/usr/bin/env python
from flask import Flask, request, abort, jsonify, render_template, url_for, redirect, flash, session 
from functions.readAssetFunction import *
from functions.mapAssetFunction import *
from functions.readTenantFunction import *
from functions.mapTenantFunction import *
import requests
import jwt
import re
import socket
import json
import os

# For testing purposes this is the keypair which will sign and validate the ipd response
#private_key = open('pseudo_prov_priv.pem').read()
#idp_public_key = open('pub_idp.pem').read()

# Flask app parameters
app = Flask(__name__)
#app = Flask(__name__, template_folder='templates')

#@app.route('/readAsset', methods=['POST'])
#def readAssetRoute():
#    datasetId = request.get_json().get("datasetId")
#    contractAddress = findContractAddress(datasetId)
#    return str(contractAddress)

@app.route('/mapAsset', methods=['POST'])
def mapAssetRoute():
    datasetId = request.get_json().get("datasetId")
    contractAddress = request.get_json().get("contractAddress")
    mapAssetMessage = mapAssetToDB(datasetId, contractAddress)
    return str(True)

@app.route('/readTenant', methods=['POST'])
def readTenantRoute():
    clientId = request.get_json().get("clientId")
    contractAddress = findContractAddressTenant(clientId)
    if len(contractAddress):
        return str(contractAddress[0])
    else:
        return str("Empty")

@app.route('/mapTenant', methods=['POST'])
def mapTenantRoute():
    clientId = request.get_json().get("clientId")
    contractAddress = request.get_json().get("contractAddress")
    mapAssetMessage = mapTenantToDB(clientId, contractAddress)
    return str(True)


if __name__ == '__main__':
    print("Listening...")

# This is the secret key of our flask application
    app.secret_key = 'datavaults'
    app.run(debug=True, host='0.0.0.0', port=8085)
