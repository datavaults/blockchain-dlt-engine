from .db import mongoConnector 
#from pymongo import MongoClient
from bson.objectid import ObjectId
import pprint
import requests
import jwt
import re
import socket
import json
import os
import base64

mongo = mongoConnector.client
db = mongo.my_database

def mapAssetToDB(datasetId, contractAddress): 
    datasetlist = db.datasetlist
    dataInput = {datasetId:contractAddress}
    dataInputId = datasetlist.insert_one(dataInput).inserted_id
    print (dataInput)
    print (dataInputId)
    return dataInputId
