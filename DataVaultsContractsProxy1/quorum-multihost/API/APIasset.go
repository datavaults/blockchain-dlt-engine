/*
	@Author: George Misiakoulis (Ubitech)
*/
package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"runtime"
	"time"
	"fmt"
	"log"

	//"github.com/davecgh/go-spew/spew"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
    	"github.com/ethereum/go-ethereum/common"
    	"github.com/ethereum/go-ethereum/rpc"
)

var (
	port       string = "8081"
	host       string = "localhost"
	caCert     string = "./CA/CAcert.pem"
	serverCert string = "./TLS/server/serverCert.pem"
	serverKey  string = "./TLS/server/serverUnecryptedKey.pem"

	// Hydra
	tokenURL                 string = "https://localhost:4444/oauth2/token"
	RootCertificateHydraPath string = "../Hydra/tls/cert.pem"

	// GoQuorum Proxy
	goQuorumNodeURL                 string = "https://192.168.3.127:22000/?PSI="
	RootCertificateGoQuorumNodePath string = "/home/ubuntu/nginx/client/CAcert.pem"
	ClientCertGoQuorumNodePath      string = "/home/ubuntu/nginx/client/clientCert.pem"
	ClientKeyGoQuorumNodePath       string = "/home/ubuntu/nginx/client/clientsKey.pem"

	ethKeyPath  string = "../nodes/keystore"
	keyPassword string = "node1"

	// Tessera Proxy
	tesseraThirdPartyEndpoint     string = "https://192.168.3.105:9081"
	RootCertificateThirdPartyPath string = "/home/ubuntu/nginx-tessera/client/CAcert.pem"
	ClientCertThirdPartyPath      string = "/home/ubuntu/nginx-tessera/client/clientCert.pem"
	ClientKeyThirdPartyPath       string = "/home/ubuntu/nginx-tessera/client/clientsKey.pem"

	// Public Contract Addresses
	assetContractAddr string = "0x1b0C81BF44A91F0ca9E267497D2F9b9a2A7839E7" //Asset Public Contract Address
)

//////////////////////////////////////////////////////////////////////////////////////////////////
type ClientDeployReadAll struct {
	ClientId     	string
	ClientSecret 	string
}

type ClientCreate struct {
        ClientId        string
        ClientSecret    string
        DataInput       AssetAsset_Configuration_Public
}

type ClientRead struct {
	ClientId     	string
	ClientSecret 	string
	DataInput	string
}
/*
func deployAsset(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
			w.Write([]byte(body))
		}

		var client ClientDeployReadAll
	    	dec := json.NewDecoder(strings.NewReader(string(body)))
	    	err = dec.Decode(&client)
	    	if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}
		clientId := client.ClientId
		clientSecret := client.ClientSecret

		if clientId == "" {
			w.Write([]byte("ClientId should not be empty."))
			return
		}

		if clientSecret == "" {
			w.Write([]byte("ClientSecret should not be empty."))
			return
		}

		// Connect to goQuorum Node
    		goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, ClientCertGoQuorumNodePath, ClientKeyGoQuorumNodePath, RootCertificateGoQuorumNodePath)
    		// Refresh Token
    		var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    			var token string
    			token, err = requestHydraToken(tokenURL, clientId, clientSecret, RootCertificateHydraPath)
    			if err != nil {
    	    			w.Write([]byte(err.Error()))
    			}
    			return token, nil
    		}

    		// Attach token to the headers
    		authenticatedClient := goQuorumClient.WithHTTPCredentials(f)

    		tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)

    		trOpts := initPrivateTransaction(ethKeyPath, keyPassword)

    		trOpts.GasLimit = 47000000

    		publicAddr, _, _, err := DeployAsset(trOpts, tesseraClient)

    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

		w.Write([]byte("Contract Address is: " + publicAddr.Hex()))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}
*/
/*
func createAsset(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientCreate
	   	dec := json.NewDecoder(strings.NewReader(string(body)))
	    	err = dec.Decode(&client)
	    	if err != nil {
    			println(err.Error())
    		}
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		contractAddr := client.ContractAddress
		input := client.DataInput //check the input

		//spew.Dump(input)

		// Connect to goQuorum Node
    		goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, ClientCertGoQuorumNodePath, ClientKeyGoQuorumNodePath, RootCertificateGoQuorumNodePath)
    		// Refresh Token
    		var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    			var token string
    			token, err = requestHydraToken(tokenURL, clientId, clientSecret, RootCertificateHydraPath)
    			if err != nil {
    	    			w.Write([]byte(err.Error()))
    			}
    			return token, nil
    		}

    		// Attach token to the headers
    		authenticatedClient := goQuorumClient.WithHTTPCredentials(f)

    		tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)

    		trOpts := initPrivateTransaction(ethKeyPath, keyPassword)

    		trOpts.GasLimit = 47000000

    		asset, err := NewAsset(common.HexToAddress(contractAddr), tesseraClient)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}
    		isAssetExists, err := asset.AssetCaller.IsExists(&bind.CallOpts{}, string(input.DatasetID))
    		//spew.Dump(isAssetExists)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

    		if(isAssetExists){
    			w.Write([]byte("DatasetID is already exists."))
    			return
    		}

    		_, err = asset.AssetTransactor.Create(trOpts, input)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}
		w.Write([]byte("A new record is successfully added."))

	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}
*/

func readAsset(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientRead
	    	dec := json.NewDecoder(strings.NewReader(string(body)))
	    	err = dec.Decode(&client)
	    	if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		input := client.DataInput

		contractAddr := assetContractAddr

		if clientId == "" {
			w.Write([]byte("ClientId should not be empty."))
			return
		}

		if clientSecret == "" {
			w.Write([]byte("ClientSecret should not be empty."))
			return
		}

		if input == "" {
    			w.Write([]byte("DataInput must NOT be empty."))
    			return
    		}

		// Connect to goQuorum Node
    		goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, ClientCertGoQuorumNodePath, ClientKeyGoQuorumNodePath, RootCertificateGoQuorumNodePath)
    		// Refresh Token
    		var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    			var token string
    			token, err = requestHydraToken(tokenURL, clientId, clientSecret, RootCertificateHydraPath)
    			if err != nil {
    	    			w.Write([]byte(err.Error()))
    			}
    			return token, nil
    		}

    		// Attach token to the headers
    		authenticatedClient := goQuorumClient.WithHTTPCredentials(f)

    		tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)

    		asset, err := NewAsset(common.HexToAddress(contractAddr), tesseraClient)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

    		isAssetExists, err := asset.AssetCaller.IsExists(&bind.CallOpts{}, string(input))
    		//spew.Dump(isAssetExists)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

    		if(!isAssetExists){
    			w.Write([]byte("There are no structs available."))
    			return
    		}

    		readAsset, err := asset.AssetCaller.Read(&bind.CallOpts{}, string(input)) 
    		if err != nil {
    			println(err.Error())
    	   		w.Write([]byte(err.Error()))
    		}
    		output, err := json.Marshal(readAsset)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}
		w.Write([]byte(output))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

func readAllAssets(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientDeployReadAll
	    	dec := json.NewDecoder(strings.NewReader(string(body)))
	    	err = dec.Decode(&client)
	    	if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

		clientId := client.ClientId
		clientSecret := client.ClientSecret
		contractAddr := assetContractAddr

		if clientId == "" {
			w.Write([]byte("ClientId should not be empty."))
			return
		}

		if clientSecret == "" {
			w.Write([]byte("ClientSecret should not be empty."))
			return
		}

		// Connect to goQuorum Node
    		goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, ClientCertGoQuorumNodePath, ClientKeyGoQuorumNodePath, RootCertificateGoQuorumNodePath)
    		// Refresh Token
    		var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    			var token string
    			token, err = requestHydraToken(tokenURL, clientId, clientSecret, RootCertificateHydraPath)
    			if err != nil {
    	    			w.Write([]byte(err.Error()))
    			}
    			return token, nil
    		}

    		// Attach token to the headers
    		authenticatedClient := goQuorumClient.WithHTTPCredentials(f)

	    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)

    		asset, err := NewAsset(common.HexToAddress(contractAddr), tesseraClient)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

    		readAllAssets, err := asset.AssetCaller.ReadAll(&bind.CallOpts{})
    		if err != nil {
    			println(err.Error())
    	    		w.Write([]byte(err.Error()))
    		}

    		output, err := json.Marshal(readAllAssets)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}
		w.Write([]byte(output))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Main function /////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
func main() {
    log.SetFlags(log.Lshortfile)
    maxProcs := runtime.NumCPU()
    runtime.GOMAXPROCS(maxProcs)

    server := &http.Server{
		Addr:         ":" + port,
		ReadTimeout:  5 * time.Minute,
		WriteTimeout: 10 * time.Second,
		TLSConfig:    getTLSConfig(host, caCert),
	}

	//http.HandleFunc("/deployAsset", deployAsset)
	//http.HandleFunc("/createAsset", createAsset)
	http.HandleFunc("/readAsset", readAsset)
	http.HandleFunc("/readAllAssets", readAllAssets)

	log.Printf("Starting HTTPS server on host %s and port %s", host, port)
	if err := server.ListenAndServeTLS(serverCert, serverKey); err != nil {
		log.Fatal(err)
	}
}
