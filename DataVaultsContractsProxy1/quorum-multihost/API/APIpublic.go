/*
	@Author: George Misiakoulis (Ubitech)
*/
package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"runtime"
	"time"
	"fmt"
	"log"
	

	//"github.com/davecgh/go-spew/spew"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
        "github.com/ethereum/go-ethereum/common"
        "github.com/ethereum/go-ethereum/rpc"
)

var (
	port string = "8081"
	host string = "localhost"
	caCert string = "./CA/CAcert.pem"
	serverCert string = "./TLS/server/serverCert.pem"
	serverKey string = "./TLS/server/serverUnecryptedKey.pem"

        goQuorumNodeURL string = "https://192.168.3.162:22000/?PSI="
        tokenURL string = "https://hydra:4444/oauth2/token"
        audience string = "Node1"

        tesseraThirdPartyEndpoint string = "https://192.168.3.105:9081"
	
	RootCertificateGoQuorumNodePath string = "../nodes/node1/tls/cert.pem"
	RootCertificateHydraPath string = "../Hydra/tls/cert.pem"
	
	ethKeyPath string = "../nodes/node1/keystore"
	keyPassword string = "node1"
	
	RootCertificateThirdPartyPath string = "/home/ubuntu/nginx/client/CAcert.pem"
	ClientCertThirdPartyPath      string = "/home/ubuntu/nginx/client/clientCert.pem"
	ClientKeyThirdPartyPath       string = "/home/ubuntu/nginx/client/clientsKey.pem"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
type ClientDeployCreate struct {
	ClientId     	string
	ClientSecret 	string
	ContractAddress string
	DataInput	AssetAsset_Configuration_Public
}

type ClientRead struct {
	ClientId     	string
	ClientSecret 	string
	ContractAddress string
	DataInput		string
}

func deployAsset(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientDeployCreate
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	} 
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		
	// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)
    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    
    	trOpts := initPrivateTransaction(ethKeyPath, keyPassword)

    	trOpts.GasLimit = 47000000
    	
    	publicAddr, _, _, err := DeployAsset(trOpts, tesseraClient)

    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
		
		w.Write([]byte("Contract Address is: " + publicAddr.Hex()))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

func createAsset(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
	     body, err := ioutil.ReadAll(r.Body)
	     defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientDeployCreate
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    	    } 
	clientId := client.ClientId
	clientSecret := client.ClientSecret
	contractAddr := client.ContractAddress
	input := client.DataInput //check the input
	    
	//spew.Dump(input)
		
	// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)
    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    	
    	trOpts := initPrivateTransaction(ethKeyPath, keyPassword)
    	trOpts.GasLimit = 47000000

    	asset, err := NewAsset(common.HexToAddress(contractAddr), tesseraClient)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	isAssetExists, err := asset.AssetCaller.IsExists(&bind.CallOpts{}, string(input.DatasetID))
    	//spew.Dump(isAssetExists)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	
    	if(isAssetExists){
    		w.Write([]byte("DatasetID is already exists."))
    		return
    	} 
   
    	_, err = asset.AssetTransactor.Create(trOpts, input)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
		w.Write([]byte("A new record is successfully added."))
		
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

func readAsset(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientRead
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	} 
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		contractAddr := client.ContractAddress
		input := client.DataInput
		if input == "" {
    		w.Write([]byte("DataInput must NOT be empty."))
    		return
    	}
		//spew.Dump(input)

		// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)
    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    	
    	asset, err := NewAsset(common.HexToAddress(contractAddr), tesseraClient)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	
    	isAssetExists, err := asset.AssetCaller.IsExists(&bind.CallOpts{}, string(input))
    	//spew.Dump(isAssetExists)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	
    	if(!isAssetExists){
    		w.Write([]byte("There are no structs available."))
    		return
    	}
    	
    	readAsset, err := asset.AssetCaller.Read(&bind.CallOpts{}, string(input)) 
    	if err != nil {
    		println(err.Error())
    	   	w.Write([]byte(err.Error()))
    	}
    	output, err := json.Marshal(readAsset)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
		w.Write([]byte(output))
		
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

func readAllAssets(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientRead
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	if client.DataInput != "" {
    		w.Write([]byte("DataInput must be empty."))
    		return
    	}
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		contractAddr := client.ContractAddress

		// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)
    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    	
    	asset, err := NewAsset(common.HexToAddress(contractAddr), tesseraClient)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}

    	readAllAssets, err := asset.AssetCaller.ReadAll(&bind.CallOpts{}) 
    	if err != nil {
    		println(err.Error())
    	    w.Write([]byte(err.Error()))
    	}

    	output, err := json.Marshal(readAllAssets)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
		w.Write([]byte(output))
			
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////
// Main function
func main() {
    log.SetFlags(log.Lshortfile)
    maxProcs := runtime.NumCPU()
    runtime.GOMAXPROCS(maxProcs)
    
    server := &http.Server{
		Addr:         ":" + port,
		ReadTimeout:  5 * time.Minute,
		WriteTimeout: 10 * time.Second,
		TLSConfig:    getTLSConfig(host, caCert),
	}

	http.HandleFunc("/deploy", deployAsset)
	http.HandleFunc("/create", createAsset)
	http.HandleFunc("/read", readAsset)
	http.HandleFunc("/readAll", readAllAssets)

	log.Printf("Starting HTTPS server on host %s and port %s", host, port)
	if err := server.ListenAndServeTLS(serverCert, serverKey); err != nil {
		log.Fatal(err)
	}
}
