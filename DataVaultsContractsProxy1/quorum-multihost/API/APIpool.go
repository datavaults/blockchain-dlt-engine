/*
	@Author: George Misiakoulis (Ubitech)
*/
package main

import (
	"encoding/json"
	"encoding/hex"
	"io/ioutil"
	"net/http"
	"context"
	"strings"
	"runtime"
	"time"
	"fmt"
	"log"


	//"github.com/davecgh/go-spew/spew"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
        "github.com/ethereum/go-ethereum/rpc"
        "github.com/ethereum/go-ethereum/crypto"
)

var (
	port       string = "8082"
	host       string = "localhost"
	caCert     string = "./CA/CAcert.pem"
	serverCert string = "./TLS/server/serverCert.pem"
	serverKey  string = "./TLS/server/serverUnecryptedKey.pem"

	// Hydra
        tokenURL                 string = "https://localhost:4444/oauth2/token"
	RootCertificateHydraPath string = "../Hydra/tls/cert.pem"

	// GoQuorum Proxy
	goQuorumNodeURL                 string = "https://192.168.3.127:22000/?PSI="
	RootCertificateGoQuorumNodePath string = "/home/ubuntu/nginx/client/CAcert.pem"
	ClientCertGoQuorumNodePath      string = "/home/ubuntu/nginx/client/clientCert.pem"
	ClientKeyGoQuorumNodePath       string = "/home/ubuntu/nginx/client/clientsKey.pem"

	ethKeyPath  string = "../nodes/keystore"
	keyPassword string = "node1"

	// Tessera Proxy
	tesseraThirdPartyEndpoint     string = "https://192.168.3.105:9081"
	RootCertificateThirdPartyPath string = "/home/ubuntu/nginx-tessera/client/CAcert.pem"
	ClientCertThirdPartyPath      string = "/home/ubuntu/nginx-tessera/client/clientCert.pem"
	ClientKeyThirdPartyPath       string = "/home/ubuntu/nginx-tessera/client/clientsKey.pem"

	// Public Contract Addresses
	poolContractAddr  string = "0x6C10986ed4607ee047c74467696e9b47F6585476"  //Pool Public Contract Address
)

//////////////////////////////////////////////////////////////////////////////////////////////////
type ClientCreate struct {
	ClientId     	string
	ClientSecret 	string
	DataInput	string
}

type ClientDeployRead struct {
        ClientId        string
        ClientSecret    string
}

/*
func deployPool(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientDeployRead
	    	dec := json.NewDecoder(strings.NewReader(string(body)))
	    	err = dec.Decode(&client)
	    	if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}
		clientId := client.ClientId
		clientSecret := client.ClientSecret

		if clientId == "" {
			w.Write([]byte("ClientId should not be empty."))
			return
		}

		if clientSecret == "" {
			w.Write([]byte("ClientSecret should not be empty."))
			return
		}

		// Connect to goQuorum Node
    		goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, ClientCertGoQuorumNodePath, ClientKeyGoQuorumNodePath, RootCertificateGoQuorumNodePath)
    		// Refresh Token
    		var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    			var token string
    			token, err = requestHydraToken(tokenURL, clientId, clientSecret, RootCertificateHydraPath)
    			if err != nil {
    	    			w.Write([]byte(err.Error()))
    			}
    			return token, nil
    		}

    		// Attach token to the headers
    		authenticatedClient := goQuorumClient.WithHTTPCredentials(f)

    		tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)

    		trOpts := initPrivateTransaction(ethKeyPath, keyPassword)

    		trOpts.GasLimit = 47000000

    		publicAddr, _, _, err := DeployPublic(trOpts, tesseraClient)

    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

		w.Write([]byte("Contract Address is: " + publicAddr.Hex()))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}
*/
func createPool(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientCreate
	    	dec := json.NewDecoder(strings.NewReader(string(body)))
	    	err = dec.Decode(&client)
	    	if err != nil {
    			println(err.Error())
    		}
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		publicKey := client.DataInput

		contractAddr := poolContractAddr

		if clientId == "" {
			w.Write([]byte("ClientId should not be empty."))
			return
		}

		if clientSecret == "" {
			w.Write([]byte("ClientSecret should not be empty."))
			return
		}

        	if publicKey == "" {
    			w.Write([]byte("DataInput must NOT be empty."))
    			return
    		}

		// Connect to goQuorum Node
    		goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, ClientCertGoQuorumNodePath, ClientKeyGoQuorumNodePath, RootCertificateGoQuorumNodePath)
    		// Refresh Token
    		var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    			var token string
    			token, err = requestHydraToken(tokenURL, clientId, clientSecret, RootCertificateHydraPath)
    			if err != nil {
    	    			w.Write([]byte(err.Error()))
    			}
    			return token, nil
    		}

    		// Attach token to the headers
    		authenticatedClient := goQuorumClient.WithHTTPCredentials(f)

    		tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)

    		trOpts := initPrivateTransaction(ethKeyPath, keyPassword)

    		trOpts.GasLimit = 47000000

    		pool, err := NewPublic(common.HexToAddress(contractAddr), tesseraClient)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

        	hashedClientId := crypto.Keccak512([]byte(client.ClientId))
        	iv := make([]byte, 16)
        	copy(iv, hashedClientId)

    		encryptedHashedClientId, err := encryptAES([]byte(clientSecret), []byte(iv),[]byte(hashedClientId))
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
	    	}

    		isClientIdExists, err := pool.PublicCaller.IsExists(&bind.CallOpts{}, hex.EncodeToString(encryptedHashedClientId))
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

    		if(isClientIdExists){
    			w.Write([]byte("ClientId is already exists."))
    			return
    		}

    		encryptedPublicKey, err := encryptAES([]byte(clientSecret), []byte(iv), []byte(publicKey))
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
	    	}

    		input := PublicKeys{
    	    		EncryptedHashedClientID: hex.EncodeToString(encryptedHashedClientId),
	        	EncryptedPublicKey: hex.EncodeToString(encryptedPublicKey),
    		}

    		_, err = pool.PublicTransactor.Create(trOpts, input)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}
		w.Write([]byte("A new record is successfully added."))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

func readPool(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientDeployRead
	    	dec := json.NewDecoder(strings.NewReader(string(body)))
	    	err = dec.Decode(&client)
	    	if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		contractAddr := poolContractAddr

		if clientId == "" {
                        w.Write([]byte("ClientId should not be empty."))
                        return
                }

                if clientSecret == "" {
                        w.Write([]byte("ClientSecret should not be empty."))
                        return
                }

		// Connect to goQuorum Node
    		goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, ClientCertGoQuorumNodePath, ClientKeyGoQuorumNodePath, RootCertificateGoQuorumNodePath)
    		// Refresh Token
    		var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    			var token string
    			token, err = requestHydraToken(tokenURL, clientId, clientSecret, RootCertificateHydraPath)
    			if err != nil {
    	    			w.Write([]byte(err.Error()))
    			}
    			return token, nil
    		}

    		// Attach token to the headers
    		authenticatedClient := goQuorumClient.WithHTTPCredentials(f)

    		tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)

    		pool, err := NewPublic(common.HexToAddress(contractAddr), tesseraClient)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

    		hashedClientId := crypto.Keccak512([]byte(client.ClientId))
        	iv := make([]byte, 16)
        	copy(iv, hashedClientId)

    		encryptedHashedClientId, err := encryptAES([]byte(clientSecret), []byte(iv),[]byte(hashedClientId))
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
	    	}

    		isClientIdExists, err := pool.PublicCaller.IsExists(&bind.CallOpts{}, hex.EncodeToString(encryptedHashedClientId))
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

    		if(!isClientIdExists){
    			w.Write([]byte("There are no structs available."))
    			return
    		}

    		readKeyFromPool, err := pool.PublicCaller.Read(&bind.CallOpts{}, hex.EncodeToString(encryptedHashedClientId)) 
    		if err != nil {
    			println(err.Error())
    	   		w.Write([]byte(err.Error()))
    		}

 	   	output, err := json.Marshal(readKeyFromPool)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

    		var publicKey PublicKeys
    		err = json.Unmarshal([]byte(output), &publicKey)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    		}

    		encryptedPublicKey, err := hex.DecodeString(publicKey.EncryptedPublicKey)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
	    	}

    		decryptedPublicKey, err := decryptAES([]byte(clientSecret), []byte(iv), []byte(encryptedPublicKey))
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
	    	}

		w.Write([]byte(decryptedPublicKey))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// Main function ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
func main() {
    log.SetFlags(log.Lshortfile)
    maxProcs := runtime.NumCPU()
    runtime.GOMAXPROCS(maxProcs)

    server := &http.Server{
	Addr:         ":" + port,
	ReadTimeout:  5 * time.Minute,
	WriteTimeout: 10 * time.Second,
	TLSConfig:    getTLSConfig(host, caCert),
    }

    //http.HandleFunc("/deployPool", deployPool)
    http.HandleFunc("/createPool", createPool)
    http.HandleFunc("/readPool", readPool)

    log.Printf("Starting HTTPS server on host %s and port %s", host, port)
    if err := server.ListenAndServeTLS(serverCert, serverKey); err != nil {
	log.Fatal(err)
    }
}
