/*
	@Author: George Misiakoulis (Ubitech)
*/
package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"context"
	"strings"
	"runtime"
	"time"
	"bytes"
	"fmt"
	"log"

	"github.com/davecgh/go-spew/spew"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
        "github.com/ethereum/go-ethereum/common"
        "github.com/ethereum/go-ethereum/rpc"
)

var (
	port       string = "8080"
	host       string = "localhost"
	caCert     string = "./CA/CAcert.pem"
	serverCert string = "./TLS/server/serverCert.pem"
	serverKey  string = "./TLS/server/serverUnecryptedKey.pem"

	// Hydra
	tokenURL                 string = "https://localhost:4444/oauth2/token"
	RootCertificateHydraPath string = "../Hydra/tls/cert.pem"

	// GoQuorum Proxy
	goQuorumNodeURL                 string = "https://192.168.3.127:22000/?PSI="
	RootCertificateGoQuorumNodePath string = "/home/ubuntu/nginx/client/CAcert.pem"
	ClientCertGoQuorumNodePath      string = "/home/ubuntu/nginx/client/clientCert.pem"
	ClientKeyGoQuorumNodePath       string = "/home/ubuntu/nginx/client/clientsKey.pem"

	ethKeyPath  string = "../nodes/keystore"
	keyPassword string = "node1"

	// Tessera Proxy
	tesseraThirdPartyEndpoint     string = "https://192.168.3.105:9081"
	RootCertificateThirdPartyPath string = "/home/ubuntu/nginx-tessera/client/CAcert.pem"
	ClientCertThirdPartyPath      string = "/home/ubuntu/nginx-tessera/client/clientCert.pem"
	ClientKeyThirdPartyPath       string = "/home/ubuntu/nginx-tessera/client/clientsKey.pem"

	// Public Contract Addresses
	poolContractAddr  string = "0x6C10986ed4607ee047c74467696e9b47F6585476"  //Pool Public Contract Address
	assetContractAddr string = "0x1b0C81BF44A91F0ca9E267497D2F9b9a2A7839E7" //Asset Public Contract Address

	// Tecnalia's Public Key
	tecnaliaPublicKey string = "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEO8XJYmck5uAWQ7DaKMcXmT214avdN4kueqjzHyqDARXKQcnlRXgkzzxUVYJrIKHPxvutUn6oOsYIhX4dD6V8FA=="
)

//////////////////////////////////////////////////////////////////////////////////////////////////
type ClientDeploy struct {
	ClientId        string
	ClientSecret    string
}

type ClientCreate struct {
	ClientId     	string
	ClientSecret 	string
	DataInput	TenantData_Sharing_Configuration
}

type ClientRead struct {
	ClientId     	string
	ClientSecret 	string
	DataInput	string
}

//Deploy a Tenant's private smart contract
func deployTenant(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    	w.Write([]byte(body))
		    	return
		}

		var client ClientDeploy
	    	dec := json.NewDecoder(strings.NewReader(string(body)))
	    	err = dec.Decode(&client)
	    	if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}
		clientId := client.ClientId
		clientSecret := client.ClientSecret

		if clientId == "" {
			w.Write([]byte("ClientId should not be empty."))
			return
		}
		if clientSecret == "" {
			w.Write([]byte("ClientSecret should not be empty."))
			return
		}

                // Read Tenant
                requestBody, err := json.Marshal(map[string]string{
                        "clientId": clientId,
                })

                if err != nil {
                        w.Write([]byte(err.Error()))
                        return
                }

                httpClient := http.Client {
                        Timeout: time.Duration(5 * time.Second),
                }

                request, err := http.NewRequest("POST", "http://localhost:8085/readTenant", bytes.NewBuffer(requestBody))
                request.Header.Set("Content-Type", "application/json")
                if err != nil {
                        w.Write([]byte(err.Error()))
                        return
                }

                resp, err := httpClient.Do(request)
                if err != nil {
                        w.Write([]byte(err.Error()))
                        return
                }
                defer resp.Body.Close()

                respBody, err := ioutil.ReadAll(resp.Body)
                if err != nil {
                        w.Write([]byte(err.Error()))
                        return
                }
                contractAddr := string(respBody)
                if contractAddr != "Empty" {
                        w.Write([]byte("There is already a contract deployed for this Tenant."))
                        return
                }

		// Connect to goQuorum Node
    		goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, ClientCertGoQuorumNodePath, ClientKeyGoQuorumNodePath, RootCertificateGoQuorumNodePath)
    		// Refresh Token
    		var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    			var token string
    			token, err = requestHydraToken(tokenURL, clientId, clientSecret, RootCertificateHydraPath)
    			if err != nil {
    	    			w.Write([]byte(err.Error()))
    			}
    			return token, nil
    		}

    		// Attach token to the headers
    		authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    		defer authenticatedClient.Close()

    		tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)

    		//Connect to Public Key Pool to Retrieve Tenant's Private Key
    		publicKey, err := retrievePublicKey(tesseraClient, poolContractAddr, clientId, clientSecret)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}

    		trOpts := initPrivateTransaction(ethKeyPath, keyPassword)
    		trOpts.GasLimit = 47000000
    		trOpts.PrivateFor = []string{string(publicKey),string(tecnaliaPublicKey)}
    		trOpts.PrivateFrom = string(publicKey)
    		spew.Dump(trOpts)

    		privateAddr, _, _, err := DeployTenant(trOpts, tesseraClient)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}

		// Map Tenant
		requestBody, err = json.Marshal(map[string]string{
			"clientId": clientId,
			"contractAddress": privateAddr.Hex(),
		})

		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}

		httpClient = http.Client {
			Timeout: time.Duration(5 * time.Second),
		}
		request, err = http.NewRequest("POST", "http://localhost:8085/mapTenant", bytes.NewBuffer(requestBody))
		request.Header.Set("Content-Type", "application/json")

		if err != nil {
			 w.Write([]byte(err.Error()))
			 return
		}

		resp, err = httpClient.Do(request)
		if err != nil {
			 w.Write([]byte(err.Error()))
			 return
		}
		defer resp.Body.Close()

		respBody, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}
		ok := string(respBody)
		if ok != "True" {
			w.Write([]byte("An error has occurred during Tenant Mapping"))
			return
		}

		w.Write([]byte("Contract Address is: " + privateAddr.Hex()))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

//Create a private transaction with the Tenant's private contract
func createTenant(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    	w.Write([]byte(body))
		    	return
		}

		var client ClientCreate
	    	dec := json.NewDecoder(strings.NewReader(string(body)))
	    	err = dec.Decode(&client)
	    	if err != nil {
    			println(err.Error())
    			return
    		}
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		input := client.DataInput

		if clientId == "" {
			w.Write([]byte("ClientId should not be empty."))
			return
		}
		if clientSecret == "" {
			w.Write([]byte("ClientSecret should not be empty."))
			return
		}

		// Read Tenant
		requestBody, err := json.Marshal(map[string]string{
	                "clientId": clientId,
		})

                if err != nil {
			w.Write([]byte(err.Error()))
			return
		}

		httpClient := http.Client {
			Timeout: time.Duration(5 * time.Second),
		}

		request, err := http.NewRequest("POST", "http://localhost:8085/readTenant", bytes.NewBuffer(requestBody))
                request.Header.Set("Content-Type", "application/json")
		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}

		resp, err := httpClient.Do(request)
		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}
		defer resp.Body.Close()

		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}
		contractAddr := string(respBody)
		if contractAddr == "Empty" {
			w.Write([]byte("There is no any contract for this Tenant."))
			return
		}

		// Connect to goQuorum Node
    		goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, ClientCertGoQuorumNodePath, ClientKeyGoQuorumNodePath, RootCertificateGoQuorumNodePath)
    		defer goQuorumClient.Close()
    		// Refresh Token
    		var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    			var token string
    			token, err = requestHydraToken(tokenURL, clientId, clientSecret, RootCertificateHydraPath)
    			if err != nil {
    	    			w.Write([]byte(err.Error()))
    			}
    			return token, nil
    		}

    		// Attach token to the headers
    		authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    		defer authenticatedClient.Close()

    		tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)

    		//Connect to Public Key Pool to Retrieve Tenant's Private Key
    		publicKey, err := retrievePublicKey(tesseraClient, poolContractAddr, clientId, clientSecret)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}

    		trOpts := initPrivateTransaction(ethKeyPath, keyPassword)
    		trOpts.GasLimit = 47000000
    		trOpts.PrivateFor = []string{string(publicKey),string(tecnaliaPublicKey)}
    		trOpts.PrivateFrom = string(publicKey)

    		tenant, err := NewTenant(common.HexToAddress(contractAddr), tesseraClient)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}

    		isDatasetIDExists, err := tenant.TenantCaller.IsExists(&bind.CallOpts{}, string(input.DatasetID))
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}

    		if(isDatasetIDExists){
    			w.Write([]byte("DatasetID is already exists."))
    			return
    		}

    		_, err = tenant.TenantTransactor.Create(trOpts, input)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}

    		// Call Asset Contract to create a new asset using Tenant's input
        	trOpts.PrivateFor = nil
        	trOpts.PrivateFrom = ""
        	ok := createAsset(tesseraClient, assetContractAddr, input, trOpts)
        	if ok != nil {
    			w.Write([]byte(ok.Error()))
    			return
        	}

		//Map Asset
		requestMapAssetBody, err := json.Marshal(map[string]string{
			"contractAddress": contractAddr,
			"datasetId"      : input.DatasetID,
		})

		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}
		httpClient = http.Client {
			Timeout: time.Duration(5 * time.Second),
		}

		request, err = http.NewRequest("POST", "http://localhost:8085/mapAsset", bytes.NewBuffer(requestMapAssetBody))
                request.Header.Set("Content-Type", "application/json")
		if err != nil {
                	w.Write([]byte(err.Error()))
			return
		}

		resp, err = httpClient.Do(request)
		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}
		defer resp.Body.Close()

		respBody, err = ioutil.ReadAll(resp.Body)
                if err != nil {
			w.Write([]byte(err.Error()))
			return
		}

		httpDone := string(respBody)
		if httpDone != "True" {
			w.Write([]byte("An error has occurred during Asset Mapping"))
			return
		}

        	w.Write([]byte("A new record is successfully added."))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

//Read a Tenant's private contract using a specific DatasetID
func readTenant(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    	w.Write([]byte(body))
		    	return
		}

		var client ClientRead
	    	dec := json.NewDecoder(strings.NewReader(string(body)))
	    	err = dec.Decode(&client)
	    	if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		input := client.DataInput

		if clientId == "" {
			w.Write([]byte("ClientId should not be empty."))
			return
		}

		if clientSecret == "" {
			w.Write([]byte("ClientSecret should not be empty."))
			return
		}

		// Read Tenant
                requestBody, err := json.Marshal(map[string]string{
			"clientId": clientId,
		})

		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}

		httpClient := http.Client {
			Timeout: time.Duration(5 * time.Second),
		}

		request, err := http.NewRequest("POST", "http://localhost:8085/readTenant", bytes.NewBuffer(requestBody))
		request.Header.Set("Content-Type", "application/json")
		if err != nil {
	                w.Write([]byte(err.Error()))
			return
		}

		resp, err := httpClient.Do(request)
		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}
		defer resp.Body.Close()

		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			w.Write([]byte(err.Error()))
			return
		}

		contractAddr := string(respBody)
		if contractAddr == "Empty" {
			w.Write([]byte("There is no any contract for this Tenant."))
			return
		}

		// Connect to goQuorum Node
    		goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, ClientCertGoQuorumNodePath, ClientKeyGoQuorumNodePath, RootCertificateGoQuorumNodePath)
    		// Refresh Token
    		var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    			var token string
    			token, err = requestHydraToken(tokenURL, clientId, clientSecret, RootCertificateHydraPath)
    			if err != nil {
    	    			w.Write([]byte(err.Error()))
    			}
    			return token, nil
    		}

    		// Attach token to the headers
    		authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    		defer authenticatedClient.Close()

    		tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)

    		tenant, err := NewTenant(common.HexToAddress(contractAddr), tesseraClient)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}

    		isDatasetIDExists, err := tenant.TenantCaller.IsExists(&bind.CallOpts{}, string(input))
    		//spew.Dump(isDatasetIDExists)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}

    		if(!isDatasetIDExists){
    			w.Write([]byte("There are no structs available."))
    			return
    		}

    		readTenant, err := tenant.TenantCaller.Read(&bind.CallOpts{}, string(input))
    		if err != nil {
    			println(err.Error())
    	    		w.Write([]byte(err.Error()))
    	    		return
    		}

    		output, err := json.Marshal(readTenant)
    		if err != nil {
    			println(err.Error())
    			w.Write([]byte(err.Error()))
    			return
    		}
		w.Write([]byte(output))

	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// Main function
func main() {
    	log.SetFlags(log.Lshortfile)
    	maxProcs := runtime.NumCPU()
    	runtime.GOMAXPROCS(maxProcs)

    	server := &http.Server{
		Addr:         ":" + port,
		ReadTimeout:  5 * time.Minute,
		WriteTimeout: 10 * time.Second,
		TLSConfig:    getTLSConfig(host, caCert),
    	}

	http.HandleFunc("/deployTenant", deployTenant)
	http.HandleFunc("/createTenant", createTenant)
	http.HandleFunc("/readTenant", readTenant)

	log.Printf("Starting HTTPS server on host %s and port %s", host, port)
	if err := server.ListenAndServeTLS(serverCert, serverKey); err != nil {
		log.Fatal(err)
	}
}
