#!/bin/bash
set -u
set -e

PASS="\033[0;32m"
ERROR="\033[0;31m"
WARNING="\033[0;33m"
NC="\033[0m" # No Color

function hydra() {
  CLIENT_ID=$1
  CLIENT_SECRET=$2
  SCOPE=$3

  AUDIENCE="Node"

  $DOCKER/docker run --rm -it -e HYDRA_ADMIN_URL=https://hydra:4445 --network hydra-Network oryd/hydra:latest clients create --skip-tls-verify --id ${CLIENT_ID} --secret ${CLIENT_SECRET} -g client_credentials --audience ${AUDIENCE} --scope "${SCOPE}"
  local TOKEN=$($DOCKER/docker run --rm -it --network hydra-Network oryd/hydra:latest token client --endpoint https://hydra:4444 --skip-tls-verify --client-id ${CLIENT_ID} --client-secret ${CLIENT_SECRET} --audience ${AUDIENCE} --scope "${SCOPE}")

  echo "${TOKEN}"
}


echo -e "${PASS}[*] Searching whether Docker is installed${NC}"
set +e
DockerPath=`which docker`
set -e
DOCKER=
if [[ ! -z "$DockerPath" ]]; then
   echo -e "${PASS}[*] Docker Found${NC}"
   DOCKER=`dirname $DockerPath`
else
   echo -e "${ERROR}[x] Docker Not Installed. Exited...${NC}"
   exit
fi

CLIENT_ID=$1
CLIENT_SECRET=$2

# Multi-Tenancy Tenant Scope
SCOPE="psi://${CLIENT_ID}?self.eoa=0x0&node.eoa=0x0"
# GoQuorum Node Scope - (Uncomment the following in order to register the GoQuorum Node for Token Introspection)
#SCOPE="rpc://rpc_modules,rpc://eth_*,rpc://admin_nodeInfo,rpc://quorumExtension_*,rpc://personal_*"

TOKEN=$(hydra ${CLIENT_ID} ${CLIENT_SECRET} ${SCOPE})
echo -e "${WARNING}###########################################################################${NC}"
echo -e "${PASS}Quorum Node TOKEN: ${TOKEN}${NC}"
echo -e "${WARNING}###########################################################################${NC}"
