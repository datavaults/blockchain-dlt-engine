#!/bin/bash
currentDir=`pwd`

PASS="\033[0;32m"
ERROR="\033[0;31m"
WARNING="\033[0;33m"
NC="\033[0m" # No Color

echo -e "${PASS}[*] Searching whether Geth is installed${NC}"
set +e
GethPath=`which geth`
set -e
GETH=
if [[ ! -z "$GethPath" ]]; then
    echo -e "${PASS}[*] Geth Found${NC}"
    GETH=`dirname $GethPath`
else
    echo -e "${ERROR}[x] Geth Not Found...${NC}"
    echo -e "${PASS}[*] Attempting to retrieve Geth from Local Directory${NC}"
    if [[ -d "${PWD}/geth" ]]; then
       echo -e "${PASS}[*] Geth Found. Retrieved Geth from Local Directory${NC}"
       GETH=${PWD}/geth
    else
       echo -e "${ERROR}[x] Geth Not Found anywhere. Exited...${NC}"
       exit
    fi
fi


#Use https if tls variable is set to STRICT
echo -e "${PASS}[*] TLS is used for secure communications between Quorum Nodes${NC}"
echo -e "${PASS}[*] TLS is used for secure RPC-JSON communications to the Quorum Nodes${NC}"
http=https

echo -e "${PASS}[*] Starting $numOfTesseraNodes Ethereum nodes with ChainID and NetworkId of $NETWORK_ID${NC}"
QUORUM_GETH_ARGS=${QUORUM_GETH_ARGS:-}

#check geth version and if it is below 1.9 then dont include allowSecureUnlock
allowSecureUnlock=
chk=`$GETH/geth help | grep "allow-insecure-unlock" | wc -l`
if (( $chk == 1 )); then
allowSecureUnlock="--allow-insecure-unlock"
fi

basePort=21000
baseRpcPort=22000
baseRaftPort=50401
baseTlsPort=9280
baseInfluxdbPort=8085

DDIR="${currentDir}/nodes/node"
PLUGINS_ARGS="--plugins file://${DDIR}/geth-plugin-settings-$(basename ${DDIR}).json --plugins.skipverify"
MULTITENANCY_ARGS="--multitenancy"
verbosity=5
NETWORK_ID=10
blockTime=50
GETH_ARGS="--nodiscover --nousb ${allowSecureUnlock} --verbosity ${verbosity} --networkid $NETWORK_ID --raft --raftblocktime ${blockTime} --http --http.corsdomain=* --http.vhosts=* --http.addr 0.0.0.0 --http.api admin,debug,web3,eth,txpool,personal,ethash,miner,net,raft,quorumExtension --emitcheckpoints --unlock 0,1,2 --password ${DDIR}/passwords.txt $QUORUM_GETH_ARGS"

ptmTlsPort=$(($baseTlsPort + 1))
raftPort=$(($baseRaftPort))
rpcPort=$(($baseRpcPort))
port=$(($basePort))

#Connect to Tessera with HTTPS
TLS_ARGS="--ptm.url ${http}://192.168.3.105:${ptmTlsPort}"
QUORUM_TLS_CLIENT_DIR="${HOME}/nginx/client"
TLS_ARGS="${TLS_ARGS} --ptm.tls.mode strict --ptm.tls.rootca ${QUORUM_TLS_CLIENT_DIR}/CAcert.pem --ptm.tls.clientcert ${QUORUM_TLS_CLIENT_DIR}/clientCert.pem --ptm.tls.clientkey ${QUORUM_TLS_CLIENT_DIR}/clientsKey.pem"

permissioned="--permissioned"

INFLUXDB_ARGS=
$GETH/geth --identity "Node" --datadir ${DDIR} ${GETH_ARGS} ${TLS_ARGS} ${INFLUXDB_ARGS} ${PLUGINS_ARGS} ${MULTITENANCY_ARGS} ${permissioned} --raftport ${raftPort} --http.port ${rpcPort} --port ${port} 2>>nodes/logs/node.log &
echo -e "${PASS}[*] Starting Quorum Node ...OK${NC}"
