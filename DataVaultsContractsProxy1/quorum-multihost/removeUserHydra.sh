#!/bin/bash
set -u
set -e

PASS="\033[0;32m"
ERROR="\033[0;31m"
WARNING="\033[0;33m"
NC="\033[0m" # No Color

function hydraDelete() {
  CLIENT_ID=$1
  $DOCKER/docker run --rm -it -e HYDRA_ADMIN_URL=https://hydra:4445 --network hydra-Network oryd/hydra:latest clients delete ${CLIENT_ID} --skip-tls-verify
}

echo -e "${PASS}[*] Searching whether Docker is installed${NC}"
set +e
DockerPath=`which docker`
set -e
DOCKER=
if [[ ! -z "$DockerPath" ]]; then
   echo -e "${PASS}[*] Docker Found${NC}"
   DOCKER=`dirname $DockerPath`
else
   echo -e "${ERROR}[x] Docker Not Installed. Exited...${NC}"
   exit
fi

CLIENT_ID=$1

hydraDelete ${CLIENT_ID}

echo -e "${WARNING}###########################################################################${NC}"
echo -e "${PASS}User ${CLIENT_ID} Deleted${NC}"
echo -e "${WARNING}###########################################################################${NC}"
