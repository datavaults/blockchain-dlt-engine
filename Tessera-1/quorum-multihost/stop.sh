#!/bin/bash
killall -INT geth
killall java

docker rmi -f postgres:ssl
docker network rm hydra-Network
docker stop $(docker ps -aq) 
docker rm -f $(docker ps -aq) 
docker system prune --volumes -f

rm genesis.json permissioned-nodes.json
rm -rf nodes/
rm -rf PostgreSQL/ Hydra/
rm -rf API/CA API/TLS

if [ "`ps | grep geth`" != "" ]
then
    kill -9 $(ps | grep geth | awk '{print $1}')
else
    echo "Geth: No process found"
fi

if [ "`jps | grep tessera`" != "" ]
then
    jps | grep tessera | cut -d " " -f1 | xargs kill
else
    echo "Tessera: No process found"
fi

if [ "`jps | grep enclave`" != "" ]
then
    jps | grep enclave | cut -d " " -f1 | xargs kill
else
    echo "Enclave: No process found"
fi

if [ "`jps | grep cakeshop`" != "" ]
then
    jps | grep cakeshop | cut -d " " -f1 | xargs kill
else
    echo "Cakeshop: No process found"
fi
