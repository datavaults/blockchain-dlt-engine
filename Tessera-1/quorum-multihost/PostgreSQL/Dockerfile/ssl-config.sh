
#!/bin/bash

rm /var/lib/postgresql/data/postgresql.conf
rm /var/lib/postgresql/data/pg_hba.conf

cat << EOF > /var/lib/postgresql/data/postgresql.conf
listen_addresses = '*'
port = 5432
max_connections = 1000
# memory
shared_buffers = 128MB
temp_buffers = 8MB
work_mem = 4MB
# WAL / replication
wal_level = hot_standby
max_wal_senders = 5
synchronous_standby_names = 'tessera'
hot_standby = on
password_encryption = 'scram-sha-256'

# here are the SSL specific settings
ssl = on # this enables SSL
ssl_cert_file = '/var/lib/postgresql/serverCert.pem' # this specifies the server certificacte
ssl_key_file = '/var/lib/postgresql/serversKey.pem' # this specifies the server private key
ssl_ca_file = '/var/lib/postgresql/CAcert.pem' # this specific which CA certificate to trust
EOF

cat << EOF > /var/lib/postgresql/data/pg_hba.conf
local   all postgres           peer
hostssl all postgres ::/0      reject
hostssl all postgres 0.0.0.0/0 reject

hostssl all all ::/0      cert clientcert=1
hostssl all all 0.0.0.0/0 cert clientcert=1
EOF