/*
	@Author: George Misiakoulis (Ubitech)
*/
package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"context"
	"strings"
	"runtime"
	"time"
	"fmt"
	"log"

	"github.com/davecgh/go-spew/spew"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
    "github.com/ethereum/go-ethereum/common"
    "github.com/ethereum/go-ethereum/rpc"
)

var (
	port string = "8080"
	host string = "localhost"
	caCert string = "./CA/CAcert.pem"
	serverCert string = "./TLS/server/serverCert.pem"
	serverKey string = "./TLS/server/serverUnecryptedKey.pem"

    goQuorumNodeURL string = "https://localhost:22000/?PSI="
    tokenURL string = "https://localhost:4444/oauth2/token"
    audience string = "Node1"

    tesseraThirdPartyEndpoint string = "https://localhost:9081"
	
	RootCertificateGoQuorumNodePath string = "../nodes/node1/tls/cert.pem"
	RootCertificateHydraPath string = "../Hydra/tls/cert.pem"
	
	ethKeyPath string = "../nodes/node1/keystore"
	keyPassword string = "node1"
	
	RootCertificateThirdPartyPath string = "../nodes/tessera1/tls/ThirdParty/client/CAcert.pem"
	ClientCertThirdPartyPath      string = "../nodes/tessera1/tls/ThirdParty/client/clientCert.pem"
	ClientKeyThirdPartyPath       string = "../nodes/tessera1/tls/ThirdParty/client/clientsKey.pem"
	
	poolContractAddr string = "0x27227003dcd66A649f9f970144Ec8950E16bfBd7"
	assetContractAddr string = "0x516b999f22675FBd4351307793a62F54A0d9471d"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
type ClientDeployCreate struct {
	ClientId     	string
	ClientSecret 	string
	ContractAddress string
	DataInput		TenantData_Sharing_Configuration
}

type ClientRead struct {
	ClientId     	string
	ClientSecret 	string
	ContractAddress string
	DataInput		string
}

//Deploy a Tenant's private smart contract
func deployTenant(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		    return
		}

		var client ClientDeployCreate
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	} 
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		
		// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)
    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	defer authenticatedClient.Close()
    	
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    	
    	//Connect to Public Key Pool to Retrieve Tenant's Private Key
    	publicKey, err := retrievePublicKey(tesseraClient, poolContractAddr, clientId, clientSecret)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	}

    	trOpts := initPrivateTransaction(ethKeyPath, keyPassword)
    	trOpts.GasLimit = 47000000
    	trOpts.PrivateFor = []string{string(publicKey)}
    	trOpts.PrivateFrom = string(publicKey)
    	spew.Dump(trOpts)
    	
    	privateAddr, _, _, err := DeployTenant(trOpts, tesseraClient)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	}
		
		w.Write([]byte("Contract Address is: " + privateAddr.Hex()))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

//Create a private transaction with the Tenant's private contract
func createTenant(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		    return
		}

		var client ClientDeployCreate
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    		return
    	} 
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		contractAddr := client.ContractAddress
		input := client.DataInput
		//spew.Dump(input)
		
		// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)
    	defer goQuorumClient.Close()
    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	defer authenticatedClient.Close()
    	
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    	
    	//Connect to Public Key Pool to Retrieve Tenant's Private Key
    	publicKey, err := retrievePublicKey(tesseraClient, poolContractAddr, clientId, clientSecret)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	}

    	trOpts := initPrivateTransaction(ethKeyPath, keyPassword)
    	trOpts.GasLimit = 47000000
    	trOpts.PrivateFor = []string{string(publicKey)}
    	trOpts.PrivateFrom = string(publicKey)
    	
    	tenant, err := NewTenant(common.HexToAddress(contractAddr), tesseraClient)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	}
    	
    	isDatasetIDExists, err := tenant.TenantCaller.IsExists(&bind.CallOpts{}, string(input.DatasetID))
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	}
    	
    	if(isDatasetIDExists){
    		w.Write([]byte("DatasetID is already exists."))
    		return
    	}
   
    	_, err = tenant.TenantTransactor.Create(trOpts, input)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	}
    	
    	// Call Asset Contract to create a new asset using Tenant's input
        trOpts.PrivateFor = nil
        trOpts.PrivateFrom = ""
        ok := createAsset(tesseraClient, assetContractAddr, input, trOpts)
        if ok != nil {
    		w.Write([]byte(ok.Error()))
    		return
        }
        w.Write([]byte("A new record is successfully added."))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

//Read a Tenant's private contract using a specific DatasetID
func readTenant(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		    return
		}

		var client ClientRead
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	} 
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		contractAddr := client.ContractAddress
		input := client.DataInput
		//spew.Dump(input)

		// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)
    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	defer authenticatedClient.Close()
    	
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    	
    	tenant, err := NewTenant(common.HexToAddress(contractAddr), tesseraClient)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	}
    	
    	isDatasetIDExists, err := tenant.TenantCaller.IsExists(&bind.CallOpts{}, string(input))
    	//spew.Dump(isDatasetIDExists)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	}
    	
    	if(!isDatasetIDExists){
    		w.Write([]byte("There are no structs available."))
    		return
    	}
    	
    	readTenant, err := tenant.TenantCaller.Read(&bind.CallOpts{}, string(input)) 
    	if err != nil {
    		println(err.Error())
    	    w.Write([]byte(err.Error()))
    	    return
    	}
    	 
    	output, err := json.Marshal(readTenant)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    		return
    	}
		w.Write([]byte(output))

	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// Main function
func main() {
    log.SetFlags(log.Lshortfile)
    maxProcs := runtime.NumCPU()
    runtime.GOMAXPROCS(maxProcs)
    
    server := &http.Server{
		Addr:         ":" + port,
		ReadTimeout:  5 * time.Minute,
		WriteTimeout: 10 * time.Second,
		TLSConfig:    getTLSConfig(host, caCert),
	}

	http.HandleFunc("/deploy", deployTenant)
	http.HandleFunc("/create", createTenant)
	http.HandleFunc("/read", readTenant)

	log.Printf("Starting HTTPS server on host %s and port %s", host, port)
	if err := server.ListenAndServeTLS(serverCert, serverKey); err != nil {
		log.Fatal(err)
	}
}
