// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package main

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// Data_TradingConfiguration is an auto generated low-level Go binding around an user-defined struct.
type Data_TradingConfiguration struct {
	DatasetID       string
	DataSeekerID    string
	Price           string
	LicenseType     string
	StandardLicense string
}

// DataTradingABI is the input ABI used to generate the binding from.
const DataTradingABI = "[{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_dataSeekerID\",\"type\":\"string\"}],\"name\":\"_isDataSeekerIDExists\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_datasetID\",\"type\":\"string\"}],\"name\":\"_isDatasetIDExists\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"DatasetID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"DataSeekerID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"Price\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"LicenseType\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"StandardLicense\",\"type\":\"string\"}],\"internalType\":\"structData_Trading.Configuration\",\"name\":\"_configuration\",\"type\":\"tuple\"}],\"name\":\"create\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_dataSeekerID\",\"type\":\"string\"}],\"name\":\"readDataSeeker\",\"outputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"DatasetID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"DataSeekerID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"Price\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"LicenseType\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"StandardLicense\",\"type\":\"string\"}],\"internalType\":\"structData_Trading.Configuration\",\"name\":\"\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_datasetID\",\"type\":\"string\"}],\"name\":\"readDataset\",\"outputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"DatasetID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"DataSeekerID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"Price\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"LicenseType\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"StandardLicense\",\"type\":\"string\"}],\"internalType\":\"structData_Trading.Configuration\",\"name\":\"\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]"

var DataTradingParsedABI, _ = abi.JSON(strings.NewReader(DataTradingABI))

// DataTradingFuncSigs maps the 4-byte function signature to its string representation.
var DataTradingFuncSigs = map[string]string{
	"1bcf9d11": "_isDataSeekerIDExists(string)",
	"3abf2bd3": "_isDatasetIDExists(string)",
	"8e94a9b2": "create((string,string,string,string,string))",
	"a26648d5": "readDataSeeker(string)",
	"4330e25f": "readDataset(string)",
}

// DataTradingBin is the compiled bytecode used for deploying new contracts.
var DataTradingBin = "0x60806040526000805534801561001457600080fd5b50610a5e806100246000396000f3fe608060405234801561001057600080fd5b50600436106100575760003560e01c80631bcf9d111461005c5780633abf2bd3146100855780634330e25f146100985780638e94a9b2146100b8578063a26648d5146100cd575b600080fd5b61006f61006a366004610771565b6100e0565b60405161007c919061095e565b60405180910390f35b61006f610093366004610771565b610144565b6100ab6100a6366004610771565b61019d565b60405161007c9190610969565b6100cb6100c63660046107ac565b61050a565b005b6100ab6100db366004610771565b6105dd565b6000805b6000548110156101395782805190602001206001600083815260200190815260200160002060010160405161011991906108ee565b6040518091039020141561013157600191505061013f565b6001016100e4565b60009150505b919050565b6000805b6000548110156101395782805190602001206001600083815260200190815260200160002060000160405161017d91906108ee565b6040518091039020141561019557600191505061013f565b600101610148565b6101a561063c565b6000805b6000548110156101fc578380519060200120600160008381526020019081526020016000206000016040516101de91906108ee565b604051809103902014156101f4578091506101fc565b6001016101a9565b6000828152600160208181526040928390208351815460029481161561010002600019011693909304601f8101839004909202830160c090810190945260a08301828152929390928492909184919084018282801561029c5780601f106102715761010080835404028352916020019161029c565b820191906000526020600020905b81548152906001019060200180831161027f57829003601f168201915b50505050508152602001600182018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561033e5780601f106103135761010080835404028352916020019161033e565b820191906000526020600020905b81548152906001019060200180831161032157829003601f168201915b5050509183525050600282810180546040805160206001841615610100026000190190931694909404601f810183900483028501830190915280845293810193908301828280156103d05780601f106103a5576101008083540402835291602001916103d0565b820191906000526020600020905b8154815290600101906020018083116103b357829003601f168201915b505050918352505060038201805460408051602060026001851615610100026000190190941693909304601f81018490048402820184019092528181529382019392918301828280156104645780601f1061043957610100808354040283529160200191610464565b820191906000526020600020905b81548152906001019060200180831161044757829003601f168201915b505050918352505060048201805460408051602060026001851615610100026000190190941693909304601f81018490048402820184019092528181529382019392918301828280156104f85780601f106104cd576101008083540402835291602001916104f8565b820191906000526020600020905b8154815290600101906020018083116104db57829003601f168201915b50505050508152505092505050919050565b6040805160a08101825282518152602080840151818301528383015182840152606080850151908301526080808501519083015260008054815260018252929092208151805192939192610561928492019061066b565b50602082810151805161057a926001850192019061066b565b506040820151805161059691600284019160209091019061066b565b50606082015180516105b291600384019160209091019061066b565b50608082015180516105ce91600484019160209091019061066b565b50506000805460010190555050565b6105e561063c565b6000805b6000548110156101fc5783805190602001206001600083815260200190815260200160002060010160405161061e91906108ee565b60405180910390201415610634578091506101fc565b6001016105e9565b6040518060a0016040528060608152602001606081526020016060815260200160608152602001606081525090565b828054600181600116156101000203166002900490600052602060002090601f0160209004810192826106a157600085556106e7565b82601f106106ba57805160ff19168380011785556106e7565b828001600101855582156106e7579182015b828111156106e75782518255916020019190600101906106cc565b506106f39291506106f7565b5090565b5b808211156106f357600081556001016106f8565b600082601f83011261071c578081fd5b813567ffffffffffffffff81111561073057fe5b610743601f8201601f1916602001610a04565b818152846020838601011115610757578283fd5b816020850160208301379081016020019190915292915050565b600060208284031215610782578081fd5b813567ffffffffffffffff811115610798578182fd5b6107a48482850161070c565b949350505050565b6000602082840312156107bd578081fd5b813567ffffffffffffffff808211156107d4578283fd5b9083019060a082860312156107e7578283fd5b6107f160a0610a04565b8235828111156107ff578485fd5b61080b8782860161070c565b82525060208301358281111561081f578485fd5b61082b8782860161070c565b602083015250604083013582811115610842578485fd5b61084e8782860161070c565b604083015250606083013582811115610865578485fd5b6108718782860161070c565b606083015250608083013582811115610888578485fd5b6108948782860161070c565b60808301525095945050505050565b60008151808452815b818110156108c8576020818501810151868301820152016108ac565b818111156108d95782602083870101525b50601f01601f19169290920160200192915050565b600080835460018082166000811461090d576001811461092457610953565b60ff198316865260028304607f1686019350610953565b600283048786526020808720875b8381101561094b5781548a820152908501908201610932565b505050860193505b509195945050505050565b901515815260200190565b600060208252825160a0602084015261098560c08401826108a3565b90506020840151601f19808584030160408601526109a383836108a3565b925060408601519150808584030160608601526109c083836108a3565b925060608601519150808584030160808601526109dd83836108a3565b925060808601519150808584030160a0860152506109fb82826108a3565b95945050505050565b60405181810167ffffffffffffffff81118282101715610a2057fe5b60405291905056fea2646970667358221220b7897ba5b1d737ce115ca925403554c8021b2857449e4ba9fadc93e15e10cae064736f6c63430007060033"

// DeployDataTrading deploys a new Ethereum contract, binding an instance of DataTrading to it.
func DeployDataTrading(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *DataTrading, error) {
	parsed, err := abi.JSON(strings.NewReader(DataTradingABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}

	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(DataTradingBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &DataTrading{DataTradingCaller: DataTradingCaller{contract: contract}, DataTradingTransactor: DataTradingTransactor{contract: contract}, DataTradingFilterer: DataTradingFilterer{contract: contract}}, nil
}

// DataTrading is an auto generated Go binding around an Ethereum contract.
type DataTrading struct {
	DataTradingCaller     // Read-only binding to the contract
	DataTradingTransactor // Write-only binding to the contract
	DataTradingFilterer   // Log filterer for contract events
}

// DataTradingCaller is an auto generated read-only Go binding around an Ethereum contract.
type DataTradingCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DataTradingTransactor is an auto generated write-only Go binding around an Ethereum contract.
type DataTradingTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DataTradingFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type DataTradingFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// DataTradingSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type DataTradingSession struct {
	Contract     *DataTrading      // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// DataTradingCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type DataTradingCallerSession struct {
	Contract *DataTradingCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts      // Call options to use throughout this session
}

// DataTradingTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type DataTradingTransactorSession struct {
	Contract     *DataTradingTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts      // Transaction auth options to use throughout this session
}

// DataTradingRaw is an auto generated low-level Go binding around an Ethereum contract.
type DataTradingRaw struct {
	Contract *DataTrading // Generic contract binding to access the raw methods on
}

// DataTradingCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type DataTradingCallerRaw struct {
	Contract *DataTradingCaller // Generic read-only contract binding to access the raw methods on
}

// DataTradingTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type DataTradingTransactorRaw struct {
	Contract *DataTradingTransactor // Generic write-only contract binding to access the raw methods on
}

// NewDataTrading creates a new instance of DataTrading, bound to a specific deployed contract.
func NewDataTrading(address common.Address, backend bind.ContractBackend) (*DataTrading, error) {
	contract, err := bindDataTrading(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &DataTrading{DataTradingCaller: DataTradingCaller{contract: contract}, DataTradingTransactor: DataTradingTransactor{contract: contract}, DataTradingFilterer: DataTradingFilterer{contract: contract}}, nil
}

// NewDataTradingCaller creates a new read-only instance of DataTrading, bound to a specific deployed contract.
func NewDataTradingCaller(address common.Address, caller bind.ContractCaller) (*DataTradingCaller, error) {
	contract, err := bindDataTrading(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &DataTradingCaller{contract: contract}, nil
}

// NewDataTradingTransactor creates a new write-only instance of DataTrading, bound to a specific deployed contract.
func NewDataTradingTransactor(address common.Address, transactor bind.ContractTransactor) (*DataTradingTransactor, error) {
	contract, err := bindDataTrading(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &DataTradingTransactor{contract: contract}, nil
}

// NewDataTradingFilterer creates a new log filterer instance of DataTrading, bound to a specific deployed contract.
func NewDataTradingFilterer(address common.Address, filterer bind.ContractFilterer) (*DataTradingFilterer, error) {
	contract, err := bindDataTrading(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &DataTradingFilterer{contract: contract}, nil
}

// bindDataTrading binds a generic wrapper to an already deployed contract.
func bindDataTrading(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(DataTradingABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_DataTrading *DataTradingRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _DataTrading.Contract.DataTradingCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_DataTrading *DataTradingRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DataTrading.Contract.DataTradingTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_DataTrading *DataTradingRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _DataTrading.Contract.DataTradingTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_DataTrading *DataTradingCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _DataTrading.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_DataTrading *DataTradingTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _DataTrading.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_DataTrading *DataTradingTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _DataTrading.Contract.contract.Transact(opts, method, params...)
}

// IsDataSeekerIDExists is a free data retrieval call binding the contract method 0x1bcf9d11.
//
// Solidity: function _isDataSeekerIDExists(string _dataSeekerID) view returns(bool)
func (_DataTrading *DataTradingCaller) IsDataSeekerIDExists(opts *bind.CallOpts, _dataSeekerID string) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _DataTrading.contract.Call(opts, out, "_isDataSeekerIDExists", _dataSeekerID)
	return *ret0, err
}

// IsDataSeekerIDExists is a free data retrieval call binding the contract method 0x1bcf9d11.
//
// Solidity: function _isDataSeekerIDExists(string _dataSeekerID) view returns(bool)
func (_DataTrading *DataTradingSession) IsDataSeekerIDExists(_dataSeekerID string) (bool, error) {
	return _DataTrading.Contract.IsDataSeekerIDExists(&_DataTrading.CallOpts, _dataSeekerID)
}

// IsDataSeekerIDExists is a free data retrieval call binding the contract method 0x1bcf9d11.
//
// Solidity: function _isDataSeekerIDExists(string _dataSeekerID) view returns(bool)
func (_DataTrading *DataTradingCallerSession) IsDataSeekerIDExists(_dataSeekerID string) (bool, error) {
	return _DataTrading.Contract.IsDataSeekerIDExists(&_DataTrading.CallOpts, _dataSeekerID)
}

// IsDatasetIDExists is a free data retrieval call binding the contract method 0x3abf2bd3.
//
// Solidity: function _isDatasetIDExists(string _datasetID) view returns(bool)
func (_DataTrading *DataTradingCaller) IsDatasetIDExists(opts *bind.CallOpts, _datasetID string) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _DataTrading.contract.Call(opts, out, "_isDatasetIDExists", _datasetID)
	return *ret0, err
}

// IsDatasetIDExists is a free data retrieval call binding the contract method 0x3abf2bd3.
//
// Solidity: function _isDatasetIDExists(string _datasetID) view returns(bool)
func (_DataTrading *DataTradingSession) IsDatasetIDExists(_datasetID string) (bool, error) {
	return _DataTrading.Contract.IsDatasetIDExists(&_DataTrading.CallOpts, _datasetID)
}

// IsDatasetIDExists is a free data retrieval call binding the contract method 0x3abf2bd3.
//
// Solidity: function _isDatasetIDExists(string _datasetID) view returns(bool)
func (_DataTrading *DataTradingCallerSession) IsDatasetIDExists(_datasetID string) (bool, error) {
	return _DataTrading.Contract.IsDatasetIDExists(&_DataTrading.CallOpts, _datasetID)
}

// ReadDataSeeker is a free data retrieval call binding the contract method 0xa26648d5.
//
// Solidity: function readDataSeeker(string _dataSeekerID) view returns((string,string,string,string,string))
func (_DataTrading *DataTradingCaller) ReadDataSeeker(opts *bind.CallOpts, _dataSeekerID string) (Data_TradingConfiguration, error) {
	var (
		ret0 = new(Data_TradingConfiguration)
	)
	out := ret0
	err := _DataTrading.contract.Call(opts, out, "readDataSeeker", _dataSeekerID)
	return *ret0, err
}

// ReadDataSeeker is a free data retrieval call binding the contract method 0xa26648d5.
//
// Solidity: function readDataSeeker(string _dataSeekerID) view returns((string,string,string,string,string))
func (_DataTrading *DataTradingSession) ReadDataSeeker(_dataSeekerID string) (Data_TradingConfiguration, error) {
	return _DataTrading.Contract.ReadDataSeeker(&_DataTrading.CallOpts, _dataSeekerID)
}

// ReadDataSeeker is a free data retrieval call binding the contract method 0xa26648d5.
//
// Solidity: function readDataSeeker(string _dataSeekerID) view returns((string,string,string,string,string))
func (_DataTrading *DataTradingCallerSession) ReadDataSeeker(_dataSeekerID string) (Data_TradingConfiguration, error) {
	return _DataTrading.Contract.ReadDataSeeker(&_DataTrading.CallOpts, _dataSeekerID)
}

// ReadDataset is a free data retrieval call binding the contract method 0x4330e25f.
//
// Solidity: function readDataset(string _datasetID) view returns((string,string,string,string,string))
func (_DataTrading *DataTradingCaller) ReadDataset(opts *bind.CallOpts, _datasetID string) (Data_TradingConfiguration, error) {
	var (
		ret0 = new(Data_TradingConfiguration)
	)
	out := ret0
	err := _DataTrading.contract.Call(opts, out, "readDataset", _datasetID)
	return *ret0, err
}

// ReadDataset is a free data retrieval call binding the contract method 0x4330e25f.
//
// Solidity: function readDataset(string _datasetID) view returns((string,string,string,string,string))
func (_DataTrading *DataTradingSession) ReadDataset(_datasetID string) (Data_TradingConfiguration, error) {
	return _DataTrading.Contract.ReadDataset(&_DataTrading.CallOpts, _datasetID)
}

// ReadDataset is a free data retrieval call binding the contract method 0x4330e25f.
//
// Solidity: function readDataset(string _datasetID) view returns((string,string,string,string,string))
func (_DataTrading *DataTradingCallerSession) ReadDataset(_datasetID string) (Data_TradingConfiguration, error) {
	return _DataTrading.Contract.ReadDataset(&_DataTrading.CallOpts, _datasetID)
}

// Create is a paid mutator transaction binding the contract method 0x8e94a9b2.
//
// Solidity: function create((string,string,string,string,string) _configuration) returns()
func (_DataTrading *DataTradingTransactor) Create(opts *bind.TransactOpts, _configuration Data_TradingConfiguration) (*types.Transaction, error) {
	return _DataTrading.contract.Transact(opts, "create", _configuration)
}

// Create is a paid mutator transaction binding the contract method 0x8e94a9b2.
//
// Solidity: function create((string,string,string,string,string) _configuration) returns()
func (_DataTrading *DataTradingSession) Create(_configuration Data_TradingConfiguration) (*types.Transaction, error) {
	return _DataTrading.Contract.Create(&_DataTrading.TransactOpts, _configuration)
}

// Create is a paid mutator transaction binding the contract method 0x8e94a9b2.
//
// Solidity: function create((string,string,string,string,string) _configuration) returns()
func (_DataTrading *DataTradingTransactorSession) Create(_configuration Data_TradingConfiguration) (*types.Transaction, error) {
	return _DataTrading.Contract.Create(&_DataTrading.TransactOpts, _configuration)
}
