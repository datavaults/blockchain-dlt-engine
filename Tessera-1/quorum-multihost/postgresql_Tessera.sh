#!/bin/bash
set -u
set -e

currentDir=`pwd`

function CA() {
 #TODO:: add input for type of EC key and certificate subject options
 echo -e "${PASS}[*] Generating Certificate Authority (CA)${NC}"
 local DDIR="${currentDir}/PostgreSQL/"
 mkdir -p ${DDIR}/CA
 pushd ${DDIR}/CA
   echo "CApassword" > password.enc
   $OPENSSL/openssl genpkey -out CAKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
   $OPENSSL/openssl req -new -x509 -nodes -key CAKey.pem -sha256 -days 1024 -out CAcert.pem --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorumPostgresqlCA/OU=DataVaultsPostgresqlCA/CN=postgresCluster" -addext "subjectAltName = DNS:localhost, IP.1:192.168.3.172, IP.2:192.168.3.189, IP.3:192.168.3.175" -passin file:password.enc
 popd
}

function TLS() {
 local TLSDIR=$1
 local NODE=$2
 
 #TODO:: add input for type of EC key and certificate subject options
 local CADIR="${currentDir}/PostgreSQL/CA"
 mkdir -p ${TLSDIR}/server ${TLSDIR}/client
 cp ${CADIR}/CAcert.pem ${TLSDIR}/server
 cp ${CADIR}/CAcert.pem ${TLSDIR}/client
 
 echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Server${NC}"
 pushd ${TLSDIR}/server
    echo "${NODE}ServerTLSPassword" > password.enc
    echo "subjectAltName = IP.1:192.168.3.172, IP.2:192.168.3.189, IP.3:192.168.3.175" > server.ext
    $OPENSSL/openssl genpkey -out serverKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc 
    $OPENSSL/openssl req -new -key serverKey.pem -sha256 -out server.csr --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorum/OU=PostgreSQL/CN=postgres_db" -passin file:password.enc 
    $OPENSSL/openssl x509 -req -days 1024 -in server.csr -CA ${TLSDIR}/server/CAcert.pem -CAkey ${CADIR}/CAKey.pem -CAcreateserial -out serverCert.pem -extfile server.ext -passin file:${CADIR}/password.enc 
    rm *.csr *.srl
    $OPENSSL/openssl pkcs8 -topk8 -inform pem -in serverKey.pem -passin file:password.enc -outform pem -nocrypt -out serversKey.pem
    echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Server is Finished${NC}"
 popd

 echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Client${NC}"
 pushd ${TLSDIR}/client
    echo "${NODE}ClientTLSPassword" > password.enc
    echo "subjectAltName = IP.1:192.168.3.172, IP.2:192.168.3.189, IP.3:192.168.3.175" > client.ext
    $OPENSSL/openssl genpkey -out clientKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
    $OPENSSL/openssl req -new -key clientKey.pem -sha256 -out client.csr --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorum/OU=PostgreSQL/CN=postgres" -passin file:password.enc
    $OPENSSL/openssl x509 -req -days 1024 -in client.csr -CA ${TLSDIR}/client/CAcert.pem -CAkey ${CADIR}/CAKey.pem -CAcreateserial -out clientCert.pem -extfile client.ext -passin file:${CADIR}/password.enc 
    rm *.csr *.srl
    $OPENSSL/openssl pkcs8 -topk8 -inform pem -in clientKey.pem -passin file:password.enc -outform pem -nocrypt -out clientsKey.pem
    $OPENSSL/openssl pkcs12 -export -nocerts -inkey clientsKey.pem -out clientKey.p12 -passout file:password.enc
    echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Client is Finished${NC}"
 popd
}

function sslconfig() {
 local DOCKERFILE_PATH=$1
 mkdir -p ${DOCKERFILE_PATH}
 pushd ${DOCKERFILE_PATH}
  echo -n "
#!/bin/bash

rm /var/lib/postgresql/data/postgresql.conf
rm /var/lib/postgresql/data/pg_hba.conf

cat << EOF > /var/lib/postgresql/data/postgresql.conf
listen_addresses = '*'
port = 5432
max_connections = 1000
# memory
shared_buffers = 128MB
temp_buffers = 8MB
work_mem = 4MB
# WAL / replication
wal_level = hot_standby
max_wal_senders = 5
synchronous_standby_names = 'tessera'
hot_standby = on
password_encryption = 'scram-sha-256'

# here are the SSL specific settings
ssl = on # this enables SSL
ssl_cert_file = '/var/lib/postgresql/serverCert.pem' # this specifies the server certificacte
ssl_key_file = '/var/lib/postgresql/serversKey.pem' # this specifies the server private key
ssl_ca_file = '/var/lib/postgresql/CAcert.pem' # this specific which CA certificate to trust
EOF

cat << EOF > /var/lib/postgresql/data/pg_hba.conf
local   all postgres           peer
hostssl all postgres ::/0      reject
hostssl all postgres 0.0.0.0/0 reject

hostssl all all ::/0      cert clientcert=1
hostssl all all 0.0.0.0/0 cert clientcert=1
EOF" >> ssl-config.sh
 popd
}

function configureDB() {
 local DOCKERFILE_PATH=$1
 pushd ${DOCKERFILE_PATH}

 echo -n '
CREATE DATABASE tesseradb
WITH
OWNER = tessera
ENCODING = "'"UTF8"'"
LC_COLLATE = "'"en_US.utf8"'"
LC_CTYPE = "'"en_US.utf8"'"
TABLESPACE = pg_default
CONNECTION LIMIT = -1;

GRANT TEMPORARY, CONNECT ON DATABASE tesseradb TO PUBLIC;
GRANT ALL ON DATABASE tesseradb TO tessera;
' >> tessera.sql
 popd
}

function prepareDockerfile_TLS() {
 local POSTGRESQL_TLSDIR=$1
 local DOCKERFILE_PATH=$2
 
 mkdir -p ${DOCKERFILE_PATH}/tls
 cp ${POSTGRESQL_TLSDIR}/server/serverCert.pem ${DOCKERFILE_PATH}/tls
 cp ${POSTGRESQL_TLSDIR}/server/serversKey.pem ${DOCKERFILE_PATH}/tls
 cp ${POSTGRESQL_TLSDIR}/server/CAcert.pem ${DOCKERFILE_PATH}/tls
}

function createDockerfile() {
 local DOCKERFILE_PATH=$1
 pushd ${DOCKERFILE_PATH}
  echo -n '
# This Dockerfile contains the image specification of our database
FROM postgres:13-alpine

COPY ./tls/serversKey.pem /var/lib/postgresql
COPY ./tls/serverCert.pem /var/lib/postgresql
COPY ./tls/CAcert.pem /var/lib/postgresql

COPY ./ssl-config.sh /usr/local/bin

COPY ./tessera.sql /docker-entrypoint-initdb.d/

RUN chown 0:70 /var/lib/postgresql/serversKey.pem && chmod 640 /var/lib/postgresql/serversKey.pem
RUN chown 0:70 /var/lib/postgresql/serverCert.pem && chmod 640 /var/lib/postgresql/serverCert.pem

RUN chown 0:70 /var/lib/postgresql/CAcert.pem && chmod 640 /var/lib/postgresql/CAcert.pem

ENTRYPOINT ["'"docker-entrypoint.sh"'"] 

CMD [ "'"-c"'", "'"ssl=on"'" , "'"-c"'", "'"ssl_cert_file=/var/lib/postgresql/serverCert.pem"'", "'"-c"'", "'"ssl_key_file=/var/lib/postgresql/serversKey.pem"'", "'"-c"'", "'"ssl_ca_file=/var/lib/postgresql/CAcert.pem"'", "'"-c"'", "'"max_connections=1000"'" ]
' >> Dockerfile 
 popd
}

function build() {
 local DOCKERFILE_PATH=$1
 pushd ${DOCKERFILE_PATH}
   $DOCKER/docker build --rm -f Dockerfile -t postgres:ssl "."
 popd
}

function run() {
 local POSTGRESQL_DIR=$1

 $DOCKER/docker run -d -p 5432:5432 --name postgres_db -e POSTGRES_USER=tessera -e POSTGRES_PASSWORD=tessera -e POSTGRES_DB=tessera postgres:ssl
 sleep 20
 $DOCKER/docker exec -it postgres_db bash /usr/local/bin/ssl-config.sh
 #$DOCKER/docker restart postgres_db
 $DOCKER/docker logs -f postgres_db >> "${POSTGRESQL_DIR}/logs/postgresql.log" 2>&1 &  
}


################################################################################################
################################################################################################
################################################################################################
function usage() {
  echo ""
  echo "Usage:"
  echo "    $0 [--tls]"
  echo ""
  echo "Where:"
  echo "    --tls enables the TLS between the POSTGRESQL and Tessera Node (default = $tls)"
  echo ""
  exit -1
}

PASS="\033[0;32m"
ERROR="\033[0;31m"
WARNING="\033[0;33m"
NC="\033[0m" # No Color

tls=OFF

while (( "$#" )); do
    case "$1" in
        --tls)
           tls=STRICT
           shift
           ;;
        --help)
           shift
           usage
           ;;
        *)
           echo -e "${ERROR}Error: Unsupported command line parameter $1${NC}"
           usage
           ;;
    esac
done

if [ "$tls" == "STRICT" ]; then
   echo -e "${PASS}[*] Searching whether OpenSSL is installed${NC}"
   set +e
   OpenSSLPath=`which openssl`
   set -e
   OPENSSL=
   if [[ ! -z "$OpenSSLPath" ]]; then
      echo -e "${PASS}[*] OpenSSL Found${NC}"
      echo -e "${PASS}[*] Checking the OpenSSL Version${NC}"
      if [[ `$OpenSSLPath version` == "OpenSSL 1.1.1k  25 Mar 2021" ]]; then
         echo -e "${PASS}[*] OpenSSL has the latest $($OpenSSLPath version) version${NC}"
         OPENSSL=`dirname $OpenSSLPath`
      fi
   else
      echo -e "${ERROR}[x] OPENSSL is not installed. Exited...${NC}"
      exit
   fi
   
   echo -e "${PASS}[*] Searching whether Docker is installed${NC}"
   set +e
   DockerPath=`which docker`
   set -e
   DOCKER=
   if [[ ! -z "$DockerPath" ]]; then
      echo -e "${PASS}[*] Docker Found${NC}"
      DOCKER=`dirname $DockerPath`
   else
      echo -e "${EROR}[x] Docker Not Installed. Exited...${NC}"
      exit
   fi

   echo -e "${PASS}[*] Searching whether Keytool is installed${NC}"
   set +e
   KeytoolPath=`which keytool`
   set -e
   KEYTOOL=
   if [[ ! -z "$KeytoolPath" ]]; then
      echo -e "${PASS}[*] Keytool Found.${NC}"
      KEYTOOL=`dirname $KeytoolPath`
   else
      echo -e "${ERROR}[x] Keytool is not installed. Exited...${NC}"
      exit
   fi

else
   echo -e "${ERROR}[x] This scripts operates only when GoQuorum Node JSON-RPC security is enabled.${NC}"
   exit
fi

echo -e "${PASS}[*] Cleaning up temporary data directories${NC}"
rm -rf PostgreSQL/
$DOCKER/docker stop postgres_db
$DOCKER/docker rm postgres_db
$DOCKER/docker rmi -f postgres:ssl

echo -e "${PASS}[*] Creating PostgreSQL CA ${NC}"
CA

echo -e "${PASS}[*] Creating PostgreSQL TLS ${NC}"
POSTGRESQL_DIR="${currentDir}/PostgreSQL"
POSTGRESQL_TLSDIR="${POSTGRESQL_DIR}/tls"
TLS ${POSTGRESQL_TLSDIR} PostgreSQL

echo -e "${PASS}[*] Creating PostgreSQL SSL configuration file ${NC}"
DOCKERFILE_PATH="${POSTGRESQL_DIR}/Dockerfile"
sslconfig ${DOCKERFILE_PATH}
prepareDockerfile_TLS ${POSTGRESQL_TLSDIR} ${DOCKERFILE_PATH}
configureDB ${DOCKERFILE_PATH}

echo -e "${PASS}[*] Creating PostgreSQL Dockerfile ${NC}"
createDockerfile ${DOCKERFILE_PATH}

echo -e "${PASS}[*] Building PostgreSQL Dockerfile ${NC}"
build ${DOCKERFILE_PATH}

echo -e "${PASS}[*] Running PostgreSQL Dockerfile ${NC}"
mkdir -p ${POSTGRESQL_DIR}/logs
run ${POSTGRESQL_DIR}










