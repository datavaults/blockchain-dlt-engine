#!/bin/bash
#TODO::Add numberOfAccounts flag in createAccounts function
set -u
set -e

#################################################################################################
################################### Initialize Tesseara Nodes ###################################
#################################################################################################

# Function to extract the IP for each peer from permissioned-nodes.json
# Results are written to the array 'peerIPList'
declare -a peerIPList
function getPeerIPs() {
    peerNum=0

    numLines=`wc -l permissioned-nodes.json  | xargs | cut -f 1,1 -d " "`
    for lineNumber in `seq 1 ${numLines}`
    do
        #if line contains an enode entry then process it, else ignore it
        line=`sed -n ${lineNumber},${lineNumber}p permissioned-nodes.json`
        if [[ "$line" =~ "enode" ]]; then

            hostIP=`echo $line |cut -f 2,2 -d "@" | cut -f 1,1 -d ":" `
            regexpIP='([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])'
            if [[ ! ( $hostIP =~ ^$regexpIP\.$regexpIP\.$regexpIP\.$regexpIP$ || "$hostIP" == "localhost" ) ]]; then
                hostIP="127.0.0.1"
                echo -e "${WARNING}[*] WARNING: invalid enode IP found on line ${lineNumber} of permissioned-nodes.json, defaulting to '${hostIP}'${NC}"
            fi
            peerNum=$((${peerNum} + 1))
            peerIPList[${peerNum}]="$hostIP"

        fi
    done
}

function CA() {
 #TODO:: add input for type of EC key and certificate subject options
 echo -e "${PASS}[*] Generating Certificate Authority (CA)${NC}"
 DDIR="${currentDir}/nodes/"
 mkdir -p ${DDIR}/CA
 pushd ${DDIR}/CA
   echo "CApassword" > password.enc
   $OPENSSL/openssl genpkey -out CAKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
   $OPENSSL/openssl req -new -x509 -nodes -key CAKey.pem -sha256 -days 1024 -out CAcert.pem --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorumCA/OU=DataVaultsCA/CN=localhost" -addext "subjectAltName = DNS:localhost, IP:127.0.0.1" -passin file:password.enc
 popd
}

function TLS() {
 TLSDIR=$1
 NODE=$2
 
 #TODO:: add input for type of EC key and certificate subject options
 CADIR="${currentDir}/nodes/CA"
 mkdir -p ${TLSDIR}/server ${TLSDIR}/client
 cp ${CADIR}/CAcert.pem ${TLSDIR}/server
 cp ${CADIR}/CAcert.pem ${TLSDIR}/client
       
 echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Server${NC}"
 pushd ${TLSDIR}/server
    echo "${NODE}ServerTLSPassword" > password.enc
    echo "subjectAltName = DNS:localhost, IP:127.0.0.1" > server.ext
    $OPENSSL/openssl genpkey -out serverKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc 
    $OPENSSL/openssl req -new -key serverKey.pem -sha256 -days 1024 -out server.csr --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorum/OU=DataVaults/CN=localhost" -passin file:password.enc 
    $OPENSSL/openssl x509 -req -in server.csr -CA ${TLSDIR}/server/CAcert.pem -CAkey ${CADIR}/CAKey.pem -CAcreateserial -out serverCert.pem -extfile server.ext -passin file:${CADIR}/password.enc 
    rm *.csr *.srl
    $OPENSSL/openssl pkcs8 -topk8 -inform pem -in serverKey.pem -passin file:password.enc -outform pem -nocrypt -out serversKey.pem
    $OPENSSL/openssl pkcs12 -export -in serverCert.pem -inkey serversKey.pem -out serverKeystore.p12 -name serverKeystore -CAfile CAcert.pem -caname CA -password file:password.enc
    $KEYTOOL/keytool -importkeystore -deststorepass $(cat password.enc) -destkeypass $(cat password.enc) -destkeystore serverKeystore.jks -srckeystore serverKeystore.p12 -srcstoretype PKCS12 -srcstorepass $(cat password.enc) -alias serverKeystore
    echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Server is Finished${NC}"
 popd
      
 echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Client${NC}"
 pushd ${TLSDIR}/client
    echo "${NODE}ClientTLSPassword" > password.enc
    echo "subjectAltName = DNS:localhost, IP:127.0.0.1" > client.ext
    $OPENSSL/openssl genpkey -out clientKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
    $OPENSSL/openssl req -new -key clientKey.pem -sha256 -days 1024 -out client.csr --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorum/OU=DataVaults/CN=localhost" -passin file:password.enc
    $OPENSSL/openssl x509 -req -in client.csr -CA ${TLSDIR}/client/CAcert.pem -CAkey ${CADIR}/CAKey.pem -CAcreateserial -out clientCert.pem -extfile client.ext -passin file:${CADIR}/password.enc 
    rm *.csr *.srl
    $OPENSSL/openssl pkcs8 -topk8 -inform pem -in clientKey.pem -passin file:password.enc -outform pem -nocrypt -out clientsKey.pem
    $OPENSSL/openssl pkcs12 -export -in clientCert.pem -inkey clientsKey.pem -out clientKeystore.p12 -name clientKeystore -CAfile CAcert.pem -caname CA -password file:password.enc
    $KEYTOOL/keytool -importkeystore -deststorepass $(cat password.enc) -destkeypass $(cat password.enc) -destkeystore clientKeystore.jks -srckeystore clientKeystore.p12 -srcstoretype PKCS12 -srcstorepass $(cat password.enc) -alias clientKeystore
    echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Client is Finished${NC}"
 popd
}

function tesseraInit() {
 privacyImpl=$1
 tesseraJar=$2
 numOfTesseraNodes=$3
 encryptorType=$4
 tls=$5
 influxDB=$6
 multitenancy=$7
 numOfTenants=$8
 
 if  [ "$privacyImpl" == "tessera-remote" ]; then
     echo -e "${PASS}[*] Creating configuration files for Remote Enclaves${NC}"
 else
     echo -e "${PASS}[*] Creating configuration files for Local Enclaves${NC}"
 fi
 
 if [[ $influxDB == "ON" ]]; then
     echo -e "${PASS}[*] InfluxDB is enabled. Metrics will be pushed from Tessera to the DB${NC}"
 else
     echo -e "${ERROR}[x] InfluxDB is disabled. No metrics will be pushed from Tessera to the DB${NC}"
 fi
 
 
 currentDir=$(pwd)
 
 #Use https if tls variable is set to STRICT
 http=http
 if [[ $tls == "STRICT" ]]; then
     echo -e "${PASS}[*] TLS is used for secure communications between Tessera Nodes${NC}"
     http=https
 fi
 
####################################################
############ Initialise Tessera Node ###############
####################################################
 echo -e "${PASS}[*] Initialising Tessera configuration for $numOfTesseraNodes node(s)${NC}"
 encryptorProps=""
 if [ "$encryptorType" == "EC" ]; then
    echo -e "${PASS}[*] Elliptic Curve encryption is used${NC}"
    encryptorProps=$(printf "\"symmetricCipher\":\"%s\",\n            \"ellipticCurve\":\"%s\",\n            \"nonceLength\":\"%s\",\n            \"sharedKeyLength\":\"%s\"" \
     "${ENCRYPTOR_EC_SYMMETRIC_CIPHER:-AES/GCM/NoPadding}" "${ENCRYPTOR_EC_ELLIPTIC_CURVE:-secp256r1}" "${ENCRYPTOR_EC_NONCE_LENGTH:-24}" "${ENCRYPTOR_EC_SHARED_KEY_LENGTH:-32}" )
     
 else
    echo -e "${PASS}[*] NACL encryption is used${NC}"
 fi

####################################################
############ Generate Tessera Node Keys ############
####################################################
 ARGS=
 for i in `seq 1 ${numOfTesseraNodes}`
 do
    echo -e "${PASS}[*] Creating tessera${i} directory${NC}"
    mkdir -p ${PWD}/nodes/tessera${i}/keys
    echo -e "${PASS}[*] Entering tessera${i} directory${NC}"
    pushd ${PWD}/nodes/tessera${i}/keys
      for j in `seq 1 ${numOfTenants}`
      do
        echo -e "${PASS}[*] Creating tenant${j} directory${NC}"
        mkdir -p tenant${j}
        echo -e "${PASS}[*] Entering tenant${j} directory${NC}"
        pushd tenant${j}
          if [ "$encryptorType" == "EC" ]; then
            #TODO::Change Elliptic Curve Key options
            echo -e "${PASS}[*] Generating EC Tenant${j} key for Tessera${i} ${NC}"
            ARGS="--encryptor.type EC \
                  --encryptor.ellipticCurve secp256r1 \
                  --encryptor.symmetricCipher AES/GCM/NoPadding \
                  --encryptor.nonceLength 24 \
                  --encryptor.sharedKeyLength 32"
          else
            echo -e "${PASS}[*] Generating NACL Tenant${j} key for Tessera${i} ${NC}"
          fi

          nodePassword=Tessera${i}Tenant${j}
          echo -e "$nodePassword\n$nodePassword" > password.txt
          java -jar ${tesseraJar} -keygen ${ARGS} -filename tm < password.txt
          rm password.txt
          echo "$nodePassword" > password.txt
          echo -e "${PASS}[*] Leaving tenant${j} directory${NC}"
        popd
      done
      echo -e "${PASS}[*] Leaving tessera${i} directory${NC}"
    popd
 done
 
 echo -e "${PASS}[*] Getting the list of IP addresses for peers${NC}"
 getPeerIPs
 # Dynamically create the config for peers, depending on numOfTesseraNodes
 echo -e "${PASS}[*] Dynamically create the config for peers for $numOfTesseraNodes node(s)${NC}"
 peerList=
 for i in `seq 1 ${numOfTesseraNodes}`
 do
    if [[ $i -ne 1 ]]; then
        peerList="$peerList,"
    fi

    portNum=$((9000 + $i))

    hostIP=${peerIPList[$i]}
    if [[ "$hostIP" == "" ]]; then
        hostIP="127.0.0.1"
        echo -e "${WARNING}[*] WARNING: host IP for node $i not found in permissioned-nodes.json, defaulting to '${hostIP}'${NC}"
    fi

    peerList="${peerList}
        {
            \"url\": \"${http}://${hostIP}:${portNum}\"
        }"
 done
 
 # Create Certificate Authority for Tessera Nodes
 if [[ $tls == "STRICT" ]]; then   
    CA
 fi
 
 if [[ $influxDB == "ON" ]]; then
     INFLUXDB_DIR=
     declare -a influxDB_servers=("ThirdParty" "Q2T" "P2P" "GoQuorum")
     if  [ "$privacyImpl" == "tessera-remote" ]; then
          declare -a influxDB_servers=("ThirdParty" "Q2T" "P2P" "GoQuorum" "ENCLAVE")
     fi
 fi
 
 echo -e "${PASS}[*] Generating config files for $numOfTesseraNodes Tessera nodes(s)${NC}"
 TESSERA_TLSDIR_ThirdParty=
 TESSERA_TLSDIR_P2P=
 INFLUXDB_DIR=
 sslConfigProps_ThirdParty=
 sslConfigProps_P2P=
 sslConfigProps_Q2T=
 sslConfigProps_ENCLAVE=
 sslConfigProps_InfluxDB=
 influxDB_Params_ThirdParty=
 influxDB_Params_Q2T=
 influxDB_Params_P2P=
 influxDB_Params_ENCLAVE=
 INFLUXDB_SERVER_ARGS=

 for i in `seq 1 ${numOfTesseraNodes}`
 do
    DDIR="${currentDir}/nodes/tessera${i}"
    KEYDIR="${DDIR}/keys"
       
    #Create Client/Server Keys and Certs
    if [[ $tls == "STRICT" ]]; then
       TESSERA_TLSDIR_ThirdParty="${DDIR}/tls/ThirdParty"
       TESSERA_TLSDIR_Q2T="${DDIR}/tls/Q2T"
       TESSERA_TLSDIR_P2P="${DDIR}/tls/P2P"
       
       TLS ${TESSERA_TLSDIR_ThirdParty} ThirdParty${i}
       TLS ${TESSERA_TLSDIR_Q2T} Quorum${i}
       TLS ${TESSERA_TLSDIR_P2P} Tessera${i}
       
       sslConfigProps_ThirdParty=$(printf ",\n\t\t \"serverKeyStore\" : \"${TESSERA_TLSDIR_ThirdParty}/server/serverKeystore.jks\",\n\t\t \"serverKeyStorePassword\" : \"$(cat ${TESSERA_TLSDIR_ThirdParty}/server/password.enc)\",\n\t\t \"serverTrustCertificates\" : [ \n\t\t%-5s \"${TESSERA_TLSDIR_ThirdParty}/server/CAcert.pem\" \n\t\t ],\n\t\t \"serverTrustMode\" : \"CA\",\n\t\t \"clientKeyStore\" : \"${TESSERA_TLSDIR_ThirdParty}/client/clientKeystore.jks\",\n\t\t \"clientKeyStorePassword\" : \"$(cat ${TESSERA_TLSDIR_ThirdParty}/client/password.enc)\",\n\t\t \"clientTrustCertificates\" : [ \n\t\t%-5s \"${TESSERA_TLSDIR_ThirdParty}/client/CAcert.pem\" \n\t\t ],\n\t\t \"clientTrustMode\" : \"CA\",\n\t\t \"generateKeyStoreIfNotExisted\" : \"false\", \n\t\t \"clientAuth\": \"true\" ")
       
       sslConfigProps_Q2T=$(printf ",\n\t\t \"serverKeyStore\" : \"${TESSERA_TLSDIR_Q2T}/server/serverKeystore.jks\",\n\t\t \"serverKeyStorePassword\" : \"$(cat ${TESSERA_TLSDIR_Q2T}/server/password.enc)\",\n\t\t \"serverTrustCertificates\" : [ \n\t\t%-5s \"${TESSERA_TLSDIR_Q2T}/server/CAcert.pem\" \n\t\t ],\n\t\t \"serverTrustMode\" : \"CA\",\n\t\t \"clientKeyStore\" : \"${TESSERA_TLSDIR_Q2T}/client/clientKeystore.jks\",\n\t\t \"clientKeyStorePassword\" : \"$(cat ${TESSERA_TLSDIR_Q2T}/client/password.enc)\",\n\t\t \"clientTrustCertificates\" : [ \n\t\t%-5s \"${TESSERA_TLSDIR_Q2T}/client/CAcert.pem\" \n\t\t ],\n\t\t \"clientTrustMode\" : \"CA\",\n\t\t \"generateKeyStoreIfNotExisted\" : \"false\", \n\t\t \"clientAuth\": \"true\" ")
       
      sslConfigProps_P2P=$(printf ",\n\t\t \"serverKeyStore\" : \"${TESSERA_TLSDIR_P2P}/server/serverKeystore.jks\",\n\t\t \"serverKeyStorePassword\" : \"$(cat ${TESSERA_TLSDIR_P2P}/server/password.enc)\",\n\t\t \"serverTrustCertificates\" : [ \n\t\t%-5s \"${TESSERA_TLSDIR_P2P}/server/CAcert.pem\" \n\t\t ],\n\t\t \"serverTrustMode\" : \"CA\",\n\t\t \"clientKeyStore\" : \"${TESSERA_TLSDIR_P2P}/client/clientKeystore.jks\",\n\t\t \"clientKeyStorePassword\" : \"$(cat ${TESSERA_TLSDIR_P2P}/client/password.enc)\",\n\t\t \"clientTrustCertificates\" : [ \n\t\t%-5s \"${TESSERA_TLSDIR_P2P}/client/CAcert.pem\" \n\t\t ],\n\t\t \"clientTrustMode\" : \"CA\",\n\t\t \"generateKeyStoreIfNotExisted\" : \"false\",\n\t\t \"clientAuth\": \"true\" ")
      
      if  [ "$privacyImpl" == "tessera-remote" ]; then
         TESSERA_TLSDIR_ENCLAVE="${DDIR}/tls/ENCLAVE"
       
         TLS ${TESSERA_TLSDIR_ENCLAVE} Enclave${i}
         
         sslConfigProps_ENCLAVE=$(printf ",\n\t\t \"serverKeyStore\" : \"${TESSERA_TLSDIR_ENCLAVE}/server/serverKeystore.jks\",\n\t\t \"serverKeyStorePassword\" : \"$(cat ${TESSERA_TLSDIR_ENCLAVE}/server/password.enc)\",\n\t\t \"serverTrustCertificates\" : [ \n\t\t%-5s \"${TESSERA_TLSDIR_ENCLAVE}/server/CAcert.pem\" \n\t\t ],\n\t\t \"serverTrustMode\" : \"CA\",\n\t\t \"clientKeyStore\" : \"${TESSERA_TLSDIR_ENCLAVE}/client/clientKeystore.jks\",\n\t\t \"clientKeyStorePassword\" : \"$(cat ${TESSERA_TLSDIR_ENCLAVE}/client/password.enc)\",\n\t\t \"clientTrustCertificates\" : [ \n\t\t%-5s \"${TESSERA_TLSDIR_ENCLAVE}/client/CAcert.pem\" \n\t\t ],\n\t\t \"clientTrustMode\" : \"CA\",\n\t\t \"generateKeyStoreIfNotExisted\" : \"false\",\n\t\t \"clientAuth\": \"true\" ")
      fi
      
    fi
    
    if [[ $influxDB == "ON" ]]; then
       INFLUXDB_CREATE_DB_PATH=
       if [[ $tls == "STRICT" ]]; then   
           INFLUXDB_DIR="${currentDir}/nodes/influxDB${i}/tls"
           TLS ${INFLUXDB_DIR} InfluxDB${i}
           
           sslConfigProps_InfluxDB=$(printf ",\n\t\t%-5s \"sslConfigType\": \"CLIENT_ONLY\",\n\t\t%-5s \"clientTrustMode\": \"CA\", \n\t\t%-5s \"clientKeyStore\": \"${INFLUXDB_DIR}/client/clientKeystore.jks\", \n\t\t%-5s \"clientKeyStorePassword\": \"$(cat ${INFLUXDB_DIR}/client/password.enc)\", \n\t\t%-5s \"clientTrustCertificates\" : [ \n\t\t\t \"${INFLUXDB_DIR}/client/CAcert.pem\" \n\t\t%-5s ] ")
           
           cat ${INFLUXDB_DIR}/server/serverCert.pem ${INFLUXDB_DIR}/server/CAcert.pem > ${INFLUXDB_DIR}/server/serversCert.pem
                  
           INFLUXDB_SERVER_ARGS="-v ${INFLUXDB_DIR}/../data:/var/lib/influxdb -v ${INFLUXDB_DIR}/../config:/etc/influxdb -v ${INFLUXDB_DIR}/../influx_init.iql:/docker-entrypoint-initdb.d/influx_init.iql -v ${INFLUXDB_DIR}/server:/etc/ssl -e INFLUXDB_HTTP_HTTPS_ENABLED=true -e INFLUXDB_HTTP_HTTPS_CERTIFICATE=/etc/ssl/serversCert.pem -e INFLUXDB_HTTP_HTTPS_PRIVATE_KEY=/etc/ssl/serversKey.pem"
           
          INFLUXDB_CREATE_DB_PATH=$INFLUXDB_DIR/..
       else
          INFLUXDB_DIR="${currentDir}/nodes/influxDB${i}"
          mkdir -p ${INFLUXDB_DIR}
          INFLUXDB_SERVER_ARGS="-v ${INFLUXDB_DIR}/data:/var/lib/influxdb -v ${INFLUXDB_DIR}/config:/etc/influxdb -v ${INFLUXDB_DIR}/influx_init.iql:/docker-entrypoint-initdb.d/influx_init.iql"
          INFLUXDB_CREATE_DB_PATH=$INFLUXDB_DIR
       fi
       
       
       pushd ${INFLUXDB_CREATE_DB_PATH}
         for j in "${influxDB_servers[@]}"
         do
           echo -e 'CREATE DATABASE "'$j'"' >> influx_init.iql
         done
       popd
        
       echo -e "${PASS}[*] Deploying InfluxDB${i} server${NC}"
       serverPortInfluxDB=$((8085 + ${i}))
       
       $DOCKER/docker run -d --name=influxDB${i} -p ${serverPortInfluxDB}:8086 ${INFLUXDB_SERVER_ARGS} influxdb:1.8
       $DOCKER/docker logs -f influxDB${i} >> "nodes/logs/influxDB${i}.log" 2>&1 &  
       
       influxDB_Params_ThirdParty=$(printf ",\n\t%-3s \"influxConfig\": { \n\t\t \"serverAddress\": \"${http}://127.0.0.1:${serverPortInfluxDB}\", \n\t\t \"dbName\": \"ThirdParty\", \n\t\t \"pushIntervalInSecs\": 15, \n\t\t \"sslConfig\": { \n\t\t%-5s \"tls\": \"${tls}\"${sslConfigProps_InfluxDB} \n\t\t } \n\t%-3s }")
       
       influxDB_Params_Q2T=$(printf ",\n\t%-3s \"influxConfig\": { \n\t\t \"serverAddress\": \"${http}://127.0.0.1:${serverPortInfluxDB}\", \n\t\t \"dbName\": \"Q2T\", \n\t\t \"pushIntervalInSecs\": 15, \n\t\t \"sslConfig\": { \n\t\t%-5s \"tls\": \"${tls}\"${sslConfigProps_InfluxDB} \n\t\t } \n\t%-3s }")
       
       influxDB_Params_P2P=$(printf ",\n\t%-3s \"influxConfig\": { \n\t\t \"serverAddress\": \"${http}://127.0.0.1:${serverPortInfluxDB}\", \n\t\t \"dbName\": \"P2P\", \n\t\t \"pushIntervalInSecs\": 15, \n\t\t \"sslConfig\": { \n\t\t%-5s \"tls\": \"${tls}\"${sslConfigProps_InfluxDB} \n\t\t } \n\t%-3s }")
       
       if  [ "$privacyImpl" == "tessera-remote" ]; then
           influxDB_Params_ENCLAVE=$(printf ",\n\t%-3s \"influxConfig\": { \n\t\t \"serverAddress\": \"${http}://127.0.0.1:${serverPortInfluxDB}\", \n\t\t \"dbName\": \"ENCLAVE\", \n\t\t \"pushIntervalInSecs\": 15, \n\t\t \"sslConfig\": { \n\t\t%-5s \"tls\": \"${tls}\"${sslConfigProps_InfluxDB} \n\t\t } \n\t%-3s }")
       fi
          
    fi

    serverPortP2P=$((9000 + ${i}))
    serverPortQ2T=$((9280 + ${i}))
    serverPortThirdParty=$((9080 + ${i}))
    serverPortEnclave=$((9180 + ${i}))
    
    keyData=
    for j in `seq 1 ${numOfTenants}`
    do
      
      if [[ $j -ne 1 ]]; then
       keyData="$keyData,"
      else
       keyData="$keyData"
      fi
      
      echo "$(cat ${KEYDIR}/tenant${j}/password.txt)" >> ${KEYDIR}/passwords.txt
      keyData="$(printf "${keyData}\n\t%-4s {\n\t%-8s \"privateKeyPath\": \"${KEYDIR}/tenant${j}/tm.key\",\n\t%-8s \"publicKeyPath\": \"${KEYDIR}/tenant${j}/tm.pub\"\n\t%-4s }")"
    done
    tessera_Key_Params="$(printf "{\n\t \"passwordFile\": \"${KEYDIR}/passwords.txt\",\n\t \"keyData\": [\n\t%-8s ${keyData}\n\t ]\n%-4s}")" 
    
    residentGroups=
    enable_Multitenancy=$(printf "false")
    if  [ "$multitenancy" == "ON" ]; then
        enable_Multitenancy=$(printf "true")
        
        for j in `seq 1 ${numOfTenants}`
        do
        
          if [[ $j -ne 1 ]]; then
            residentGroups="$residentGroups,"
          else
            residentGroups="$residentGroups"
          fi
          residentGroups=$(printf "${residentGroups}\n\t {\n\t%-4s \"name\": \"PS${i}${j}\",\n\t%-4s \"members\": [\"$(cat ${KEYDIR}/tenant${j}/tm.pub)\"],\n\t%-4s \"description\": \"Quorum Node ${i} Private state for Tenant ${j}\" \n\t }")
       done
   fi

        
    if  [ "$privacyImpl" == "tessera" ]; then
     echo -e "${PASS}[*] Writing the config file for the Tessera node${i}${NC}"
     cat <<EOF > ${DDIR}/tessera-config-09-${i}.json
{
    "encryptor":{
        "type":"${encryptorType}",
        "properties":{
            ${encryptorProps}
        }
    },
    "useWhiteList": true,
    "jdbc": {
        "username": "sa",
        "password": "",
        "url": "jdbc:h2:${DDIR}/db${i};MODE=Oracle;TRACE_LEVEL_SYSTEM_OUT=0",
        "autoCreateTables": true
    },
    "serverConfigs":[
        {
            "app":"ThirdParty",
            "enabled": true,
            "serverAddress": "${http}://localhost:${serverPortThirdParty}",
            "cors" : {
                "allowedMethods" : ["GET", "OPTIONS"],
                "allowedOrigins" : ["*"]
            },
            "sslConfig" : {
    		 "tls" : "${tls}"${sslConfigProps_ThirdParty}
    	    },
            "communicationType" : "REST"${influxDB_Params_ThirdParty}
        },
        {
            "app":"Q2T",
            "enabled": true,
            "serverAddress": "${http}://localhost:${serverPortQ2T}",
            "sslConfig" : {
    		 "tls" : "${tls}"${sslConfigProps_Q2T}
    	    },
            "communicationType" : "REST"${influxDB_Params_Q2T}
        },
        {
            "app":"P2P",
            "enabled": true,
            "serverAddress":"${http}://localhost:${serverPortP2P}",
            "sslConfig" : {
    		 "tls" : "${tls}"${sslConfigProps_P2P}           
            },
            "communicationType" : "REST"${influxDB_Params_P2P}
        }
    ],
    "peer": [
        ${peerList}
    ],
    "keys": ${tessera_Key_Params},
    "alwaysSendTo": [],
    "features": {
        "enablePrivacyEnhancements": "true",
        "enableRemoteKeyValidation": "true",
        "enableMultiplePrivateStates": "${enable_Multitenancy}" 
    },
    "residentGroups": [
    	${residentGroups}
    ]
}
EOF
    echo -e "${PASS}[*] Writing the config file for the Tessera node${i} is completed${NC}"
    fi

    if  [ "$privacyImpl" == "tessera-remote" ]; then
     # Enclave configurations
     echo -e "${PASS}[*] Writing the config file for the Enclave node${i}${NC}"
cat <<EOF > ${DDIR}/tessera-config-enclave-09-${i}.json
{
    "useWhiteList": true,
    "jdbc": {
        "username": "sa",
        "password": "",
        "url": "jdbc:h2:${DDIR}/db${i};MODE=Oracle;TRACE_LEVEL_SYSTEM_OUT=0",
        "autoCreateTables": true
    },
    "serverConfigs":[
       {
            "app":"ENCLAVE",
            "enabled": true,
            "serverAddress": "${http}://localhost:${serverPortEnclave}",
            "sslConfig" : {
    		 "tls" : "${tls}"${sslConfigProps_ENCLAVE}
    	    },
            "communicationType" : "REST"${influxDB_Params_ENCLAVE}
       },
       {
            "app":"ThirdParty",
            "enabled": true,
            "serverAddress": "${http}://localhost:${serverPortThirdParty}",
            "cors" : {
                "allowedMethods" : ["GET", "OPTIONS"],
                "allowedOrigins" : ["*"]
            },
            "sslConfig" : {
    		 "tls" : "${tls}"${sslConfigProps_ThirdParty}
    	    },
            "communicationType" : "REST"${influxDB_Params_ThirdParty}
        },
        {
            "app":"Q2T",
            "enabled": true,
            "serverAddress": "${http}://localhost:${serverPortQ2T}",
            "sslConfig" : {
    		 "tls" : "${tls}"${sslConfigProps_Q2T}
    	    },
            "communicationType" : "REST"${influxDB_Params_Q2T}
        },
        {
            "app":"P2P",
            "enabled": true,
            "serverAddress":"${http}://localhost:${serverPortP2P}",
            "sslConfig" : {
    		 "tls" : "${tls}"${sslConfigProps_P2P}           
            },
            "communicationType" : "REST"${influxDB_Params_P2P}
        }
    ],
    "peer": [
        ${peerList}
    ]
}
EOF

cat <<EOF > ${DDIR}/enclave-09-${i}.json
{
    "encryptor":{
        "type":"${encryptorType}",
        "properties":{
            ${encryptorProps}
        }
    },
    "serverConfigs":[
        {
            "app":"ENCLAVE",
            "enabled": true,
            "serverAddress": "${http}://localhost:${serverPortEnclave}",
            "sslConfig" : {
    		 "tls" : "${tls}"${sslConfigProps_ENCLAVE}
    	    },
            "communicationType" : "REST"${influxDB_Params_ENCLAVE}
       }
    ],
    "keys": ${tessera_Key_Params},
    "alwaysSendTo": [],
    "features": {
        "enablePrivacyEnhancements": true,
        "enableRemoteKeyValidation": true,
        "enableMultiplePrivateStates": "${enable_Multitenancy}"
    },
    "residentGroups": [
    	${residentGroups}
    ]
}
EOF
    fi
  echo -e "${PASS}[*] Writing the config file for the Enclave node${i} is completed${NC}"
 done
}
#################################################################################################
################################### Initialize Raft #############################################
#################################################################################################
############################################
############ Create Accounts ###############
############################################
function createAccounts() {
 numOfTesseraNodes=$1
 createAccountArr=
 for i in `seq 1 ${numOfTesseraNodes}`
 do
    mkdir -p nodes/node${i}/{keystore,geth}
    nodePassword=node${i}
    echo -e "$nodePassword" >> ${PWD}/nodes/node${i}/passwords.txt
    createAccount=`$GETH/geth --datadir nodes/node${i} account new --password <(echo $nodePassword) | grep "Public address of the key:" | grep -o ":.*" | awk '{print $2}'`
    echo -e "${PASS}###########################################################################${NC}"
    echo -e "${PASS}## Public address of the key: $createAccount ##${NC}"
    echo -e "${PASS}###########################################################################${NC}"
    createAccountArr=(${createAccountArr[@]} `echo $createAccount`)
 done
}

############################################
############ Create Genesis ################
############################################
function createGenesis() {
 multitenancy=$1
 shift
 accountArray=("$@")
 
 isMPS=$(printf "false")
 if  [ "$multitenancy" == "ON" ]; then
   isMPS=$(printf "true")
 fi
 
 touch ${PWD}/genesis.json
echo '
{
  "alloc": {' >> ${PWD}/genesis.json
  for (( accountNum=0; accountNum < ${#accountArray[@]}; accountNum++ ))
   do
    if [[ $accountNum -eq 0 ]]; then
     echo -n '
    "'"${accountArray[$accountNum]}"'": {
      "balance": "1000000000000000000000000000"
      }' >> ${PWD}/genesis.json
    else
     echo ',
    "'"${accountArray[$accountNum]}"'": {
      "balance": "1000000000000000000000000000"
      }' >> ${PWD}/genesis.json
    fi
   done
    echo '
  },
  "coinbase": "0x0000000000000000000000000000000000000000",
  "config": {
    "homesteadBlock": 0,
    "byzantiumBlock": 0,
    "constantinopleBlock": 0,
    "petersburgBlock": 0,
    "istanbulBlock": 0,
    "chainId": 10,
    "eip150Block": 0,
    "eip155Block": 0,
    "eip150Hash": "0x0000000000000000000000000000000000000000000000000000000000000000",
    "eip158Block": 0,
    "isQuorum": true,
    "isMPS": '${isMPS}',
    "privacyEnhancementsBlock": 0,
    "maxCodeSizeConfig" : [
      {
        "block" : 0,
        "size" : 32
      }
    ]
  },
  "difficulty": "0x0",
  "extraData": "0x0000000000000000000000000000000000000000000000000000000000000000",
  "gasLimit": "0xE0000000",
  "mixhash": "0x00000000000000000000000000000000000000647572616c65787365646c6578",
  "nonce": "0x0",
  "parentHash": "0x0000000000000000000000000000000000000000000000000000000000000000",
  "timestamp": "0x00"
}' >> ${PWD}/genesis.json
}

#######################################################
############### Generate Enode Keys ###################
#######################################################
function generateEnodeKeys() {
 numOfTesseraNodes=$1
 enodeArr=
 for i in `seq 1 ${numOfTesseraNodes}`
 do
    echo -e "${PASS}[*] Entering node${i} directory${NC}"
    pushd ${PWD}/nodes/node${i}
      $GETH/bootnode --genkey=node${i}key
      enodeID=`$GETH/bootnode --nodekey=node${i}key --writeaddress`
      enodeArr=(${enodeArr[@]} `echo $enodeID`)
    popd
 done
}

######################################################
############# Generate Permissioned Nodes ############
######################################################
function generatePermissionedNodes() {
 touch ${PWD}/permissioned-nodes.json
echo '[' >> permissioned-nodes.json
 for (( enodeNum=0; enodeNum < ${#enodeArr[@]}; enodeNum++ ))
  do
   if [[ $enodeNum -eq 0 ]]; then
    echo '"enode://'${enodeArr[$enodeNum]}'@127.0.0.1:2100'${enodeNum}'?discport=0&raftport=5040'$((enodeNum+1))'"' >> permissioned-nodes.json
   else
    echo ',"enode://'${enodeArr[$enodeNum]}'@127.0.0.1:2100'${enodeNum}'?discport=0&raftport=5040'$((enodeNum+1))'"' >> permissioned-nodes.json
   fi
 done
echo ']' >> permissioned-nodes.json

 permNodesFile=./permissioned-nodes.json
 numPermissionedNodes=`grep "enode" ${permNodesFile} |wc -l`
 if [[ $numPermissionedNodes -ne ${numOfTesseraNodes} ]]; then
    echo -e "${ERROR}ERROR: $numPermissionedNodes nodes are configured in 'permissioned-nodes.json', but expecting configuration for ${numOfTesseraNodes} nodes${NC}"
    rm -f $permNodesFile
    exit -1
 fi
}

#################################################################################################
#################################### Start Raft Nodes ###########################################
#################################################################################################
function performValidation() {
    # Warn the user if chainId is same as Ethereum main net (see https://github.com/jpmorganchase/quorum/issues/487)
    genesisFile=$1
    NETWORK_ID=$(cat $genesisFile | tr -d '\r' | grep chainId | awk -F " " '{print $2}' | awk -F "," '{print $1}')

    if [ $NETWORK_ID -eq 1 ]
    then
        echo -e "${ERROR}  Quorum should not be run with a chainId of 1 (Ethereum mainnet)${NC}"
        echo -e "${ERROR}  please set the chainId in the $genesisFile to another value ${NC}"
        echo -e "${ERROR}  1337 is the recommend ChainId for Geth private clients.${NC}"
    fi

    # Check that the correct geth executable is on the path
    set +e
 
    GETH_VERSION=`$GETH/geth version |grep -i "Quorum Version"`
    if [ "$GETH_VERSION" == "" ]; then
        echo -e "${ERROR}ERROR: you appear to be running with upstream geth. Ensure that Quorum geth is on the PATH (before any other geth version).${NC}"
        exit -1
    fi
    echo -e "${PASS}Found geth: \"$GETH_VERSION\"${NC}"
    set -e
}

#TODO::UNDER CONSTRUCTION
function gethPluginSelfSignedTLS() {
 DDIR=$1
 node=$(basename ${DDIR})
 Node=${node^}
 #TODO:: add input for type of EC key and certificate subject options
 echo -e "${PASS}[*] Generating Certificate Authority (CA) for Quorum ${Node}${NC}"
 mkdir -p ${DDIR}/tls
 pushd ${DDIR}/tls
   echo "${Node}password" > password.enc
   $OPENSSL/openssl genpkey -out key.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
   $OPENSSL/openssl req -new -x509 -nodes -key key.pem -sha256 -days 1024 -out cert.pem --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorum${Node}CA/OU=Quorum${Node}CA/CN=localhost" -addext "subjectAltName = DNS:localhost, IP:127.0.0.1" -passin file:password.enc
   $OPENSSL/openssl ec -in key.pem -out keyUnlocked.pem -passin file:password.enc
 popd
}

function gethPluginSettings() {
 DDIR=$1
 CLIENT_ID=$2
 CLIENT_SECRET=$3
 
 if [ ! -d "${DDIR}" ]; then
    echo -e "${RED}[x] Directory ${DDIR} does not exists."
    exit
 fi
 
 GethNode=$(basename ${DDIR})
 
 PluginPath="${currentDir}/security"
 cat <<EOF > ${DDIR}/geth-plugin-settings-${GethNode}.json
{
    "baseDir": "${PluginPath}",
    "providers": {
        "security": {
            "name": "quorum-security-plugin-enterprise",
            "version": "0.0.0",
            "config": "file://${DDIR}/security-config.json"
        }
    }

}
EOF

 cat <<EOF > ${DDIR}/security-config.json
{
  "tls": {
    "auto": false,
    "certFile": "${DDIR}/tls/cert.pem",
    "keyFile": "${DDIR}/tls/keyUnlocked.pem",
    "advanced": {
      "cipherSuites": [
        "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"
      ]
    }
  },
  "tokenValidation": {
    "issuers": ["https://goquorum.com/oauth/"],
    "cache": {
      "limit": 80,
      "expirationInSeconds": 3600
    },
    "introspect": {
      "endpoint": "https://hydra:4445/oauth2/introspect",
      "authentication": {
        "method": "client_secret_basic",
        "credentials": {
          "clientId": "${CLIENT_ID}",
          "clientSecret": "${CLIENT_SECRET}"
        }
      },
      "tlsConnection": {
        "insecureSkipVerify": true
      }
    },
    "jws": {
      "endpoint": "https://hydra:4444/.well-known/jwks.json",
      "tlsConnection": {
        "insecureSkipVerify": true
      }
    },
    "jwt": {
      "authorizationField": "scope",
      "preferIntrospection": true
    }
  }
}
EOF
}

function hydra() {
  CLIENT_ID=$1
  CLIENT_SECRET=$2
  SCOPE=$3
  i=$4
 
  AUDIENCE="Node${i}"
    
  $DOCKER/docker run --rm -it -e HYDRA_ADMIN_URL=https://hydra:4445 --network hydra-Network oryd/hydra:latest clients create --skip-tls-verify --id ${CLIENT_ID} --secret ${CLIENT_SECRET} -g client_credentials --audience ${AUDIENCE} --scope "${SCOPE}"
  local TOKEN=$($DOCKER/docker run --rm -it --network hydra-Network oryd/hydra:latest token client --endpoint https://hydra:4444 --skip-tls-verify --client-id ${CLIENT_ID} --client-secret ${CLIENT_SECRET} --audience ${AUDIENCE} --scope "${SCOPE}")
  
  echo "${TOKEN}"
}


#TODO::UNDER CONSTRUCTION

function raftStart() {
 numOfTesseraNodes=$1
 tls=$2
 influxDB=$3
 multitenancy=$4
 numOfTenants=$5
 
 currentDir=`pwd`
 
 #Use https if tls variable is set to STRICT
 http=http
 if [[ $tls == "STRICT" ]]; then
     echo -e "${PASS}[*] TLS is used for secure communications between Quorum Nodes${NC}"
     echo -e "${PASS}[*] TLS is used for secure RPC-JSON communications to the Quorum Nodes${NC}"
     http=https
 fi
 
 echo -e "${PASS}[*] Starting $numOfTesseraNodes Ethereum nodes with ChainID and NetworkId of $NETWORK_ID${NC}"
 QUORUM_GETH_ARGS=${QUORUM_GETH_ARGS:-}

 #check geth version and if it is below 1.9 then dont include allowSecureUnlock
 allowSecureUnlock=
 chk=`$GETH/geth help | grep "allow-insecure-unlock" | wc -l`
 if (( $chk == 1 )); then
    allowSecureUnlock="--allow-insecure-unlock"
 fi

 basePort=21000
 baseRpcPort=22000
 baseRaftPort=50401
 baseTlsPort=9280
 baseInfluxdbPort=8085
 
 for i in `seq 1 ${numOfTesseraNodes}`
  do
     echo -e "${PASS}[*] Starting Quorum Node ${i} ...${NC}"
     
     DDIR="${currentDir}/nodes/node${i}"
     
     #TODO::Local verify or Remote Verify of the plugin
     #JSON-RPC security implemention in GoQuorum Node
     PLUGINS_ARGS=
     if [[ $tls == "STRICT" ]]; then
         CLIENT_ID="QuorumNode${i}"
         CLIENT_SECRET="QuorumNode${i}SecretPassword"
         SCOPE="rpc://rpc_modules,rpc://eth_*,rpc://admin_nodeInfo"
         
         gethPluginSelfSignedTLS ${DDIR}
         
         TOKEN=$(hydra ${CLIENT_ID} ${CLIENT_SECRET} ${SCOPE} ${i})
         echo -e "${WARNING}###########################################################################${NC}"
         echo -e "${PASS}Quorum Node${i} TOKEN: ${TOKEN}${NC}"
         echo -e "${WARNING}###########################################################################${NC}"
         gethPluginSettings ${DDIR} ${CLIENT_ID} ${CLIENT_SECRET}
         
         PLUGINS_ARGS="--plugins file://${DDIR}/geth-plugin-settings-$(basename ${DDIR}).json --plugins.skipverify"
     fi
     
     MULTITENANCY_ARGS=
     if [[ $tls == "STRICT" ]] && [[ $multitenancy == "ON" ]]; then
        MULTITENANCY_ARGS="--multitenancy"
        
        for j in `seq 1 ${numOfTenants}`
        do
          CLIENT_ID="PS${i}${j}"
          CLIENT_SECRET="PS${i}${j}SecretPassword"
          SCOPE="rpc://rpc_modules,rpc://quorumExtension_*,rpc://eth_*,psi://PS${i}${j}?self.eoa=0x0&node.eoa=0x0"
          
          TOKEN=$(hydra ${CLIENT_ID} ${CLIENT_SECRET} ${SCOPE} ${i})
          echo -e "${WARNING}###########################################################################${NC}"
          echo -e "${PASS}Quorum Node${i} TOKEN for Tenant${j}: ${TOKEN}${NC}"
          echo -e "${WARNING}###########################################################################${NC}"
        done
        
     fi
     #TODO::############################################
     
     GETH_ARGS="--nodiscover --nousb ${allowSecureUnlock} --verbosity ${verbosity} --networkid $NETWORK_ID --raft --raftblocktime ${blockTime} --http --http.corsdomain=* --http.vhosts=* --http.addr 0.0.0.0 --http.api admin,debug,web3,eth,txpool,personal,ethash,miner,net,raft,quorumExtension --emitcheckpoints --unlock 0 --password ${DDIR}/passwords.txt $QUORUM_GETH_ARGS"
    
    port=$(($basePort + ${i} - 1))
    rpcPort=$(($baseRpcPort + ${i} - 1))
    raftPort=$(($baseRaftPort + ${i} - 1))
    ptmTlsPort=$(($baseTlsPort + ${i}))
    influxdbPort=$(($baseInfluxdbPort + ${i}))
    
    #Connect to Tessera with HTTPS
    TLS_ARGS="--ptm.url ${http}://127.0.0.1:${ptmTlsPort}"
    if [[ $tls == "STRICT" ]]; then  
       QUORUM_TLS_CLIENT_DIR="${currentDir}/nodes/tessera${i}/tls/Q2T/client"
        
       TLS_ARGS="${TLS_ARGS} --ptm.tls.mode strict --ptm.tls.rootca ${QUORUM_TLS_CLIENT_DIR}/CAcert.pem --ptm.tls.clientcert ${QUORUM_TLS_CLIENT_DIR}/clientCert.pem --ptm.tls.clientkey ${QUORUM_TLS_CLIENT_DIR}/clientsKey.pem"
    fi
    
    #TODO::Geth does not handle tls connections with influxdb
    INFLUXDB_ARGS=
    if [[ $influxDB == "ON" ]] && [[ $tls != "STRICT" ]]; then
       INFLUXDB_ARGS="--metrics --metrics.influxdb --metrics.influxdb.endpoint ${http}://127.0.0.1:${influxdbPort} --metrics.influxdb.database GoQuorum"    
    fi 
    #TODO::#############################################################
    
    permissioned="--permissioned"

    $GETH/geth --identity "Node${i}" --datadir ${DDIR} ${GETH_ARGS} ${TLS_ARGS} ${INFLUXDB_ARGS} ${PLUGINS_ARGS} ${MULTITENANCY_ARGS} ${permissioned} --raftport ${raftPort} --http.port ${rpcPort} --port ${port} 2>>nodes/logs/node${i}.log &
    echo -e "${PASS}[*] Starting Quorum Node ${i} ...OK${NC}"
 done
 sleep 5
 echo -e "${PASS}[*] The private Ethereum Network consisting of $numOfTesseraNodes with ChainID and NetworkId of $NETWORK_ID has started.${NC}"
}
#################################################################################################
###################################### Start Tessera Nodes ######################################
#################################################################################################
function tesseraVersion() {
 tesseraJar=$1
 #extract the tessera version from the jar
 TESSERA_VERSION=$(unzip -p $tesseraJar META-INF/MANIFEST.MF | grep Tessera-Version | cut -d" " -f2)
 echo -e "${PASS}Tessera version (extracted from manifest file): $TESSERA_VERSION${NC}"

 TESSERA_CONFIG_TYPE="-09-"

 #if the Tessera version is 0.10, use this config version
 if [ "$TESSERA_VERSION" \> "0.10" ] || [ "$TESSERA_VERSION" == "0.10" ]; then
    TESSERA_CONFIG_TYPE="-09-"
 fi

 echo -e "${PASS}Config type $TESSERA_CONFIG_TYPE${NC}"
}

function tesseraStart() {
 tesseraJar=$1
 remoteDebug=$2
 jvmParams=$3
 numOfTesseraNodes=$4
 tls=$5

 tesseraVersion ${tesseraJar}

 http=http
 if [[ $tls == "STRICT" ]]; then
     http=https
 fi
 
 echo -e "${PASS}[*] Starting $numOfTesseraNodes Tessera node(s)${NC}"

 currentDir=`pwd`
 for i in `seq 1 ${numOfTesseraNodes}`
 do
    echo -e "${PASS}[*] Starting Tessera Node ${i} ...${NC}"
    
    DDIR="${currentDir}/nodes/tessera${i}"

    DEBUG=""
    if [ "${remoteDebug}" == "true" ]; then
      DEBUG="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=500${i} -Xdebug"
    fi

    #Only set heap size if not specified on command line
    MEMORY=
    if [[ ! "${jvmParams}" =~ "Xm" ]]; then
      MEMORY="-Xms128M -Xmx128M"
    fi
    
    CMD="java ${jvmParams} ${DEBUG} ${MEMORY} -jar ${tesseraJar} -configfile ${DDIR}/tessera-config${TESSERA_CONFIG_TYPE}${i}.json"
    echo "$CMD >> nodes/logs/tessera${i}.log 2>&1 &"
    ${CMD} >> "nodes/logs/tessera${i}.log" 2>&1 &
    sleep 5
    echo -e "${PASS}[*] Starting Tessera Node ${i} ...OK${NC}"
 done

 echo -e "${WARNING}Waiting until all Tessera nodes are running...${NC}"
 DOWN=true
 CURL_ARGS=
 k=10
 while ${DOWN}; do
    sleep 1
    DOWN=false
    for i in `seq 1 ${numOfTesseraNodes}`
    do
 
        set +e
        CURL_ARGS="${http}://localhost:900${i}/upcheck"
        #NOTE: if using https, change the scheme
        if [[ ${tls} == "STRICT" ]]; then
          CLIENT_TLSDIR="${PWD}/nodes/tessera${i}/tls/P2P/client"
          CURL_ARGS="--cacert ${CLIENT_TLSDIR}/CAcert.pem --key ${CLIENT_TLSDIR}/clientKey.pem --pass $(cat ${CLIENT_TLSDIR}/password.enc) --cert ${CLIENT_TLSDIR}/clientCert.pem ${CURL_ARGS}" 
        fi
        
        #NOTE: if using the IP whitelist, change the host to an allowed host
        result=$($CURL/curl -s ${CURL_ARGS})
        set -e
        if [ ! "${result}" == "I'm up!" ]; then
            echo -e "${WARNING}Node ${i} is not yet listening on http.${NC}"
            DOWN=true
        fi
    done
    
    k=$((k - 1))
    if [ ${k} -le 0 ]; then
        echo -e "${WARNING}Tessera is taking a long time to start.  Look at the Tessera logs in nodes/logs/ for help diagnosing the problem.${NC}"
    fi
    echo -e "${WARNING}Waiting until all Tessera nodes are running...${NC}"
 done

 echo -e "${PASS}[*] All Tessera Nodes are started.${NC}"
}
#################################################################################################
################################# Tessera Start Remote ##########################################
#################################################################################################
function tesseraStartRemote() {
 tesseraJar=$1
 enclaveJar=$2
 remoteDebug=$3
 jvmParams=$4
 numberOfRemoteEnclaves=$5
 tls=$6
 
 tesseraVersion ${tesseraJar}
 
 http=http
 if [[ $tls == "STRICT" ]]; then
     http=https
 fi
 
 echo -e "${PASS}[*] Start up all remote enclaves${NC}"
 for ((i=1;i<=numberOfRemoteEnclaves;i++));
  do
    echo -e "${PASS}[*] Starting Enclave Node ${i} ...OK${NC}"
    DDIR="${currentDir}/nodes/tessera${i}"

    DEBUG=""
    if [ "${remoteDebug}" == "true" ]; then
      DEBUG="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=501${i} -Xdebug"
    fi

    #Only set heap size if not specified on command line
    MEMORY=
    if [[ ! "${jvmParams}" =~ "Xm" ]]; then
      MEMORY="-Xms128M -Xmx128M"
    fi

    CMD="java ${jvmParams} ${DEBUG} ${MEMORY} -jar ${enclaveJar} -configfile ${DDIR}/enclave${TESSERA_CONFIG_TYPE}${i}.json"
    echo "$CMD >> nodes/logs/enclave${i}.log 2>&1 &"
    ${CMD} >> "nodes/logs/enclave${i}.log" 2>&1 &
    sleep 5
    echo -e "${PASS}[*] Starting Enclave Node ${i} ...OK"
 done

 echo -e "${PASS}[*] Waiting until all Tessera enclaves are running...${NC}"
 DOWN=true
 CURL_ARGS=
 k=10
 while ${DOWN}; do
    sleep 1
    DOWN=false
    for ((i=1;i<=numberOfRemoteEnclaves;i++));
    do
        set +e
        CURL_ARGS="${http}://localhost:918${i}/ping"
        #NOTE: if using https, change the scheme
        if [[ ${tls} == "STRICT" ]]; then
          CLIENT_TLSDIR="${PWD}/nodes/tessera${i}/tls/ENCLAVE/client"
          CURL_ARGS="--cacert ${CLIENT_TLSDIR}/CAcert.pem --key ${CLIENT_TLSDIR}/clientKey.pem --pass $(cat ${CLIENT_TLSDIR}/password.enc) --cert ${CLIENT_TLSDIR}/clientCert.pem ${CURL_ARGS}" 
        fi
        
        result=$($CURL/curl -s ${CURL_ARGS})
        set -e
        if [ ! "${result}" == "STARTED" ]; then
            echo -e "${WARNING}Enclave ${i} is not yet listening on http${NC}"
            DOWN=true
        fi
    done

    k=$((k - 1))
    if [ ${k} -le 0 ]; then
        echo -e "${WARNING}Tessera is taking a long time to start.  Look at the Tessera logs in nodes/logs/ for help diagnosing the problem.${NC}"
    fi
    echo -e "${WARNING}Waiting until all Tessera enclaves are running...${NC}"

    sleep 5
 done
 echo -e "${PASS}[*] All Tessera Enclave Nodes are started${NC}"
 
 echo -e "${PASS}[*] Startup all Remote Enclave Transaction Managers${NC}"
 for ((i=1;i<=numberOfRemoteEnclaves;i++));
 do
    echo -e "${PASS}[*] Starting Tessera Node ${i} ...OK${NC}"
    DDIR="${currentDir}/nodes/tessera${i}"
    
    DEBUG=""
    if [ "${remoteDebug}" == "true" ]; then
      DEBUG="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=500${i} -Xdebug"
    fi

    #Only set heap size if not specified on command line
    MEMORY=
    if [[ ! "${jvmParams}" =~ "Xm" ]]; then
      MEMORY="-Xms128M -Xmx128M"
    fi

    CMD="java ${jvmParams} ${DEBUG} ${MEMORY} -jar ${tesseraJar} -configfile ${DDIR}/tessera-config-enclave${TESSERA_CONFIG_TYPE}${i}.json"
    echo "$CMD >> nodes/logs/tessera${i}.log 2>&1 &"
    ${CMD} >> "nodes/logs/tessera${i}.log" 2>&1 &
    sleep 5
    echo -e "${PASS}[*] Starting Tessera Node ${i} ...OK${NC}"
 done
 
 echo -e "${PASS}[*] Waiting until all Tessera nodes are running...${NC}"
 DOWN=true
 CURL_ARGS=
 k=10
 while ${DOWN}; do
    sleep 1
    DOWN=false
    for ((i=1;i<=numberOfRemoteEnclaves;i++));
    do

        set +e
        CURL_ARGS="${http}://localhost:900${i}/upcheck"
        #NOTE: if using https, change the scheme
        if [[ ${tls} == "STRICT" ]]; then
          CLIENT_TLSDIR="${PWD}/nodes/tessera${i}/tls/P2P/client"
          CURL_ARGS="--cacert ${CLIENT_TLSDIR}/CAcert.pem --key ${CLIENT_TLSDIR}/clientKey.pem --pass $(cat ${CLIENT_TLSDIR}/password.enc) --cert ${CLIENT_TLSDIR}/clientCert.pem ${CURL_ARGS}" 
        fi
        
        result=$(curl -s ${CURL_ARGS})
        set -e
        if [ ! "${result}" == "I'm up!" ]; then
            echo -e "${WARNING}Node ${i} is not yet listening on http${NC}"
            DOWN=true
        fi
    done

    k=$((k - 1))
    if [ ${k} -le 0 ]; then
        echo -e "${WARNING}Tessera is taking a long time to start.  Look at the Tessera logs in nodes/logs/ for help diagnosing the problem.${NC}"
    fi
    echo -e "${WARNING}Waiting until all Tessera nodes are running...${NC}"

    sleep 5
 done

 echo -e "${PASS}[*] All Tessera Nodes are started${NC}"
}
#################################################################################################
#################################################################################################
#################################################################################################
function usage() {
  echo ""
  echo "Usage:"
  echo "    $0 [tessera | tessera-remote ] [--blockPeriod blockPeriod] [--verbosity verbosity] [--numOfTesseraNodes numberOfNodes] [--encryptorType typeOfEncryption] [--tls] [--remoteDebug] [--jvmParams \"JVM parameters\"]"
  echo ""
  echo "Where:"
  echo "tessera | tessera-remote (default = tessera): specifies which privacy implementation to use"
  echo ""
  echo "    --blockPeriod: raftblocktime (default = $blockTime) ms"
  echo "    --verbosity: verbosity for logging (default = $verbosity)"
  echo "    --numOfTesseraNodes specifies the number of Tessera Nodes to be initialized (default = $numOfTesseraNodes)"
  echo "    --encryptorType specifies the type of encryption that Tessera nodes use in order to encrypt/decrypt private payloads. Options are NACL, EC, CUSTOM (default = $encryptorType)"
  echo "    --remoteDebug enables remote debug on port 500n for each Tessera node (for use with JVisualVm etc)"
  echo "    --jvmParams specifies parameters to be used by JVM when running Tessera"
  echo "    --tls enables TLS communications between Tessera components (default = $tls)"
  echo "    --influxDB enables the Influx DB server to store Tessera Node metrics (default = $influxDB)"
  echo ""
  echo "  - Tessera jar location defaults to ${defaultTesseraJarPath};"
  echo "  - Enclave jar location defaults to ${defaultEnclaveJarPath};"
  echo "    However, this can be overridden by environment variable TESSERA_JAR or ENCLAVE_JAR or by the command line option."
  echo ""
  exit -1
}

PASS="\033[0;32m"
ERROR="\033[0;31m"
WARNING="\033[0;33m"
NC="\033[0m" # No Color

numOfTesseraNodes=4
privacyImpl=tessera
tesseraJar=
enclaveJar=
encryptorType=${ENCRYPTOR_TYPE:-NACL}
blockTime=50
verbosity=3
remoteDebug=false
jvmParams=
tls=OFF
influxDB=OFF
multitenancy=OFF
numOfTenants=1 #In case of multitenancy, it must be declared. Otherwise it will consider it as single tenant.
defaultTesseraJarPath="${PWD}/tessera/tessera.jar"
defaultEnclaveJarPath="${PWD}/tessera/enclave.jar"


while (( "$#" )); do
    case "$1" in
        tessera)
            privacyImpl=tessera
            shift
            ;;
        tessera-remote)
            privacyImpl="tessera-remote"
            shift
            ;;
        --blockPeriod)
            blockTime=$2
            shift 2
            ;;
        --verbosity)
            verbosity=$2
            shift 2
            ;;
        --numOfTesseraNodes)
            re='^[0-9]+$'
            if ! [[ $2 =~ $re ]] ; then
                echo -e "${ERROR}ERROR: numOfTesseraNodes value must be a number${NC}"
                usage
            fi
            numOfTesseraNodes=$2
            shift 2
            ;;
        --encryptorType)
            if ! [[ $2 =~ ^(NACL|EC)$ ]]; then
                echo -e "${ERROR}ERROR: typeOfEncryption value must be either NACL or EC${NC}"
                usage
            fi
            encryptorType=$2
            shift 2
            ;;
        --remoteDebug)
           remoteDebug=true
           shift
           ;;
        --jvmParams)
           jvmParams=$2
           shift 2
           ;;
        --tls)
           tls=STRICT
           shift
           ;;
        --influxDB)
           influxDB=ON
           shift
           ;;
        --multitenancy)
           multitenancy=ON
           numOfTenants=$2
           shift 2
           ;;
        --help)
           shift
           usage
           ;;
        *)
           echo -e "${ERROR}Error: Unsupported command line parameter $1${NC}"
           usage
           ;;
    esac
done

####################################################
############ Resolve Tessera Path ##################
####################################################
set +e
defaultTesseraJar=`find ${defaultTesseraJarPath} 2>/dev/null`
set -e
if [[ "${TESSERA_JAR:-unset}" == "unset" ]]; then
   echo -e "${PASS}[*] Tessera Found at: ${defaultTesseraJar}${NC}"
   tesseraJar=${defaultTesseraJar}
else
   tesseraJar=${TESSERA_JAR}
fi

if [  "${tesseraJar}" == "" ]; then
   echo -e "${ERROR}[x] Unable to find Tessera jar file using TESSERA_JAR envvar, or using ${defaultTesseraJarPath}${NC}"
   exit -1
elif [  ! -f "${tesseraJar}" ]; then
   echo -e "${ERROR}[x] Unable to find Tessera jar file: ${tesseraJar}${NC}"
   exit -1
fi

####################################################
############ Resolve Enclave Path ##################
####################################################
if [ "$privacyImpl" == "tessera-remote" ]; then
   set +e
   defaultEnclaveJar=`find ${defaultEnclaveJarPath} 2>/dev/null`
   set -e
   if [[ "${ENCLAVE_JAR:-unset}" == "unset" ]]; then
      echo -e "${PASS}[*] Enclave Found at: ${defaultEnclaveJar}${NC}"
      enclaveJar=${defaultEnclaveJar}
   else
      enclaveJar=${ENCLAVE_JAR}
   fi
   
   if [  "${enclaveJar}" == "" ]; then
      echo -e "${ERROR}[x] Unable to find Enclave jar file using ENCLAVE_JAR envvar, or using ${defaultEnclaveJarPath}${NC}"
      usage
   elif [  ! -f "${enclaveJar}" ]; then
      echo -e "${ERROR}[x] Unable to find Enclave jar file: ${enclaveJar}${NC}"
      usage
   fi
fi

if [ "$tls" == "STRICT" ]; then
   echo -e "${PASS}[*] Searching whether OpenSSL is installed${NC}"
   set +e
   OpenSSLPath=`which openssl`
   set -e
   OPENSSL=
   if [[ ! -z "$OpenSSLPath" ]]; then
      echo -e "${PASS}[*] OpenSSL Found${NC}"
      echo -e "${PASS}[*] Checking the OpenSSL Version${NC}"
      if [[ `$OpenSSLPath version` == "OpenSSL 1.1.1k  25 Mar 2021" ]]; then
         echo -e "${PASS}[*] OpenSSL has the latest $($OpenSSLPath version) version${NC}"
         OPENSSL=`dirname $OpenSSLPath`
      fi
   else
      echo -e "${ERROR}[x] OPENSSL is not installed. Exited...${NC}"
      exit
   fi
   
   echo -e "${PASS}[*] Searching whether Keytool is installed${NC}"
   set +e
   KeytoolPath=`which keytool`
   set -e
   KEYTOOL=
   if [[ ! -z "$KeytoolPath" ]]; then
      echo -e "${PASS}[*] Keytool Found.${NC}"
      KEYTOOL=`dirname $KeytoolPath`
   else
      echo -e "${ERROR}[x] Keytool is not installed. Exited...${NC}"
      exit
   fi
fi

echo -e "${PASS}[*] Searching whether Geth is installed${NC}"
set +e
GethPath=`which geth`
set -e
GETH=
if [[ ! -z "$GethPath" ]]; then
    echo -e "${PASS}[*] Geth Found${NC}"
    GETH=`dirname $GethPath`
else
    echo -e "${ERROR}[x] Geth Not Found...${NC}"
    echo -e "${PASS}[*] Attempting to retrieve Geth from Local Directory${NC}"
    if [[ -d "${PWD}/geth" ]]; then
       echo -e "${PASS}[*] Geth Found. Retrieved Geth from Local Directory${NC}"
       GETH=${PWD}/geth 
    else
       echo -e "${ERROR}[x] Geth Not Found anywhere. Exited...${NC}"
       exit
    fi
fi

echo -e "${PASS}[*] Searching whether Curl is installed${NC}"
set +e
CurlPath=`which curl`
set -e
CURL=
if [[ ! -z "$CurlPath" ]]; then
    echo -e "${PASS}[*] Curl Found${NC}"
    CURL=`dirname $CurlPath`
else
    echo -e "${ERROR}[x] Curl Not Installed. Exited...${NC}"
    exit
fi


echo -e "${PASS}[*] Searching whether Docker is installed${NC}"
set +e
DockerPath=`which docker`
set -e
DOCKER=
if [[ ! -z "$DockerPath" ]]; then
   echo -e "${PASS}[*] Docker Found${NC}"
   DOCKER=`dirname $DockerPath`
else
   echo -e "${ERROR}[x] Docker Not Installed. Exited...${NC}"
   exit
fi

echo -e "${PASS}[*] Searching whether Docker-Compose is installed${NC}"
set +e
DockerComposePath=`which docker-compose`
set -e
DOCKER_COMPOSE=
if [[ ! -z "$DockerComposePath" ]]; then
   echo -e "${PASS}[*] Docker-Compose Found${NC}"
   DOCKER_COMPOSE=`dirname $DockerComposePath`
else
   echo -e "${ERROR}[x] Docker-Compose Not Installed. Exited...${NC}"
   exit
fi

if [ "$tls" == "STRICT" ]; then
   echo -e "${PASS}[*] Starting Hydra Authentication Server${NC}"
   ./hydra.sh --tls
   echo -e "${PASS}[*] Starting Hydra Authentication Server ...OK${NC}"
fi

echo -e "${PASS}[*] Cleaning up temporary data directories${NC}"
rm -rf nodes genesis.json permissioned-nodes.json nohup.out passwords.txt
mkdir -p nodes/logs

echo -e "${PASS}[*] Configuring for $numOfTesseraNodes node(s)${NC}"
echo $numOfTesseraNodes > nodes/numberOfNodes

echo -e "${PASS}[*] Creating $numOfTesseraNodes Account(s)${NC}"
createAccounts ${numOfTesseraNodes}

echo -e "${PASS}[*] Creating Genesis File${NC}"
createGenesis ${multitenancy} "${createAccountArr[@]}" 

echo -e "${PASS}[*] Generating $numOfTesseraNodes Raft ENodes Keys${NC}"
generateEnodeKeys ${numOfTesseraNodes}

echo -e "${PASS}[*] Generating Permissioned Nodes${NC}"
generatePermissionedNodes "${enodeArr[@]}"

for i in `seq 1 ${numOfTesseraNodes}`
do
    cp ${permNodesFile} nodes/node${i}/permissioned-nodes.json
    cp ${permNodesFile} nodes/node${i}/static-nodes.json
    cp nodes/node${i}/node${i}key nodes/node${i}/geth/nodekey
    echo -e "${PASS}[*] Initialize Geth for node${i}${NC}"
    $GETH/geth --datadir nodes/node${i} init genesis.json
    echo -e "${PASS}[*] Initialization for node${i} is completed${NC}"
done

echo -e "${PASS}[*] All nodes are successfully initialized${NC}"

# Intialize Tessera configuraion
tesseraInit ${privacyImpl} ${tesseraJar} ${numOfTesseraNodes} ${encryptorType} ${tls} ${influxDB} ${multitenancy} ${numOfTenants}

# Perform any necessary validation
performValidation genesis.json

if [ "$privacyImpl" == "tessera" ]; then
  echo -e "${PASS}[*] Starting Tessera nodes${NC}"
  tesseraStart ${tesseraJar} ${remoteDebug} "${jvmParams}" ${numOfTesseraNodes} ${tls}
elif [ "$privacyImpl" == "tessera-remote" ]; then
  echo -e "${PASS}[*] Starting Remote Tessera nodes${NC}"
  tesseraStartRemote ${tesseraJar} ${enclaveJar} ${remoteDebug} "${jvmParams}" ${numOfTesseraNodes} ${tls}
else
  echo -e "${ERROR}[x] Unsupported privacy implementation: ${privacyImpl}${NC}"
  usage
fi

raftStart ${numOfTesseraNodes} ${tls} ${influxDB} ${multitenancy} ${numOfTenants}

exit
#Initialise Cakeshop configuration
./cakeshop-init.sh

rm -f $permNodesFile
