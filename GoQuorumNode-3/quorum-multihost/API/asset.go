// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package main

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// AssetAsset_Configuration_Public is an auto generated low-level Go binding around an user-defined struct.
type AssetAsset_Configuration_Public struct {
	DatasetID     string
	Configuration AssetConfiguration
	Policy        AssetPolicy
}

// AssetConfiguration is an auto generated low-level Go binding around an user-defined struct.
type AssetConfiguration struct {
	Price           string
	LicenseType     string
	StandardLicense string
	OwnLicense      string
}

// AssetPolicy is an auto generated low-level Go binding around an user-defined struct.
type AssetPolicy struct {
	Sector    []string
	Continent []string
	Country   []string
	Type      []string
	Size      []string
}

// AssetABI is the input ABI used to generate the binding from.
const AssetABI = "[{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_id\",\"type\":\"string\"}],\"name\":\"_isExists\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"DatasetID\",\"type\":\"string\"},{\"components\":[{\"internalType\":\"string\",\"name\":\"Price\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"LicenseType\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"StandardLicense\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"OwnLicense\",\"type\":\"string\"}],\"internalType\":\"structAsset.Configuration\",\"name\":\"configuration\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"string[]\",\"name\":\"Sector\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Continent\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Country\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Type\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Size\",\"type\":\"string[]\"}],\"internalType\":\"structAsset.Policy\",\"name\":\"policy\",\"type\":\"tuple\"}],\"internalType\":\"structAsset.Asset_Configuration_Public\",\"name\":\"_asset_Configuration_Public\",\"type\":\"tuple\"}],\"name\":\"create\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_id\",\"type\":\"string\"}],\"name\":\"read\",\"outputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"DatasetID\",\"type\":\"string\"},{\"components\":[{\"internalType\":\"string\",\"name\":\"Price\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"LicenseType\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"StandardLicense\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"OwnLicense\",\"type\":\"string\"}],\"internalType\":\"structAsset.Configuration\",\"name\":\"configuration\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"string[]\",\"name\":\"Sector\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Continent\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Country\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Type\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Size\",\"type\":\"string[]\"}],\"internalType\":\"structAsset.Policy\",\"name\":\"policy\",\"type\":\"tuple\"}],\"internalType\":\"structAsset.Asset_Configuration_Public\",\"name\":\"\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"readAll\",\"outputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"DatasetID\",\"type\":\"string\"},{\"components\":[{\"internalType\":\"string\",\"name\":\"Price\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"LicenseType\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"StandardLicense\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"OwnLicense\",\"type\":\"string\"}],\"internalType\":\"structAsset.Configuration\",\"name\":\"configuration\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"string[]\",\"name\":\"Sector\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Continent\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Country\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Type\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Size\",\"type\":\"string[]\"}],\"internalType\":\"structAsset.Policy\",\"name\":\"policy\",\"type\":\"tuple\"}],\"internalType\":\"structAsset.Asset_Configuration_Public[]\",\"name\":\"\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]"

var AssetParsedABI, _ = abi.JSON(strings.NewReader(AssetABI))

// AssetFuncSigs maps the 4-byte function signature to its string representation.
var AssetFuncSigs = map[string]string{
	"e54d592f": "_isExists(string)",
	"5d647bca": "create((string,(string,string,string,string),(string[],string[],string[],string[],string[])))",
	"616ffe83": "read(string)",
	"41f654f7": "readAll()",
}

// AssetBin is the compiled bytecode used for deploying new contracts.
var AssetBin = "0x60806040526000805534801561001457600080fd5b50611b06806100246000396000f3fe608060405234801561001057600080fd5b506004361061004c5760003560e01c806341f654f7146100515780635d647bca1461006f578063616ffe8314610084578063e54d592f146100a4575b600080fd5b6100596100c4565b6040516100669190611a27565b60405180910390f35b61008261007d366004611743565b6108c2565b005b610097610092366004611708565b610a78565b6040516100669190611a92565b6100b76100b2366004611708565b611252565b6040516100669190611a87565b60606000805467ffffffffffffffff811180156100e057600080fd5b5060405190808252806020026020018201604052801561011a57816020015b6101076112b4565b8152602001906001900390816100ff5790505b50905060005b6000548110156108bc576000818152600160208181526040928390208351815460029481161561010002600019011693909304601f8101839004909202830160809081019094526060830182815292939092849290918491908401828280156101ca5780601f1061019f576101008083540402835291602001916101ca565b820191906000526020600020905b8154815290600101906020018083116101ad57829003601f168201915b5050505050815260200160018201604051806080016040529081600082018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561027c5780601f106102515761010080835404028352916020019161027c565b820191906000526020600020905b81548152906001019060200180831161025f57829003601f168201915b50505050508152602001600182018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561031e5780601f106102f35761010080835404028352916020019161031e565b820191906000526020600020905b81548152906001019060200180831161030157829003601f168201915b5050509183525050600282810180546040805160206001841615610100026000190190931694909404601f810183900483028501830190915280845293810193908301828280156103b05780601f10610385576101008083540402835291602001916103b0565b820191906000526020600020905b81548152906001019060200180831161039357829003601f168201915b505050918352505060038201805460408051602060026001851615610100026000190190941693909304601f81018490048402820184019092528181529382019392918301828280156104445780601f1061041957610100808354040283529160200191610444565b820191906000526020600020905b81548152906001019060200180831161042757829003601f168201915b5050505050815250508152602001600582016040518060a001604052908160008201805480602002602001604051908101604052809291908181526020016000905b828210156105315760008481526020908190208301805460408051601f600260001961010060018716150201909416939093049283018590048502810185019091528181529283018282801561051d5780601f106104f25761010080835404028352916020019161051d565b820191906000526020600020905b81548152906001019060200180831161050057829003601f168201915b505050505081526020019060010190610486565b50505050815260200160018201805480602002602001604051908101604052809291908181526020016000905b828210156106095760008481526020908190208301805460408051601f60026000196101006001871615020190941693909304928301859004850281018501909152818152928301828280156105f55780601f106105ca576101008083540402835291602001916105f5565b820191906000526020600020905b8154815290600101906020018083116105d857829003601f168201915b50505050508152602001906001019061055e565b50505050815260200160028201805480602002602001604051908101604052809291908181526020016000905b828210156106e15760008481526020908190208301805460408051601f60026000196101006001871615020190941693909304928301859004850281018501909152818152928301828280156106cd5780601f106106a2576101008083540402835291602001916106cd565b820191906000526020600020905b8154815290600101906020018083116106b057829003601f168201915b505050505081526020019060010190610636565b50505050815260200160038201805480602002602001604051908101604052809291908181526020016000905b828210156107b95760008481526020908190208301805460408051601f60026000196101006001871615020190941693909304928301859004850281018501909152818152928301828280156107a55780601f1061077a576101008083540402835291602001916107a5565b820191906000526020600020905b81548152906001019060200180831161078857829003601f168201915b50505050508152602001906001019061070e565b50505050815260200160048201805480602002602001604051908101604052809291908181526020016000905b828210156108915760008481526020908190208301805460408051601f600260001961010060018716150201909416939093049283018590048502810185019091528181529283018282801561087d5780601f106108525761010080835404028352916020019161087d565b820191906000526020600020905b81548152906001019060200180831161086057829003601f168201915b5050505050815260200190600101906107e6565b5050505081525050815250508282815181106108a957fe5b6020908102919091010152600101610120565b50905090565b6040805160808082018352602080850180515184528051820151828501528051850151848601525160609081015181850152845160a081018652868601805151825280518401518285015280518701518288015280518301518284015251840151938101939093528451908101855285518152808201849052808501839052600080548152600183529490942084518051949593949192610968928492909101906112e0565b50602082810151805180519192600185019261098792849201906112e0565b5060208281015180516109a092600185019201906112e0565b50604082015180516109bc9160028401916020909101906112e0565b50606082015180516109d89160038401916020909101906112e0565b50505060408201518051805160058401916109f89183916020019061136c565b506020828101518051610a11926001850192019061136c565b5060408201518051610a2d91600284019160209091019061136c565b5060608201518051610a4991600384019160209091019061136c565b5060808201518051610a6591600484019160209091019061136c565b5050600080546001019055505050505050565b610a806112b4565b6000805b600054811015610ad757838051906020012060016000838152602001908152602001600020600001604051610ab991906119b7565b60405180910390201415610acf57809150610ad7565b600101610a84565b6000828152600160208181526040928390208351815460029481161561010002600019011693909304601f810183900490920283016080908101909452606083018281529293909284929091849190840182828015610b775780601f10610b4c57610100808354040283529160200191610b77565b820191906000526020600020905b815481529060010190602001808311610b5a57829003601f168201915b5050505050815260200160018201604051806080016040529081600082018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610c295780601f10610bfe57610100808354040283529160200191610c29565b820191906000526020600020905b815481529060010190602001808311610c0c57829003601f168201915b50505050508152602001600182018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610ccb5780601f10610ca057610100808354040283529160200191610ccb565b820191906000526020600020905b815481529060010190602001808311610cae57829003601f168201915b5050509183525050600282810180546040805160206001841615610100026000190190931694909404601f81018390048302850183019091528084529381019390830182828015610d5d5780601f10610d3257610100808354040283529160200191610d5d565b820191906000526020600020905b815481529060010190602001808311610d4057829003601f168201915b505050918352505060038201805460408051602060026001851615610100026000190190941693909304601f8101849004840282018401909252818152938201939291830182828015610df15780601f10610dc657610100808354040283529160200191610df1565b820191906000526020600020905b815481529060010190602001808311610dd457829003601f168201915b5050505050815250508152602001600582016040518060a001604052908160008201805480602002602001604051908101604052809291908181526020016000905b82821015610ede5760008481526020908190208301805460408051601f6002600019610100600187161502019094169390930492830185900485028101850190915281815292830182828015610eca5780601f10610e9f57610100808354040283529160200191610eca565b820191906000526020600020905b815481529060010190602001808311610ead57829003601f168201915b505050505081526020019060010190610e33565b50505050815260200160018201805480602002602001604051908101604052809291908181526020016000905b82821015610fb65760008481526020908190208301805460408051601f6002600019610100600187161502019094169390930492830185900485028101850190915281815292830182828015610fa25780601f10610f7757610100808354040283529160200191610fa2565b820191906000526020600020905b815481529060010190602001808311610f8557829003601f168201915b505050505081526020019060010190610f0b565b50505050815260200160028201805480602002602001604051908101604052809291908181526020016000905b8282101561108e5760008481526020908190208301805460408051601f600260001961010060018716150201909416939093049283018590048502810185019091528181529283018282801561107a5780601f1061104f5761010080835404028352916020019161107a565b820191906000526020600020905b81548152906001019060200180831161105d57829003601f168201915b505050505081526020019060010190610fe3565b50505050815260200160038201805480602002602001604051908101604052809291908181526020016000905b828210156111665760008481526020908190208301805460408051601f60026000196101006001871615020190941693909304928301859004850281018501909152818152928301828280156111525780601f1061112757610100808354040283529160200191611152565b820191906000526020600020905b81548152906001019060200180831161113557829003601f168201915b5050505050815260200190600101906110bb565b50505050815260200160048201805480602002602001604051908101604052809291908181526020016000905b8282101561123e5760008481526020908190208301805460408051601f600260001961010060018716150201909416939093049283018590048502810185019091528181529283018282801561122a5780601f106111ff5761010080835404028352916020019161122a565b820191906000526020600020905b81548152906001019060200180831161120d57829003601f168201915b505050505081526020019060010190611193565b50505091525050905250925050505b919050565b6000805b6000548110156112ab5782805190602001206001600083815260200190815260200160002060000160405161128b91906119b7565b604051809103902014156112a357600191505061124d565b600101611256565b50600092915050565b6040518060600160405280606081526020016112ce6113c5565b81526020016112db6113ed565b905290565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282611316576000855561135c565b82601f1061132f57805160ff191683800117855561135c565b8280016001018555821561135c579182015b8281111561135c578251825591602001919060010190611341565b5061136892915061141c565b5090565b8280548282559060005260206000209081019282156113b9579160200282015b828111156113b957825180516113a99184916020909101906112e0565b509160200191906001019061138c565b50611368929150611431565b6040518060800160405280606081526020016060815260200160608152602001606081525090565b6040518060a0016040528060608152602001606081526020016060815260200160608152602001606081525090565b5b80821115611368576000815560010161141d565b80821115611368576000611445828261144e565b50600101611431565b50805460018160011615610100020316600290046000825580601f106114745750611492565b601f016020900490600052602060002090810190611492919061141c565b50565b600082601f8301126114a5578081fd5b8135602067ffffffffffffffff8211156114bb57fe5b6114c88182840201611aac565b82815281810190858301855b858110156114fd576114eb898684358b010161150a565b845292840192908401906001016114d4565b5090979650505050505050565b600082601f83011261151a578081fd5b813567ffffffffffffffff81111561152e57fe5b611541601f8201601f1916602001611aac565b818152846020838601011115611555578283fd5b816020850160208301379081016020019190915292915050565b600060808284031215611580578081fd5b61158a6080611aac565b9050813567ffffffffffffffff808211156115a457600080fd5b6115b08583860161150a565b835260208401359150808211156115c657600080fd5b6115d28583860161150a565b602084015260408401359150808211156115eb57600080fd5b6115f78583860161150a565b6040840152606084013591508082111561161057600080fd5b5061161d8482850161150a565b60608301525092915050565b600060a0828403121561163a578081fd5b61164460a0611aac565b9050813567ffffffffffffffff8082111561165e57600080fd5b61166a85838601611495565b8352602084013591508082111561168057600080fd5b61168c85838601611495565b602084015260408401359150808211156116a557600080fd5b6116b185838601611495565b604084015260608401359150808211156116ca57600080fd5b6116d685838601611495565b606084015260808401359150808211156116ef57600080fd5b506116fc84828501611495565b60808301525092915050565b600060208284031215611719578081fd5b813567ffffffffffffffff81111561172f578182fd5b61173b8482850161150a565b949350505050565b600060208284031215611754578081fd5b813567ffffffffffffffff8082111561176b578283fd5b908301906060828603121561177e578283fd5b60405160608101818110838211171561179357fe5b6040528235828111156117a4578485fd5b6117b08782860161150a565b8252506020830135828111156117c4578485fd5b6117d08782860161156f565b6020830152506040830135828111156117e7578485fd5b6117f387828601611629565b60408301525095945050505050565b6000815180845260208085018081965082840281019150828601855b85811015611848578284038952611836848351611855565b9885019893509084019060010161181e565b5091979650505050505050565b60008151808452815b8181101561187a5760208185018101518683018201520161185e565b8181111561188b5782602083870101525b50601f01601f19169290920160200192915050565b6000606082518185526118b582860182611855565b9050602080850151868303828801528051608084526118d76080850182611855565b905082820151848203848601526118ee8282611855565b915050604080830151858303828701526119088382611855565b925050858301519250848203868601526119228284611855565b9450808801519250888503818a01528251915060a0855261194660a0860183611802565b9150838301518583038587015261195d8382611802565b94505080830151915084840381860152506119788382611802565b92505083810151838303858501526119908382611802565b945050608081015191505081830360808301526119ad8382611802565b9695505050505050565b60008083546001808216600081146119d657600181146119ed57611a1c565b60ff198316865260028304607f1686019350611a1c565b600283048786526020808720875b83811015611a145781548a8201529085019082016119fb565b505050860193505b509195945050505050565b6000602080830181845280855180835260408601915060408482028701019250838701855b82811015611a7a57603f19888603018452611a688583516118a0565b94509285019290850190600101611a4c565b5092979650505050505050565b901515815260200190565b600060208252611aa560208301846118a0565b9392505050565b60405181810167ffffffffffffffff81118282101715611ac857fe5b60405291905056fea2646970667358221220805fddcf1594f47c1bd73efb428de5f5dc1830226bebfdf36f4c2222975d0a8264736f6c63430007060033"

// DeployAsset deploys a new Ethereum contract, binding an instance of Asset to it.
func DeployAsset(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Asset, error) {
	parsed, err := abi.JSON(strings.NewReader(AssetABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}

	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(AssetBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Asset{AssetCaller: AssetCaller{contract: contract}, AssetTransactor: AssetTransactor{contract: contract}, AssetFilterer: AssetFilterer{contract: contract}}, nil
}

// Asset is an auto generated Go binding around an Ethereum contract.
type Asset struct {
	AssetCaller     // Read-only binding to the contract
	AssetTransactor // Write-only binding to the contract
	AssetFilterer   // Log filterer for contract events
}

// AssetCaller is an auto generated read-only Go binding around an Ethereum contract.
type AssetCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AssetTransactor is an auto generated write-only Go binding around an Ethereum contract.
type AssetTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AssetFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type AssetFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AssetSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type AssetSession struct {
	Contract     *Asset            // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// AssetCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type AssetCallerSession struct {
	Contract *AssetCaller  // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// AssetTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type AssetTransactorSession struct {
	Contract     *AssetTransactor  // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// AssetRaw is an auto generated low-level Go binding around an Ethereum contract.
type AssetRaw struct {
	Contract *Asset // Generic contract binding to access the raw methods on
}

// AssetCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type AssetCallerRaw struct {
	Contract *AssetCaller // Generic read-only contract binding to access the raw methods on
}

// AssetTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type AssetTransactorRaw struct {
	Contract *AssetTransactor // Generic write-only contract binding to access the raw methods on
}

// NewAsset creates a new instance of Asset, bound to a specific deployed contract.
func NewAsset(address common.Address, backend bind.ContractBackend) (*Asset, error) {
	contract, err := bindAsset(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Asset{AssetCaller: AssetCaller{contract: contract}, AssetTransactor: AssetTransactor{contract: contract}, AssetFilterer: AssetFilterer{contract: contract}}, nil
}

// NewAssetCaller creates a new read-only instance of Asset, bound to a specific deployed contract.
func NewAssetCaller(address common.Address, caller bind.ContractCaller) (*AssetCaller, error) {
	contract, err := bindAsset(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &AssetCaller{contract: contract}, nil
}

// NewAssetTransactor creates a new write-only instance of Asset, bound to a specific deployed contract.
func NewAssetTransactor(address common.Address, transactor bind.ContractTransactor) (*AssetTransactor, error) {
	contract, err := bindAsset(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &AssetTransactor{contract: contract}, nil
}

// NewAssetFilterer creates a new log filterer instance of Asset, bound to a specific deployed contract.
func NewAssetFilterer(address common.Address, filterer bind.ContractFilterer) (*AssetFilterer, error) {
	contract, err := bindAsset(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &AssetFilterer{contract: contract}, nil
}

// bindAsset binds a generic wrapper to an already deployed contract.
func bindAsset(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(AssetABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Asset *AssetRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Asset.Contract.AssetCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Asset *AssetRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Asset.Contract.AssetTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Asset *AssetRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Asset.Contract.AssetTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Asset *AssetCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Asset.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Asset *AssetTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Asset.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Asset *AssetTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Asset.Contract.contract.Transact(opts, method, params...)
}

// IsExists is a free data retrieval call binding the contract method 0xe54d592f.
//
// Solidity: function _isExists(string _id) view returns(bool)
func (_Asset *AssetCaller) IsExists(opts *bind.CallOpts, _id string) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _Asset.contract.Call(opts, out, "_isExists", _id)
	return *ret0, err
}

// IsExists is a free data retrieval call binding the contract method 0xe54d592f.
//
// Solidity: function _isExists(string _id) view returns(bool)
func (_Asset *AssetSession) IsExists(_id string) (bool, error) {
	return _Asset.Contract.IsExists(&_Asset.CallOpts, _id)
}

// IsExists is a free data retrieval call binding the contract method 0xe54d592f.
//
// Solidity: function _isExists(string _id) view returns(bool)
func (_Asset *AssetCallerSession) IsExists(_id string) (bool, error) {
	return _Asset.Contract.IsExists(&_Asset.CallOpts, _id)
}

// Read is a free data retrieval call binding the contract method 0x616ffe83.
//
// Solidity: function read(string _id) view returns((string,(string,string,string,string),(string[],string[],string[],string[],string[])))
func (_Asset *AssetCaller) Read(opts *bind.CallOpts, _id string) (AssetAsset_Configuration_Public, error) {
	var (
		ret0 = new(AssetAsset_Configuration_Public)
	)
	out := ret0
	err := _Asset.contract.Call(opts, out, "read", _id)
	return *ret0, err
}

// Read is a free data retrieval call binding the contract method 0x616ffe83.
//
// Solidity: function read(string _id) view returns((string,(string,string,string,string),(string[],string[],string[],string[],string[])))
func (_Asset *AssetSession) Read(_id string) (AssetAsset_Configuration_Public, error) {
	return _Asset.Contract.Read(&_Asset.CallOpts, _id)
}

// Read is a free data retrieval call binding the contract method 0x616ffe83.
//
// Solidity: function read(string _id) view returns((string,(string,string,string,string),(string[],string[],string[],string[],string[])))
func (_Asset *AssetCallerSession) Read(_id string) (AssetAsset_Configuration_Public, error) {
	return _Asset.Contract.Read(&_Asset.CallOpts, _id)
}

// ReadAll is a free data retrieval call binding the contract method 0x41f654f7.
//
// Solidity: function readAll() view returns((string,(string,string,string,string),(string[],string[],string[],string[],string[]))[])
func (_Asset *AssetCaller) ReadAll(opts *bind.CallOpts) ([]AssetAsset_Configuration_Public, error) {
	var (
		ret0 = new([]AssetAsset_Configuration_Public)
	)
	out := ret0
	err := _Asset.contract.Call(opts, out, "readAll")
	return *ret0, err
}

// ReadAll is a free data retrieval call binding the contract method 0x41f654f7.
//
// Solidity: function readAll() view returns((string,(string,string,string,string),(string[],string[],string[],string[],string[]))[])
func (_Asset *AssetSession) ReadAll() ([]AssetAsset_Configuration_Public, error) {
	return _Asset.Contract.ReadAll(&_Asset.CallOpts)
}

// ReadAll is a free data retrieval call binding the contract method 0x41f654f7.
//
// Solidity: function readAll() view returns((string,(string,string,string,string),(string[],string[],string[],string[],string[]))[])
func (_Asset *AssetCallerSession) ReadAll() ([]AssetAsset_Configuration_Public, error) {
	return _Asset.Contract.ReadAll(&_Asset.CallOpts)
}

// Create is a paid mutator transaction binding the contract method 0x5d647bca.
//
// Solidity: function create((string,(string,string,string,string),(string[],string[],string[],string[],string[])) _asset_Configuration_Public) returns()
func (_Asset *AssetTransactor) Create(opts *bind.TransactOpts, _asset_Configuration_Public AssetAsset_Configuration_Public) (*types.Transaction, error) {
	return _Asset.contract.Transact(opts, "create", _asset_Configuration_Public)
}

// Create is a paid mutator transaction binding the contract method 0x5d647bca.
//
// Solidity: function create((string,(string,string,string,string),(string[],string[],string[],string[],string[])) _asset_Configuration_Public) returns()
func (_Asset *AssetSession) Create(_asset_Configuration_Public AssetAsset_Configuration_Public) (*types.Transaction, error) {
	return _Asset.Contract.Create(&_Asset.TransactOpts, _asset_Configuration_Public)
}

// Create is a paid mutator transaction binding the contract method 0x5d647bca.
//
// Solidity: function create((string,(string,string,string,string),(string[],string[],string[],string[],string[])) _asset_Configuration_Public) returns()
func (_Asset *AssetTransactorSession) Create(_asset_Configuration_Public AssetAsset_Configuration_Public) (*types.Transaction, error) {
	return _Asset.Contract.Create(&_Asset.TransactOpts, _asset_Configuration_Public)
}
