// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package main

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// PublicKeys is an auto generated low-level Go binding around an user-defined struct.
type PublicKeys struct {
	EncryptedHashedClientID string
	EncryptedPublicKey      string
}

// PublicABI is the input ABI used to generate the binding from.
const PublicABI = "[{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_encryptedHashedClientID\",\"type\":\"string\"}],\"name\":\"_isExists\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"EncryptedHashedClientID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"EncryptedPublicKey\",\"type\":\"string\"}],\"internalType\":\"structPublic.Keys\",\"name\":\"_keys\",\"type\":\"tuple\"}],\"name\":\"create\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_encryptedHashedClientID\",\"type\":\"string\"}],\"name\":\"read\",\"outputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"EncryptedHashedClientID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"EncryptedPublicKey\",\"type\":\"string\"}],\"internalType\":\"structPublic.Keys\",\"name\":\"\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]"

var PublicParsedABI, _ = abi.JSON(strings.NewReader(PublicABI))

// PublicFuncSigs maps the 4-byte function signature to its string representation.
var PublicFuncSigs = map[string]string{
	"e54d592f": "_isExists(string)",
	"838c6377": "create((string,string))",
	"616ffe83": "read(string)",
}

// PublicBin is the compiled bytecode used for deploying new contracts.
var PublicBin = "0x60806040526000805534801561001457600080fd5b5061065f806100246000396000f3fe608060405234801561001057600080fd5b50600436106100415760003560e01c8063616ffe8314610046578063838c63771461006f578063e54d592f14610084575b600080fd5b61005961005436600461044a565b6100a4565b60405161006691906105e7565b60405180910390f35b61008261007d366004610485565b610256565b005b61009761009236600461044a565b6102b8565b60405161006691906105dc565b6100ac61031a565b6000805b600054811015610103578380519060200120600160008381526020019081526020016000206000016040516100e5919061056c565b604051809103902014156100fb57809150610103565b6001016100b0565b6000828152600160208181526040928390208351815460029481161561010002600019011693909304601f810183900490920283016060908101855293830182815292939092849290918491908401828280156101a15780601f10610176576101008083540402835291602001916101a1565b820191906000526020600020905b81548152906001019060200180831161018457829003601f168201915b50505050508152602001600182018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156102435780601f1061021857610100808354040283529160200191610243565b820191906000526020600020905b81548152906001019060200180831161022657829003601f168201915b505050505081525050925050505b919050565b6040805180820182528251815260208084015181830152600080548152600182529290922081518051929391926102909284920190610334565b5060208281015180516102a99260018501920190610334565b50506000805460010190555050565b6000805b600054811015610311578280519060200120600160008381526020019081526020016000206000016040516102f1919061056c565b60405180910390201415610309576001915050610251565b6001016102bc565b50600092915050565b604051806040016040528060608152602001606081525090565b828054600181600116156101000203166002900490600052602060002090601f01602090048101928261036a57600085556103b0565b82601f1061038357805160ff19168380011785556103b0565b828001600101855582156103b0579182015b828111156103b0578251825591602001919060010190610395565b506103bc9291506103c0565b5090565b5b808211156103bc57600081556001016103c1565b600082601f8301126103e5578081fd5b813567ffffffffffffffff808211156103fa57fe5b604051601f8301601f19168101602001828111828210171561041857fe5b60405282815284830160200186101561042f578384fd5b82602086016020830137918201602001929092529392505050565b60006020828403121561045b578081fd5b813567ffffffffffffffff811115610471578182fd5b61047d848285016103d5565b949350505050565b600060208284031215610496578081fd5b813567ffffffffffffffff808211156104ad578283fd5b90830190604082860312156104c0578283fd5b6040516040810181811083821117156104d557fe5b6040528235828111156104e6578485fd5b6104f2878286016103d5565b825250602083013582811115610506578485fd5b610512878286016103d5565b60208301525095945050505050565b60008151808452815b818110156105465760208185018101518683018201520161052a565b818111156105575782602083870101525b50601f01601f19169290920160200192915050565b600080835460018082166000811461058b57600181146105a2576105d1565b60ff198316865260028304607f16860193506105d1565b600283048786526020808720875b838110156105c95781548a8201529085019082016105b0565b505050860193505b509195945050505050565b901515815260200190565b6000602082528251604060208401526106036060840182610521565b90506020840151601f198483030160408501526106208282610521565b9594505050505056fea26469706673582212205686f6a20b94a2f6b20b66a84e5970ab31f61168a7240df2725135099a8befc464736f6c63430007060033"

// DeployPublic deploys a new Ethereum contract, binding an instance of Public to it.
func DeployPublic(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Public, error) {
	parsed, err := abi.JSON(strings.NewReader(PublicABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}

	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(PublicBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Public{PublicCaller: PublicCaller{contract: contract}, PublicTransactor: PublicTransactor{contract: contract}, PublicFilterer: PublicFilterer{contract: contract}}, nil
}

// Public is an auto generated Go binding around an Ethereum contract.
type Public struct {
	PublicCaller     // Read-only binding to the contract
	PublicTransactor // Write-only binding to the contract
	PublicFilterer   // Log filterer for contract events
}

// PublicCaller is an auto generated read-only Go binding around an Ethereum contract.
type PublicCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// PublicTransactor is an auto generated write-only Go binding around an Ethereum contract.
type PublicTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// PublicFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type PublicFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// PublicSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type PublicSession struct {
	Contract     *Public           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// PublicCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type PublicCallerSession struct {
	Contract *PublicCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// PublicTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type PublicTransactorSession struct {
	Contract     *PublicTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// PublicRaw is an auto generated low-level Go binding around an Ethereum contract.
type PublicRaw struct {
	Contract *Public // Generic contract binding to access the raw methods on
}

// PublicCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type PublicCallerRaw struct {
	Contract *PublicCaller // Generic read-only contract binding to access the raw methods on
}

// PublicTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type PublicTransactorRaw struct {
	Contract *PublicTransactor // Generic write-only contract binding to access the raw methods on
}

// NewPublic creates a new instance of Public, bound to a specific deployed contract.
func NewPublic(address common.Address, backend bind.ContractBackend) (*Public, error) {
	contract, err := bindPublic(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Public{PublicCaller: PublicCaller{contract: contract}, PublicTransactor: PublicTransactor{contract: contract}, PublicFilterer: PublicFilterer{contract: contract}}, nil
}

// NewPublicCaller creates a new read-only instance of Public, bound to a specific deployed contract.
func NewPublicCaller(address common.Address, caller bind.ContractCaller) (*PublicCaller, error) {
	contract, err := bindPublic(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &PublicCaller{contract: contract}, nil
}

// NewPublicTransactor creates a new write-only instance of Public, bound to a specific deployed contract.
func NewPublicTransactor(address common.Address, transactor bind.ContractTransactor) (*PublicTransactor, error) {
	contract, err := bindPublic(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &PublicTransactor{contract: contract}, nil
}

// NewPublicFilterer creates a new log filterer instance of Public, bound to a specific deployed contract.
func NewPublicFilterer(address common.Address, filterer bind.ContractFilterer) (*PublicFilterer, error) {
	contract, err := bindPublic(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &PublicFilterer{contract: contract}, nil
}

// bindPublic binds a generic wrapper to an already deployed contract.
func bindPublic(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(PublicABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Public *PublicRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Public.Contract.PublicCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Public *PublicRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Public.Contract.PublicTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Public *PublicRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Public.Contract.PublicTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Public *PublicCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Public.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Public *PublicTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Public.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Public *PublicTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Public.Contract.contract.Transact(opts, method, params...)
}

// IsExists is a free data retrieval call binding the contract method 0xe54d592f.
//
// Solidity: function _isExists(string _encryptedHashedClientID) view returns(bool)
func (_Public *PublicCaller) IsExists(opts *bind.CallOpts, _encryptedHashedClientID string) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _Public.contract.Call(opts, out, "_isExists", _encryptedHashedClientID)
	return *ret0, err
}

// IsExists is a free data retrieval call binding the contract method 0xe54d592f.
//
// Solidity: function _isExists(string _encryptedHashedClientID) view returns(bool)
func (_Public *PublicSession) IsExists(_encryptedHashedClientID string) (bool, error) {
	return _Public.Contract.IsExists(&_Public.CallOpts, _encryptedHashedClientID)
}

// IsExists is a free data retrieval call binding the contract method 0xe54d592f.
//
// Solidity: function _isExists(string _encryptedHashedClientID) view returns(bool)
func (_Public *PublicCallerSession) IsExists(_encryptedHashedClientID string) (bool, error) {
	return _Public.Contract.IsExists(&_Public.CallOpts, _encryptedHashedClientID)
}

// Read is a free data retrieval call binding the contract method 0x616ffe83.
//
// Solidity: function read(string _encryptedHashedClientID) view returns((string,string))
func (_Public *PublicCaller) Read(opts *bind.CallOpts, _encryptedHashedClientID string) (PublicKeys, error) {
	var (
		ret0 = new(PublicKeys)
	)
	out := ret0
	err := _Public.contract.Call(opts, out, "read", _encryptedHashedClientID)
	return *ret0, err
}

// Read is a free data retrieval call binding the contract method 0x616ffe83.
//
// Solidity: function read(string _encryptedHashedClientID) view returns((string,string))
func (_Public *PublicSession) Read(_encryptedHashedClientID string) (PublicKeys, error) {
	return _Public.Contract.Read(&_Public.CallOpts, _encryptedHashedClientID)
}

// Read is a free data retrieval call binding the contract method 0x616ffe83.
//
// Solidity: function read(string _encryptedHashedClientID) view returns((string,string))
func (_Public *PublicCallerSession) Read(_encryptedHashedClientID string) (PublicKeys, error) {
	return _Public.Contract.Read(&_Public.CallOpts, _encryptedHashedClientID)
}

// Create is a paid mutator transaction binding the contract method 0x838c6377.
//
// Solidity: function create((string,string) _keys) returns()
func (_Public *PublicTransactor) Create(opts *bind.TransactOpts, _keys PublicKeys) (*types.Transaction, error) {
	return _Public.contract.Transact(opts, "create", _keys)
}

// Create is a paid mutator transaction binding the contract method 0x838c6377.
//
// Solidity: function create((string,string) _keys) returns()
func (_Public *PublicSession) Create(_keys PublicKeys) (*types.Transaction, error) {
	return _Public.Contract.Create(&_Public.TransactOpts, _keys)
}

// Create is a paid mutator transaction binding the contract method 0x838c6377.
//
// Solidity: function create((string,string) _keys) returns()
func (_Public *PublicTransactorSession) Create(_keys PublicKeys) (*types.Transaction, error) {
	return _Public.Contract.Create(&_Public.TransactOpts, _keys)
}
