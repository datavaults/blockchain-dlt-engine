// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package main

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// TenantABE is an auto generated low-level Go binding around an user-defined struct.
type TenantABE struct {
	Key       string
	ABEpolicy []string
}

// TenantAnonymisation is an auto generated low-level Go binding around an user-defined struct.
type TenantAnonymisation struct {
	Anonymise        bool
	PseudoId         string
	ExistingPseudoId string
	Levels           TenantLevels
}

// TenantConfiguration is an auto generated low-level Go binding around an user-defined struct.
type TenantConfiguration struct {
	Anonymisation   TenantAnonymisation
	Price           string
	LicenseType     string
	StandardLicense string
	OwnLicense      string
	Encryption      bool
	Persona         bool
	UseTPM          bool
}

// TenantData_Sharing_Configuration is an auto generated low-level Go binding around an user-defined struct.
type TenantData_Sharing_Configuration struct {
	DatasetID     string
	Asset         string
	Name          string
	Description   string
	Keywords      []string
	Configuration TenantConfiguration
	Policy        TenantPolicy
	Abe           []TenantABE
}

// TenantLevels is an auto generated low-level Go binding around an user-defined struct.
type TenantLevels struct {
	LevelOfAnonymity string
	Value            *big.Int
}

// TenantPolicy is an auto generated low-level Go binding around an user-defined struct.
type TenantPolicy struct {
	Sector     []string
	Continent  []string
	Country    []string
	Type       []string
	Size       []string
	Reputation *big.Int
}

// TenantABI is the input ABI used to generate the binding from.
const TenantABI = "[{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_id\",\"type\":\"string\"}],\"name\":\"_isExists\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"DatasetID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"Asset\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"Name\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"Description\",\"type\":\"string\"},{\"internalType\":\"string[]\",\"name\":\"Keywords\",\"type\":\"string[]\"},{\"components\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"Anonymise\",\"type\":\"bool\"},{\"internalType\":\"string\",\"name\":\"PseudoId\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"ExistingPseudoId\",\"type\":\"string\"},{\"components\":[{\"internalType\":\"string\",\"name\":\"LevelOfAnonymity\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"Value\",\"type\":\"uint256\"}],\"internalType\":\"structTenant.Levels\",\"name\":\"levels\",\"type\":\"tuple\"}],\"internalType\":\"structTenant.Anonymisation\",\"name\":\"anonymisation\",\"type\":\"tuple\"},{\"internalType\":\"string\",\"name\":\"Price\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"LicenseType\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"StandardLicense\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"OwnLicense\",\"type\":\"string\"},{\"internalType\":\"bool\",\"name\":\"Encryption\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"Persona\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"UseTPM\",\"type\":\"bool\"}],\"internalType\":\"structTenant.Configuration\",\"name\":\"configuration\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"string[]\",\"name\":\"Sector\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Continent\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Country\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Type\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Size\",\"type\":\"string[]\"},{\"internalType\":\"uint256\",\"name\":\"Reputation\",\"type\":\"uint256\"}],\"internalType\":\"structTenant.Policy\",\"name\":\"policy\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"string\",\"name\":\"Key\",\"type\":\"string\"},{\"internalType\":\"string[]\",\"name\":\"ABEpolicy\",\"type\":\"string[]\"}],\"internalType\":\"structTenant.ABE[]\",\"name\":\"abe\",\"type\":\"tuple[]\"}],\"internalType\":\"structTenant.Data_Sharing_Configuration\",\"name\":\"_data_Sharing_Configuration\",\"type\":\"tuple\"}],\"name\":\"create\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_id\",\"type\":\"string\"}],\"name\":\"read\",\"outputs\":[{\"components\":[{\"internalType\":\"string\",\"name\":\"DatasetID\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"Asset\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"Name\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"Description\",\"type\":\"string\"},{\"internalType\":\"string[]\",\"name\":\"Keywords\",\"type\":\"string[]\"},{\"components\":[{\"components\":[{\"internalType\":\"bool\",\"name\":\"Anonymise\",\"type\":\"bool\"},{\"internalType\":\"string\",\"name\":\"PseudoId\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"ExistingPseudoId\",\"type\":\"string\"},{\"components\":[{\"internalType\":\"string\",\"name\":\"LevelOfAnonymity\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"Value\",\"type\":\"uint256\"}],\"internalType\":\"structTenant.Levels\",\"name\":\"levels\",\"type\":\"tuple\"}],\"internalType\":\"structTenant.Anonymisation\",\"name\":\"anonymisation\",\"type\":\"tuple\"},{\"internalType\":\"string\",\"name\":\"Price\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"LicenseType\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"StandardLicense\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"OwnLicense\",\"type\":\"string\"},{\"internalType\":\"bool\",\"name\":\"Encryption\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"Persona\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"UseTPM\",\"type\":\"bool\"}],\"internalType\":\"structTenant.Configuration\",\"name\":\"configuration\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"string[]\",\"name\":\"Sector\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Continent\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Country\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Type\",\"type\":\"string[]\"},{\"internalType\":\"string[]\",\"name\":\"Size\",\"type\":\"string[]\"},{\"internalType\":\"uint256\",\"name\":\"Reputation\",\"type\":\"uint256\"}],\"internalType\":\"structTenant.Policy\",\"name\":\"policy\",\"type\":\"tuple\"},{\"components\":[{\"internalType\":\"string\",\"name\":\"Key\",\"type\":\"string\"},{\"internalType\":\"string[]\",\"name\":\"ABEpolicy\",\"type\":\"string[]\"}],\"internalType\":\"structTenant.ABE[]\",\"name\":\"abe\",\"type\":\"tuple[]\"}],\"internalType\":\"structTenant.Data_Sharing_Configuration\",\"name\":\"\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]"

var TenantParsedABI, _ = abi.JSON(strings.NewReader(TenantABI))

// TenantFuncSigs maps the 4-byte function signature to its string representation.
var TenantFuncSigs = map[string]string{
	"e54d592f": "_isExists(string)",
	"86ed7f81": "create((string,string,string,string,string[],((bool,string,string,(string,uint256)),string,string,string,string,bool,bool,bool),(string[],string[],string[],string[],string[],uint256),(string,string[])[]))",
	"616ffe83": "read(string)",
}

// TenantBin is the compiled bytecode used for deploying new contracts.
var TenantBin = "0x60806040526000805534801561001457600080fd5b50612174806100246000396000f3fe608060405234801561001057600080fd5b50600436106100415760003560e01c8063616ffe831461004657806386ed7f811461006f578063e54d592f14610084575b600080fd5b610059610054366004611b1d565b6100a4565b6040516100669190612007565b60405180910390f35b61008261007d366004611b58565b610f0c565b005b610097610092366004611b1d565b611397565b6040516100669190611ffc565b6100ac6113f9565b6000805b600054811015610103578380519060200120600160008381526020019081526020016000206000016040516100e59190611f8c565b604051809103902014156100fb57809150610103565b6001016100b0565b6000828152600160208181526040928390208351815460026101009582161586026000190190911604601f8101849004909302810161012090810190955292830182815292939092849290918491908401828280156101a35780601f10610178576101008083540402835291602001916101a3565b820191906000526020600020905b81548152906001019060200180831161018657829003601f168201915b50505050508152602001600182018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156102455780601f1061021a57610100808354040283529160200191610245565b820191906000526020600020905b81548152906001019060200180831161022857829003601f168201915b5050509183525050600282810180546040805160206001841615610100026000190190931694909404601f810183900483028501830190915280845293810193908301828280156102d75780601f106102ac576101008083540402835291602001916102d7565b820191906000526020600020905b8154815290600101906020018083116102ba57829003601f168201915b505050918352505060038201805460408051602060026001851615610100026000190190941693909304601f810184900484028201840190925281815293820193929183018282801561036b5780601f106103405761010080835404028352916020019161036b565b820191906000526020600020905b81548152906001019060200180831161034e57829003601f168201915b5050505050815260200160048201805480602002602001604051908101604052809291908181526020016000905b828210156104445760008481526020908190208301805460408051601f60026000196101006001871615020190941693909304928301859004850281018501909152818152928301828280156104305780601f1061040557610100808354040283529160200191610430565b820191906000526020600020905b81548152906001019060200180831161041357829003601f168201915b505050505081526020019060010190610399565b50505090825250604080516101808101825260058401805460ff16151561010080840191825260068701805486516020600260018416159095026000190190921693909304601f810182900482028401820190975286835296870196949593948694869361012087019390929091908301828280156105045780601f106104d957610100808354040283529160200191610504565b820191906000526020600020905b8154815290600101906020018083116104e757829003601f168201915b5050509183525050600282810180546040805160206001841615610100026000190190931694909404601f810183900483028501830190915280845293810193908301828280156105965780601f1061056b57610100808354040283529160200191610596565b820191906000526020600020905b81548152906001019060200180831161057957829003601f168201915b50505091835250506040805160038401805460606020601f60026000196101006001871615020190941693909304928301819004810285018201865294840182815294909501949293919284929184918401828280156106375780601f1061060c57610100808354040283529160200191610637565b820191906000526020600020905b81548152906001019060200180831161061a57829003601f168201915b50505050508152602001600182015481525050815250508152602001600582018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156106eb5780601f106106c0576101008083540402835291602001916106eb565b820191906000526020600020905b8154815290600101906020018083116106ce57829003601f168201915b505050918352505060068201805460408051602060026001851615610100026000190190941693909304601f810184900484028201840190925281815293820193929183018282801561077f5780601f106107545761010080835404028352916020019161077f565b820191906000526020600020905b81548152906001019060200180831161076257829003601f168201915b505050918352505060078201805460408051602060026001851615610100026000190190941693909304601f81018490048402820184019092528181529382019392918301828280156108135780601f106107e857610100808354040283529160200191610813565b820191906000526020600020905b8154815290600101906020018083116107f657829003601f168201915b505050918352505060088201805460408051602060026001851615610100026000190190941693909304601f81018490048402820184019092528181529382019392918301828280156108a75780601f1061087c576101008083540402835291602001916108a7565b820191906000526020600020905b81548152906001019060200180831161088a57829003601f168201915b50505091835250506009919091015460ff80821615156020808501919091526101008304821615156040808601919091526201000090930490911615156060909301929092529183528151600f8501805460e08185028401810190955260c0830181815295909301949193909284929184919060009085015b828210156109cb5760008481526020908190208301805460408051601f60026000196101006001871615020190941693909304928301859004850281018501909152818152928301828280156109b75780601f1061098c576101008083540402835291602001916109b7565b820191906000526020600020905b81548152906001019060200180831161099a57829003601f168201915b505050505081526020019060010190610920565b50505050815260200160018201805480602002602001604051908101604052809291908181526020016000905b82821015610aa35760008481526020908190208301805460408051601f6002600019610100600187161502019094169390930492830185900485028101850190915281815292830182828015610a8f5780601f10610a6457610100808354040283529160200191610a8f565b820191906000526020600020905b815481529060010190602001808311610a7257829003601f168201915b5050505050815260200190600101906109f8565b50505050815260200160028201805480602002602001604051908101604052809291908181526020016000905b82821015610b7b5760008481526020908190208301805460408051601f6002600019610100600187161502019094169390930492830185900485028101850190915281815292830182828015610b675780601f10610b3c57610100808354040283529160200191610b67565b820191906000526020600020905b815481529060010190602001808311610b4a57829003601f168201915b505050505081526020019060010190610ad0565b50505050815260200160038201805480602002602001604051908101604052809291908181526020016000905b82821015610c535760008481526020908190208301805460408051601f6002600019610100600187161502019094169390930492830185900485028101850190915281815292830182828015610c3f5780601f10610c1457610100808354040283529160200191610c3f565b820191906000526020600020905b815481529060010190602001808311610c2257829003601f168201915b505050505081526020019060010190610ba8565b50505050815260200160048201805480602002602001604051908101604052809291908181526020016000905b82821015610d2b5760008481526020908190208301805460408051601f6002600019610100600187161502019094169390930492830185900485028101850190915281815292830182828015610d175780601f10610cec57610100808354040283529160200191610d17565b820191906000526020600020905b815481529060010190602001808311610cfa57829003601f168201915b505050505081526020019060010190610c80565b505050508152602001600582015481525050815260200160158201805480602002602001604051908101604052809291908181526020016000905b82821015610efa5760008481526020908190206040805160028681029093018054600181161561010002600019011693909304601f810185900490940281016060908101835291810184815290938492849190840182828015610e0a5780601f10610ddf57610100808354040283529160200191610e0a565b820191906000526020600020905b815481529060010190602001808311610ded57829003601f168201915b5050505050815260200160018201805480602002602001604051908101604052809291908181526020016000905b82821015610ee35760008481526020908190208301805460408051601f6002600019610100600187161502019094169390930492830185900485028101850190915281815292830182828015610ecf5780601f10610ea457610100808354040283529160200191610ecf565b820191906000526020600020905b815481529060010190602001808311610eb257829003601f168201915b505050505081526020019060010190610e38565b505050508152505081526020019060010190610d66565b5050505081525050925050505b919050565b60408051808201825260a080840180515160609081015151845281515181015160209081015181860152855160808181018852845151511515825284515183015182840152845151880151828901528184018790528751610100810189528281528551840151818501528551890151818a01528551850151818601528551820151818301528551870151151581880152855160c090810151151581830152955160e09081015115159082015288518087018a52958a01805151875280518501518786015280518a0151878b0152805186015195870195909552845182015191860191909152925185015194840194909452865160008054815260018352969096208651959694959294611022939192019061144a565b5060208086015160008054815260018084526040909120825161104b949190920192019061144a565b5084604001516001600080548152602001908152602001600020600201908051906020019061107b92919061144a565b50606085015160008054815260016020908152604090912082516110a8936003909201929091019061144a565b50608085015160008054815260016020908152604090912082516110d593600490920192909101906114d6565b506000805481526001602090815260409091208351805160058301805460ff191691151591909117815581840151805187959294859361111d9360069092019291019061144a565b506040820151805161113991600284019160209091019061144a565b5060608201518051805160038401916111579183916020019061144a565b5060209182015160019091015584810151805161117d945060058601935091019061144a565b506040820151805161119991600684019160209091019061144a565b50606082015180516111b591600784019160209091019061144a565b50608082015180516111d191600884019160209091019061144a565b5060a08201516009909101805460c084015160e0909401511515620100000262ff0000199415156101000261ff001994151560ff1990931692909217939093161792909216179055600080548152600160209081526040909120825180518493600f909301926112459284929101906114d6565b50602082810151805161125e92600185019201906114d6565b506040820151805161127a9160028401916020909101906114d6565b50606082015180516112969160038401916020909101906114d6565b50608082015180516112b29160048401916020909101906114d6565b5060a0919091015160059091015560e08501515160005b8181101561138557600160008054815260200190815260200160002060150160405180604001604052808960e00151848151811061130357fe5b60200260200101516000015181526020018960e00151848151811061132457fe5b602090810291909101810151810151909152825460018101845560009384529281902082518051939460020290910192611361928492019061144a565b50602082810151805161137a92600185019201906114d6565b5050506001016112c9565b50506000805460010190555050505050565b6000805b6000548110156113f0578280519060200120600160008381526020019081526020016000206000016040516113d09190611f8c565b604051809103902014156113e8576001915050610f07565b60010161139b565b50600092915050565b604051806101000160405280606081526020016060815260200160608152602001606081526020016060815260200161143061152f565b815260200161143d611579565b8152602001606081525090565b828054600181600116156101000203166002900490600052602060002090601f01602090048101928261148057600085556114c6565b82601f1061149957805160ff19168380011785556114c6565b828001600101855582156114c6579182015b828111156114c65782518255916020019190600101906114ab565b506114d29291506115af565b5090565b828054828255906000526020600020908101928215611523579160200282015b82811115611523578251805161151391849160209091019061144a565b50916020019190600101906114f6565b506114d29291506115c4565b6040518061010001604052806115436115e1565b8152606060208201819052604082018190528082018190526080820152600060a0820181905260c0820181905260e09091015290565b6040518060c001604052806060815260200160608152602001606081526020016060815260200160608152602001600081525090565b5b808211156114d257600081556001016115b0565b808211156114d25760006115d88282611610565b506001016115c4565b6040518060800160405280600015158152602001606081526020016060815260200161160b611657565b905290565b50805460018160011615610100020316600290046000825580601f106116365750611654565b601f01602090049060005260206000209081019061165491906115af565b50565b604051806040016040528060608152602001600081525090565b600082601f830112611681578081fd5b8135602061169661169183612120565b6120fc565b82815281810190858301855b858110156116cb576116b9898684358b01016117ad565b845292840192908401906001016116a2565b5090979650505050505050565b600082601f8301126116e8578081fd5b813560206116f861169183612120565b82815281810190858301855b858110156116cb5781358801604080601f19838d03011215611724578889fd5b805181810167ffffffffffffffff828210818311171561174057fe5b908352838901359080821115611754578b8cfd5b6117628e8b848801016117ad565b8352928401359280841115611775578b8cfd5b50506117858c8984860101611671565b81890152865250509284019290840190600101611704565b80358015158114610f0757600080fd5b600082601f8301126117bd578081fd5b813567ffffffffffffffff8111156117d157fe5b6117e4601f8201601f19166020016120fc565b8181528460208386010111156117f8578283fd5b816020850160208301379081016020019190915292915050565b600060808284031215611823578081fd5b61182d60806120fc565b90506118388261179d565b8152602082013567ffffffffffffffff8082111561185557600080fd5b611861858386016117ad565b6020840152604084013591508082111561187a57600080fd5b611886858386016117ad565b6040840152606084013591508082111561189f57600080fd5b506118ac848285016119cc565b60608301525092915050565b60006101008083850312156118cb578182fd5b6118d4816120fc565b915050813567ffffffffffffffff808211156118ef57600080fd5b6118fb85838601611812565b8352602084013591508082111561191157600080fd5b61191d858386016117ad565b6020840152604084013591508082111561193657600080fd5b611942858386016117ad565b6040840152606084013591508082111561195b57600080fd5b611967858386016117ad565b6060840152608084013591508082111561198057600080fd5b5061198d848285016117ad565b60808301525061199f60a0830161179d565b60a08201526119b060c0830161179d565b60c08201526119c160e0830161179d565b60e082015292915050565b6000604082840312156119dd578081fd5b6040516040810167ffffffffffffffff82821081831117156119fb57fe5b816040528293508435915080821115611a1357600080fd5b50611a20858286016117ad565b825250602083013560208201525092915050565b600060c08284031215611a45578081fd5b611a4f60c06120fc565b9050813567ffffffffffffffff80821115611a6957600080fd5b611a7585838601611671565b83526020840135915080821115611a8b57600080fd5b611a9785838601611671565b60208401526040840135915080821115611ab057600080fd5b611abc85838601611671565b60408401526060840135915080821115611ad557600080fd5b611ae185838601611671565b60608401526080840135915080821115611afa57600080fd5b50611b0784828501611671565b60808301525060a082013560a082015292915050565b600060208284031215611b2e578081fd5b813567ffffffffffffffff811115611b44578182fd5b611b50848285016117ad565b949350505050565b600060208284031215611b69578081fd5b813567ffffffffffffffff80821115611b80578283fd5b8184019150610100808387031215611b96578384fd5b611b9f816120fc565b9050823582811115611baf578485fd5b611bbb878286016117ad565b825250602083013582811115611bcf578485fd5b611bdb878286016117ad565b602083015250604083013582811115611bf2578485fd5b611bfe878286016117ad565b604083015250606083013582811115611c15578485fd5b611c21878286016117ad565b606083015250608083013582811115611c38578485fd5b611c4487828601611671565b60808301525060a083013582811115611c5b578485fd5b611c67878286016118b8565b60a08301525060c083013582811115611c7e578485fd5b611c8a87828601611a34565b60c08301525060e083013582811115611ca1578485fd5b611cad878286016116d8565b60e08301525095945050505050565b60008282518085526020808601955080818302840101818601855b848110156116cb57601f19868403018952611cf3838351611d7a565b98840198925090830190600101611cd7565b60008282518085526020808601955080818302840101818601855b848110156116cb57601f19868403018952815160408151818652611d4682870182611d7a565b91505085820151915084810386860152611d608183611cbc565b9a86019a9450505090830190600101611d20565b15159052565b60008151808452815b81811015611d9f57602081850181015186830182015201611d83565b81811115611db05782602083870101525b50601f01601f19169290920160200192915050565b6000815115158352602082015160806020850152611de66080850182611d7a565b905060408301518482036040860152611dff8282611d7a565b91505060608301518482036060860152805160408352611e226040840182611d7a565b6020928301519390920192909252949350505050565b60006101008251818552611e4e82860182611dc5565b91505060208301518482036020860152611e688282611d7a565b91505060408301518482036040860152611e828282611d7a565b91505060608301518482036060860152611e9c8282611d7a565b91505060808301518482036080860152611eb68282611d7a565b91505060a0830151611ecb60a0860182611d74565b5060c0830151611ede60c0860182611d74565b5060e0830151611ef160e0860182611d74565b509392505050565b6000815160c08452611f0e60c0850182611cbc565b905060208301518482036020860152611f278282611cbc565b91505060408301518482036040860152611f418282611cbc565b91505060608301518482036060860152611f5b8282611cbc565b91505060808301518482036080860152611f758282611cbc565b91505060a083015160a08501528091505092915050565b6000808354600180821660008114611fab5760018114611fc257611ff1565b60ff198316865260028304607f1686019350611ff1565b600283048786526020808720875b83811015611fe95781548a820152908501908201611fd0565b505050860193505b509195945050505050565b901515815260200190565b6000602082528251610100806020850152612026610120850183611d7a565b91506020850151601f19808685030160408701526120448483611d7a565b935060408701519150808685030160608701526120618483611d7a565b9350606087015191508086850301608087015261207e8483611d7a565b935060808701519150808685030160a087015261209b8483611cbc565b935060a08701519150808685030160c08701526120b88483611e38565b935060c08701519150808685030160e08701526120d58483611ef9565b935060e08701519150808685030183870152506120f28382611d05565b9695505050505050565b60405181810167ffffffffffffffff8111828210171561211857fe5b604052919050565b600067ffffffffffffffff82111561213457fe5b506020908102019056fea26469706673582212208790027f3b366e3a21a47305109fc2fd8848e3765e8db0a90cbbe0029aec291f64736f6c63430007060033"

// DeployTenant deploys a new Ethereum contract, binding an instance of Tenant to it.
func DeployTenant(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Tenant, error) {
	parsed, err := abi.JSON(strings.NewReader(TenantABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}

	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(TenantBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Tenant{TenantCaller: TenantCaller{contract: contract}, TenantTransactor: TenantTransactor{contract: contract}, TenantFilterer: TenantFilterer{contract: contract}}, nil
}

// Tenant is an auto generated Go binding around an Ethereum contract.
type Tenant struct {
	TenantCaller     // Read-only binding to the contract
	TenantTransactor // Write-only binding to the contract
	TenantFilterer   // Log filterer for contract events
}

// TenantCaller is an auto generated read-only Go binding around an Ethereum contract.
type TenantCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TenantTransactor is an auto generated write-only Go binding around an Ethereum contract.
type TenantTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TenantFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type TenantFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TenantSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type TenantSession struct {
	Contract     *Tenant           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// TenantCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type TenantCallerSession struct {
	Contract *TenantCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// TenantTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type TenantTransactorSession struct {
	Contract     *TenantTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// TenantRaw is an auto generated low-level Go binding around an Ethereum contract.
type TenantRaw struct {
	Contract *Tenant // Generic contract binding to access the raw methods on
}

// TenantCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type TenantCallerRaw struct {
	Contract *TenantCaller // Generic read-only contract binding to access the raw methods on
}

// TenantTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type TenantTransactorRaw struct {
	Contract *TenantTransactor // Generic write-only contract binding to access the raw methods on
}

// NewTenant creates a new instance of Tenant, bound to a specific deployed contract.
func NewTenant(address common.Address, backend bind.ContractBackend) (*Tenant, error) {
	contract, err := bindTenant(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Tenant{TenantCaller: TenantCaller{contract: contract}, TenantTransactor: TenantTransactor{contract: contract}, TenantFilterer: TenantFilterer{contract: contract}}, nil
}

// NewTenantCaller creates a new read-only instance of Tenant, bound to a specific deployed contract.
func NewTenantCaller(address common.Address, caller bind.ContractCaller) (*TenantCaller, error) {
	contract, err := bindTenant(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &TenantCaller{contract: contract}, nil
}

// NewTenantTransactor creates a new write-only instance of Tenant, bound to a specific deployed contract.
func NewTenantTransactor(address common.Address, transactor bind.ContractTransactor) (*TenantTransactor, error) {
	contract, err := bindTenant(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &TenantTransactor{contract: contract}, nil
}

// NewTenantFilterer creates a new log filterer instance of Tenant, bound to a specific deployed contract.
func NewTenantFilterer(address common.Address, filterer bind.ContractFilterer) (*TenantFilterer, error) {
	contract, err := bindTenant(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &TenantFilterer{contract: contract}, nil
}

// bindTenant binds a generic wrapper to an already deployed contract.
func bindTenant(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(TenantABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Tenant *TenantRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Tenant.Contract.TenantCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Tenant *TenantRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Tenant.Contract.TenantTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Tenant *TenantRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Tenant.Contract.TenantTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Tenant *TenantCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Tenant.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Tenant *TenantTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Tenant.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Tenant *TenantTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Tenant.Contract.contract.Transact(opts, method, params...)
}

// IsExists is a free data retrieval call binding the contract method 0xe54d592f.
//
// Solidity: function _isExists(string _id) view returns(bool)
func (_Tenant *TenantCaller) IsExists(opts *bind.CallOpts, _id string) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _Tenant.contract.Call(opts, out, "_isExists", _id)
	return *ret0, err
}

// IsExists is a free data retrieval call binding the contract method 0xe54d592f.
//
// Solidity: function _isExists(string _id) view returns(bool)
func (_Tenant *TenantSession) IsExists(_id string) (bool, error) {
	return _Tenant.Contract.IsExists(&_Tenant.CallOpts, _id)
}

// IsExists is a free data retrieval call binding the contract method 0xe54d592f.
//
// Solidity: function _isExists(string _id) view returns(bool)
func (_Tenant *TenantCallerSession) IsExists(_id string) (bool, error) {
	return _Tenant.Contract.IsExists(&_Tenant.CallOpts, _id)
}

// Read is a free data retrieval call binding the contract method 0x616ffe83.
//
// Solidity: function read(string _id) view returns((string,string,string,string,string[],((bool,string,string,(string,uint256)),string,string,string,string,bool,bool,bool),(string[],string[],string[],string[],string[],uint256),(string,string[])[]))
func (_Tenant *TenantCaller) Read(opts *bind.CallOpts, _id string) (TenantData_Sharing_Configuration, error) {
	var (
		ret0 = new(TenantData_Sharing_Configuration)
	)
	out := ret0
	err := _Tenant.contract.Call(opts, out, "read", _id)
	return *ret0, err
}

// Read is a free data retrieval call binding the contract method 0x616ffe83.
//
// Solidity: function read(string _id) view returns((string,string,string,string,string[],((bool,string,string,(string,uint256)),string,string,string,string,bool,bool,bool),(string[],string[],string[],string[],string[],uint256),(string,string[])[]))
func (_Tenant *TenantSession) Read(_id string) (TenantData_Sharing_Configuration, error) {
	return _Tenant.Contract.Read(&_Tenant.CallOpts, _id)
}

// Read is a free data retrieval call binding the contract method 0x616ffe83.
//
// Solidity: function read(string _id) view returns((string,string,string,string,string[],((bool,string,string,(string,uint256)),string,string,string,string,bool,bool,bool),(string[],string[],string[],string[],string[],uint256),(string,string[])[]))
func (_Tenant *TenantCallerSession) Read(_id string) (TenantData_Sharing_Configuration, error) {
	return _Tenant.Contract.Read(&_Tenant.CallOpts, _id)
}

// Create is a paid mutator transaction binding the contract method 0x86ed7f81.
//
// Solidity: function create((string,string,string,string,string[],((bool,string,string,(string,uint256)),string,string,string,string,bool,bool,bool),(string[],string[],string[],string[],string[],uint256),(string,string[])[]) _data_Sharing_Configuration) returns()
func (_Tenant *TenantTransactor) Create(opts *bind.TransactOpts, _data_Sharing_Configuration TenantData_Sharing_Configuration) (*types.Transaction, error) {
	return _Tenant.contract.Transact(opts, "create", _data_Sharing_Configuration)
}

// Create is a paid mutator transaction binding the contract method 0x86ed7f81.
//
// Solidity: function create((string,string,string,string,string[],((bool,string,string,(string,uint256)),string,string,string,string,bool,bool,bool),(string[],string[],string[],string[],string[],uint256),(string,string[])[]) _data_Sharing_Configuration) returns()
func (_Tenant *TenantSession) Create(_data_Sharing_Configuration TenantData_Sharing_Configuration) (*types.Transaction, error) {
	return _Tenant.Contract.Create(&_Tenant.TransactOpts, _data_Sharing_Configuration)
}

// Create is a paid mutator transaction binding the contract method 0x86ed7f81.
//
// Solidity: function create((string,string,string,string,string[],((bool,string,string,(string,uint256)),string,string,string,string,bool,bool,bool),(string[],string[],string[],string[],string[],uint256),(string,string[])[]) _data_Sharing_Configuration) returns()
func (_Tenant *TenantTransactorSession) Create(_data_Sharing_Configuration TenantData_Sharing_Configuration) (*types.Transaction, error) {
	return _Tenant.Contract.Create(&_Tenant.TransactOpts, _data_Sharing_Configuration)
}
