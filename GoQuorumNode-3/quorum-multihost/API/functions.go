package main

import (
    "encoding/base64"
    "encoding/json"
	"encoding/hex"
    "crypto/cipher"
    "crypto/aes"
    "crypto/tls"
    "crypto/x509"
    "net/http"
    "net/url"
    "strings"
    "context"
    "time"
    "log"
    "errors"
    "io/ioutil"
    
    "github.com/ethereum/go-ethereum/accounts/abi/bind"
    "github.com/ethereum/go-ethereum/crypto"
    "github.com/ethereum/go-ethereum/common"
    "github.com/ethereum/go-ethereum/rpc"
    
    "golang.org/x/oauth2"
    "golang.org/x/oauth2/clientcredentials"
    
)

// connect function establish a HTTPS connection with the goQuorum Node.
func connectToGoQuorumNode(url string, rootCA string) (*rpc.Client) {
    cert, err := ioutil.ReadFile(rootCA)
    if err != nil {
		println("Couldn't load file: ", err.Error())
    }
    
    certPool := x509.NewCertPool()
    certPool.AppendCertsFromPEM(cert)
    println("RootCA loaded")
         
    httpClient := &http.Client{
    	Timeout:   5 * time.Second,
    	Transport: &http.Transport{
    		IdleConnTimeout: 10 * time.Second,
    		TLSClientConfig: &tls.Config{
    			RootCAs: certPool,
    		},
    	},
    }
    
    client, err := rpc.DialHTTPWithClient(url, httpClient)
    if err != nil {
		println("Could not connect to goQuorum Node: ", err.Error())
    }
    return client
}

// requestHydraToken functin requests a refreshed access token from the Hydra server.
func requestHydraToken(tokenURL string, clientId string, clientSecret string, audience string, rootCA string) (string, error) {
	ctx := context.Background()
	
    hydraCert, err := ioutil.ReadFile(rootCA)
	if err != nil {
		println("Couldn't load file: ", err.Error())
    }
    
    hydraCertPool := x509.NewCertPool()
    hydraCertPool.AppendCertsFromPEM(hydraCert)
    println("Hydra RootCA loaded")
         
    httpHydraClient := &http.Client{
    	Timeout:   5 * time.Second,
    	Transport: &http.Transport{
    		IdleConnTimeout: 10 * time.Second,
    		TLSClientConfig: &tls.Config{
    			RootCAs: hydraCertPool,
    		},
    	},
    }
    	
    ctx = context.WithValue(ctx, oauth2.HTTPClient, httpHydraClient)
    	
    conf := &clientcredentials.Config{
    	TokenURL: tokenURL,
    	ClientID: clientId,
    	ClientSecret: clientSecret,
    	Scopes: []string{"rpc://rpc_modules", "rpc://quorumExtension_*", "rpc://eth_*", "psi://" + clientId + "?self.eoa=0x0&node.eoa=0x0"},
    	EndpointParams: url.Values{"audience": { audience }},
    }
        
    token ,err := conf.Token(ctx)
    if err != nil {
    	println("Couldn't refresh token", err.Error())
    	return "", err
    }

    return "Bearer " + token.AccessToken, err
}

// connectToTesseraThirdPartyNode functions connects to tessera ThirdParty node to facilitate private transactions.
func connectToTesseraThirdPartyNode(authClient *rpc.Client, tesseraThirdPartyEndpoint string, tesseraThirdPartyClientCert string, tesseraThirdPartyClientKey string, tesseraThirdPartyRootCA string) (*Client) {
	thirdPartyRootCA, err := ioutil.ReadFile(tesseraThirdPartyRootCA)
	if err != nil {
    	println("Couldn't load file: ", err.Error())
    }
    
    tesseraThirdPartyCertPool := x509.NewCertPool()
    tesseraThirdPartyCertPool.AppendCertsFromPEM(thirdPartyRootCA)
    println("ThirdParty RootCA loaded")
	 
    tesseraThirdPartyClient := &PrivateTransactionManagerDefaultClient{
    	rawurl: tesseraThirdPartyEndpoint,
    	httpClient: &http.Client{
    		Timeout:   5 * time.Second,
    		Transport: &http.Transport{
    			IdleConnTimeout: 10 * time.Second,
    			TLSClientConfig: &tls.Config{
    				RootCAs: tesseraThirdPartyCertPool,
    				GetClientCertificate: func(info *tls.CertificateRequestInfo) (certificate *tls.Certificate, e error) {
						c, err := tls.LoadX509KeyPair(ClientCertThirdPartyPath, ClientKeyThirdPartyPath)
						if err != nil {
							println("Error loading client key pair: ", err.Error())
							return nil, err
						}
						return &c, nil
					},
					// print information about the certificate received from server
					VerifyPeerCertificate: func(rawCerts [][]byte, chains [][]*x509.Certificate) error {
						if len(chains) > 0 {
							println("Verified certificate chain from peer:")
							for _, v := range chains {
								for i, cert := range v {
									println("  Cert: ", i)
									certificateInfo(cert)
								}
							}
						}
						return nil
					},
    			},
    		},
    	},
    }
    
    conn := NewClientWithPTM(authClient, tesseraThirdPartyClient)

    return conn
}

// CertificateInfo function extracts info from certificate
func certificateInfo(cert *x509.Certificate) {
	if cert.Subject.CommonName == cert.Issuer.CommonName {
		println("    Self-signed certificate: ", cert.Issuer.CommonName)
	}

	println("    Subject: ", cert.DNSNames)
	println("    Usage: ", cert.ExtKeyUsage)
	println("    Issued by: ", cert.Issuer.CommonName)
	println("    Issued by: ", cert.Issuer.SerialNumber)
}

//
func initPrivateTransaction(ethkeyPath string, keyPasswd string) (*bind.TransactOpts) {
	var files []string
    fileInfo, err := ioutil.ReadDir(ethkeyPath)
    if err != nil {
		println("Couldn't load directory contents: ", err.Error())
    }
    for _, file := range fileInfo {
    	files = append(files, file.Name())
    }

    ethKey, err := ioutil.ReadFile(ethkeyPath + "/" + files[0])
    if err != nil {
		println("Couldn't get the private key from file: ", err.Error())
    }

    tr, err := bind.NewTransactor(strings.NewReader(string(ethKey)), keyPasswd);
    if err != nil {
		println("Couldn't decrypt private key: ", err.Error())
    }
    return tr
}

func encryptAES(key, iv, text []byte) ([]byte, error) {
    newKey := []byte{}
    if len(key) < 32 {
        for i:=0; i < len(key); i++ {
            newKey = append(newKey, key[i])
        }
           
        for i:=len(key); i < 32; i++ {
            newKey = append(newKey, 0)
        }
    } else if len(key) == 32 {
        newKey = key
    } else {
		return nil, errors.New("Key must be 32 bytes length.")
    }
	block, err := aes.NewCipher(newKey)
	if err != nil {
		return nil, err
	}
	b := base64.StdEncoding.EncodeToString(text)
	ciphertext := make([]byte, aes.BlockSize+len(b))
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))
	return ciphertext, nil
}

func decryptAES(key, iv, text []byte) ([]byte, error) {
    newKey := []byte{}
    if len(key) < 32 {
        for i:=0; i < len(key); i++ {
            newKey = append(newKey, key[i])
        }
           
        for i:=len(key); i < 32; i++ {
            newKey = append(newKey, 0)
        }
    } else if len(key) == 32 {
        newKey = key
    } else {
		return nil, errors.New("Key must be 32 bytes length.")
    }
	block, err := aes.NewCipher(newKey)
	if err != nil {
		return nil, err
	}
	if len(text) < aes.BlockSize {
		return nil, errors.New("Ciphertext is too short")
	}
	text = text[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(text, text)
	data, err := base64.StdEncoding.DecodeString(string(text))
	if err != nil {
		return nil, err
	}
	return data, nil
}

func getTLSConfig(host, caCertFile string) *tls.Config {
	var caCert []byte
	var err error
	var caCertPool *x509.CertPool

	caCert, err = ioutil.ReadFile(caCertFile)
	if err != nil {
		log.Fatal("Error opening cert file", caCertFile, ", error ", err)
	}
	caCertPool = x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	return &tls.Config{
		ServerName: host,
		ClientAuth: tls.RequireAndVerifyClientCert,
		ClientCAs:  caCertPool,
		MinVersion: tls.VersionTLS12,
		CurvePreferences: []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}
}

func retrievePublicKey(tesseraClient *Client, contractAddr, clientId, clientSecret string) ([]byte, error) {
        pool, err := NewPublic(common.HexToAddress(contractAddr), tesseraClient)
    	if err != nil {
    		println(err.Error())
            return nil, err
    	}
    	
    	hashedClientId := crypto.Keccak512([]byte(clientId))
        iv := make([]byte, 16)
        copy(iv, hashedClientId)

    	encryptedHashedClientId, err := encryptAES([]byte(clientSecret), []byte(iv),[]byte(hashedClientId))
    	if err != nil {
    		println(err.Error())
            return nil, err
	    }
    	
    	isClientIdExists, err := pool.PublicCaller.IsExists(&bind.CallOpts{}, hex.EncodeToString(encryptedHashedClientId))
    	if err != nil {
    		println(err.Error())
            return nil, err
    	}
    	
    	if(!isClientIdExists){
    		err := errors.New("There are no structs available.")
    		return nil, err
    	}
    	
    	readKeyFromPool, err := pool.PublicCaller.Read(&bind.CallOpts{}, hex.EncodeToString(encryptedHashedClientId)) 
    	if err != nil {
    		println(err.Error())
            return nil, err
    	}
    	
    	output, err := json.Marshal(readKeyFromPool)
    	if err != nil {
    		println(err.Error())
            return nil, err
    	}
    	
    	var publicKey PublicKeys
    	err = json.Unmarshal([]byte(output), &publicKey)
    	if err != nil {
    		println(err.Error())
            return nil, err
    	}
    	
    	encryptedPublicKey, err := hex.DecodeString(publicKey.EncryptedPublicKey)
    	if err != nil {
    		println(err.Error())
            return nil, err
	    }
	    
    	decryptedPublicKey, err := decryptAES([]byte(clientSecret), []byte(iv), []byte(encryptedPublicKey))
    	if err != nil {
    		println(err.Error())
            return nil, err
	    }
	    
	    return decryptedPublicKey, nil
}

func createAsset(tesseraClient *Client, contractAddr string, tenant TenantData_Sharing_Configuration, trOpts *bind.TransactOpts) (error) {
    asset, err := NewAsset(common.HexToAddress(contractAddr), tesseraClient)
   	if err != nil {
   	    println(err.Error())
        return err
   	}
   	
   	isAssetExists, err := asset.AssetCaller.IsExists(&bind.CallOpts{}, string(tenant.DatasetID))
    if err != nil {
   	   	println(err.Error())
        return err
   	}
    	
   	if(isAssetExists){
   	    err := errors.New("DatasetID is already exists.")
        return err
   	}
   	    
   	assetConfiguration := AssetAsset_Configuration_Public{
   	    DatasetID: tenant.DatasetID,
	    Configuration: AssetConfiguration{
	        Price: tenant.Configuration.Price,
	        LicenseType: tenant.Configuration.LicenseType,
	        StandardLicense: tenant.Configuration.StandardLicense,
	        OwnLicense: tenant.Configuration.OwnLicense,
	    },
	    Policy: AssetPolicy{
	     	Sector: tenant.Policy.Sector,
	        Continent: tenant.Policy.Continent,
	        Country: tenant.Policy.Country,
	        Type: tenant.Policy.Type,
	        Size: tenant.Policy.Size,
	    },
   	}

   	_, err = asset.AssetTransactor.Create(trOpts, assetConfiguration)
   	if err != nil {
   	    println(err.Error())
        return err
   	}
   	return nil
}
