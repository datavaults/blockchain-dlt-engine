#!/bin/bash

set -u
set -e

PASS="\033[0;32m"
NC="\033[0m"


DDIR="${PWD}"

echo -e "${PASS}[*] Searching whether OpenSSL is installed${NC}"
set +e
OpenSSLPath=`which openssl`
set -e
OPENSSL=
if [[ ! -z "$OpenSSLPath" ]]; then
    echo -e "${PASS}[*] OpenSSL Found${NC}"
    echo -e "${PASS}[*] Checking the OpenSSL Version${NC}"
    if [[ `$OpenSSLPath version` == "OpenSSL 1.1.1k  25 Mar 2021" ]]; then
        echo -e "${PASS}[*] OpenSSL has the latest $($OpenSSLPath version) version${NC}"
        OPENSSL=`dirname $OpenSSLPath`
    fi
else
    echo -e "${ERROR}[x] OPENSSL is not installed. Exited...${NC}"
    exit
fi


echo -e "${PASS}[*] Generating Certificate Authority (CA) for Quorum Node${NC}"
mkdir -p ${DDIR}/tls
pushd ${DDIR}/tls
        echo "GoQuorumPassword" > password.enc
        $OPENSSL/openssl genpkey -out key.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
        $OPENSSL/openssl req -new -x509 -nodes -key key.pem -sha256 -days 1024 -out cert.pem --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorumCA/CN=goquorum.datavaults.com" -addext "subjectAltName = IP.1:192.168.3.181, IP.2:192.168.3.161, IP.3:192.168.3.182" -passin file:password.enc
        $OPENSSL/openssl ec -in key.pem -out keyUnlocked.pem -passin file:password.enc
popd
