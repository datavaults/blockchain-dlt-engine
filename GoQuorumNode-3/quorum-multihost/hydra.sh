#!/bin/bash
set -u
set -e

currentDir=`pwd`

function CA() {
 #TODO:: add input for type of EC key and certificate subject options
 echo -e "${PASS}[*] Generating Certificate Authority (CA)${NC}"
 local DDIR="${currentDir}/PostgreSQL/"
 mkdir -p ${DDIR}/CA
 pushd ${DDIR}/CA
   echo "CApassword" > password.enc
   $OPENSSL/openssl genpkey -out CAKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
   $OPENSSL/openssl req -new -x509 -nodes -key CAKey.pem -sha256 -days 1024 -out CAcert.pem --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorumPostgresqlCA/OU=DataVaultsPostgresqlCA/CN=localhost" -addext "subjectAltName = DNS:localhost, IP:127.0.0.1" -passin file:password.enc
 popd
}

function TLS() {
 local TLSDIR=$1
 local NODE=$2
 
 #TODO:: add input for type of EC key and certificate subject options
 local CADIR="${currentDir}/PostgreSQL/CA"
 mkdir -p ${TLSDIR}/server ${TLSDIR}/client
 cp ${CADIR}/CAcert.pem ${TLSDIR}/server
 cp ${CADIR}/CAcert.pem ${TLSDIR}/client
       
 echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Server${NC}"
 pushd ${TLSDIR}/server
    echo "${NODE}ServerTLSPassword" > password.enc
    echo "subjectAltName = DNS:postgres_db, IP:127.0.0.1" > server.ext
    $OPENSSL/openssl genpkey -out serverKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc 
    $OPENSSL/openssl req -new -key serverKey.pem -sha256 -days 1024 -out server.csr --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorum/OU=PostgreSQL/CN=postgres_db" -passin file:password.enc 
    $OPENSSL/openssl x509 -req -in server.csr -CA ${TLSDIR}/server/CAcert.pem -CAkey ${CADIR}/CAKey.pem -CAcreateserial -out serverCert.pem -extfile server.ext -passin file:${CADIR}/password.enc 
    rm *.csr *.srl
    $OPENSSL/openssl pkcs8 -topk8 -inform pem -in serverKey.pem -passin file:password.enc -outform pem -nocrypt -out serversKey.pem
    echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Server is Finished${NC}"
 popd
      
 echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Client${NC}"
 pushd ${TLSDIR}/client
    echo "${NODE}ClientTLSPassword" > password.enc
    echo "subjectAltName = DNS:postgres_db, IP:127.0.0.1" > client.ext
    $OPENSSL/openssl genpkey -out clientKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
    $OPENSSL/openssl req -new -key clientKey.pem -sha256 -days 1024 -out client.csr --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorum/OU=PostgreSQL/CN=postgres" -passin file:password.enc
    $OPENSSL/openssl x509 -req -in client.csr -CA ${TLSDIR}/client/CAcert.pem -CAkey ${CADIR}/CAKey.pem -CAcreateserial -out clientCert.pem -extfile client.ext -passin file:${CADIR}/password.enc 
    rm *.csr *.srl
    $OPENSSL/openssl pkcs8 -topk8 -inform pem -in clientKey.pem -passin file:password.enc -outform pem -nocrypt -out clientsKey.pem
    echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Client is Finished${NC}"
 popd
}

function sslconfig() {
 local DOCKERFILE_PATH=$1
 mkdir -p ${DOCKERFILE_PATH}
 pushd ${DOCKERFILE_PATH}
  echo -n "
#!/bin/bash

rm /var/lib/postgresql/data/postgresql.conf
rm /var/lib/postgresql/data/pg_hba.conf

cat << EOF > /var/lib/postgresql/data/postgresql.conf
listen_addresses = '*'
port = 5432
max_connections = 20
# memory
shared_buffers = 128MB
temp_buffers = 8MB
work_mem = 4MB
# WAL / replication
wal_level = replica
max_wal_senders = 3
password_encryption = 'scram-sha-256'
	
# here are the SSL specific settings
ssl = on # this enables SSL
ssl_cert_file = '/var/lib/postgresql/serverCert.pem' # this specifies the server certificacte
ssl_key_file = '/var/lib/postgresql/serversKey.pem' # this specifies the server private key
ssl_ca_file = '/var/lib/postgresql/CAcert.pem' # this specific which CA certificate to trust
EOF
 
cat << EOF > /var/lib/postgresql/data/pg_hba.conf
hostssl all postgres ::/0 cert clientcert=1
hostssl all postgres 0.0.0.0/0 cert clientcert=1

hostssl all all ::/0 cert clientcert=1
hostssl all all 0.0.0.0/0 cert clientcert=1
EOF" >> ssl-config.sh
 popd
}

function prepareDockerfile_TLS() {
 local POSTGRESQL_TLSDIR=$1
 local DOCKERFILE_PATH=$2
 
 mkdir -p ${DOCKERFILE_PATH}/tls
 cp ${POSTGRESQL_TLSDIR}/server/serverCert.pem ${DOCKERFILE_PATH}/tls
 cp ${POSTGRESQL_TLSDIR}/server/serversKey.pem ${DOCKERFILE_PATH}/tls
 cp ${POSTGRESQL_TLSDIR}/server/CAcert.pem ${DOCKERFILE_PATH}/tls
}

function prepareHydraClient_TLS() {
 local POSTGRESQL_TLSDIR=$1
 local HYDRA_PATH=$2
 
 mkdir -p ${HYDRA_PATH}/client
 cp ${POSTGRESQL_TLSDIR}/client/clientCert.pem ${HYDRA_PATH}/client
 cp ${POSTGRESQL_TLSDIR}/client/clientsKey.pem ${HYDRA_PATH}/client
 cp ${POSTGRESQL_TLSDIR}/client/CAcert.pem ${HYDRA_PATH}/client
}

function createDockerfile() {
 local DOCKERFILE_PATH=$1
 pushd ${DOCKERFILE_PATH}
  echo -n '
# This Dockerfile contains the image specification of our database
FROM postgres:13-alpine

COPY ./tls/serversKey.pem /var/lib/postgresql
COPY ./tls/serverCert.pem /var/lib/postgresql

COPY ./tls/CAcert.pem /var/lib/postgresql

COPY ./ssl-config.sh /usr/local/bin

RUN chown 0:70 /var/lib/postgresql/serversKey.pem && chmod 640 /var/lib/postgresql/serversKey.pem
RUN chown 0:70 /var/lib/postgresql/serverCert.pem && chmod 640 /var/lib/postgresql/serverCert.pem

RUN chown 0:70 /var/lib/postgresql/CAcert.pem && chmod 640 /var/lib/postgresql/CAcert.pem

ENTRYPOINT ["'"docker-entrypoint.sh"'"] 

CMD [ "'"-c"'", "'"ssl=on"'" , "'"-c"'", "'"ssl_cert_file=/var/lib/postgresql/serverCert.pem"'", "'"-c"'", "'"ssl_key_file=/var/lib/postgresql/serversKey.pem"'", "'"-c"'", "'"ssl_ca_file=/var/lib/postgresql/CAcert.pem"'" ]
' >> Dockerfile 
 popd
}

function build() {
 local DOCKERFILE_PATH=$1
 pushd ${DOCKERFILE_PATH}
   $DOCKER/docker build --rm -f Dockerfile -t postgres:ssl "."
 popd
}

function run() {
 local POSTGRESQL_DIR=$1
 $DOCKER/docker network create hydra-Network
 $DOCKER/docker run -d -p 5432:5432 --network hydra-Network --name postgres_db -e POSTGRES_USER=hydra -e POSTGRES_PASSWORD=HydraSecurePassword -e POSTGRES_DB=hydra postgres:ssl
 sleep 20
 $DOCKER/docker exec -it postgres_db bash /usr/local/bin/ssl-config.sh
 $DOCKER/docker logs -f postgres_db >> "${POSTGRESQL_DIR}/logs/postgresql.log" 2>&1 &  
}

function hydraSelfSignedTLS() {
 local DDIR=$1
 #TODO:: add input for type of EC key and certificate subject options
 echo -e "${PASS}[*] Generating Certificate Authority (CA) for Hydra${NC}"
 mkdir ${DDIR}
 pushd ${DDIR}
   echo "HydraPassword" > password.enc
   $OPENSSL/openssl genpkey -out key.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
   $OPENSSL/openssl req -new -x509 -nodes -key key.pem -sha256 -days 1024 -out cert.pem --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorumHydraCA/OU=QuorumHydraCA/CN=localhost" -addext "subjectAltName = DNS:localhost, IP:127.0.0.1" -passin file:password.enc
   $OPENSSL/openssl ec -in key.pem -out keyUnlocked.pem -passin file:password.enc
 popd
}

function hydra() {
 local HYDRA_PATH_CLIENT=$1
 local HYDRA_PATH_TLS=$2
 local HYDRA_PATH_LOGS=$3
 
 local SECRETS_SYSTEM=$(export LC_CTYPE=C; cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
 local DSN="postgres://hydra:HydraSecurePassword@postgres_db:5432/hydra?sslmode=verify-full&sslcert=/etc/ssl/certs/client.crt&sslkey=/etc/ssl/certs/client.key&sslrootcert=/etc/ssl/certs/ca.crt"
 local URLS_SELF_ISSUER="https://goquorum.com/oauth/"
 $DOCKER/docker run -it --rm --network hydra-Network -v ${HYDRA_PATH_CLIENT}/CAcert.pem:/etc/ssl/certs/ca.crt -v ${HYDRA_PATH_CLIENT}/clientCert.pem:/etc/ssl/certs/client.crt -v ${HYDRA_PATH_CLIENT}/clientsKey.pem:/etc/ssl/certs/client.key oryd/hydra:latest migrate sql --yes ${DSN}
 $DOCKER/docker run -d --name hydra --hostname hydra --network hydra-Network -p 4444:4444 -p 4445:4445 -e SECRETS_SYSTEM=${SECRETS_SYSTEM} -e DSN=${DSN} -e URLS_SELF_ISSUER=${URLS_SELF_ISSUER} -e STRATEGIES_ACCESS_TOKEN=jwt -e SERVE_TLS_CERT_BASE64="$(base64 -i ${HYDRA_PATH_TLS}/cert.pem)" -e SERVE_TLS_KEY_BASE64="$(base64 -i ${HYDRA_PATH_TLS}/keyUnlocked.pem)" -v ${HYDRA_PATH_CLIENT}/CAcert.pem:/etc/ssl/certs/ca.crt -v ${HYDRA_PATH_CLIENT}/clientCert.pem:/etc/ssl/certs/client.crt -v ${HYDRA_PATH_CLIENT}/clientsKey.pem:/etc/ssl/certs/client.key oryd/hydra:latest serve all
 $DOCKER/docker logs -f hydra >> "${HYDRA_PATH_LOGS}/hydra.log" 2>&1 &  
}

################################################################################################
################################################################################################
################################################################################################
function usage() {
  echo ""
  echo "Usage:"
  echo "    $0 [--tls]"
  echo ""
  echo "Where:"
  echo "    --tls enables the TLS between the POSTGRESQL and HYDRA (default = $tls)"
  echo ""
  exit -1
}

PASS="\033[0;32m"
ERROR="\033[0;31m"
WARNING="\033[0;33m"
NC="\033[0m" # No Color

tls=OFF

while (( "$#" )); do
    case "$1" in
        --tls)
           tls=STRICT
           shift
           ;;
        --help)
           shift
           usage
           ;;
        *)
           echo -e "${ERROR}Error: Unsupported command line parameter $1${NC}"
           usage
           ;;
    esac
done

if [ "$tls" == "STRICT" ]; then
   echo -e "${PASS}[*] Searching whether OpenSSL is installed${NC}"
   set +e
   OpenSSLPath=`which openssl`
   set -e
   OPENSSL=
   if [[ ! -z "$OpenSSLPath" ]]; then
      echo -e "${PASS}[*] OpenSSL Found${NC}"
      echo -e "${PASS}[*] Checking the OpenSSL Version${NC}"
      if [[ `$OpenSSLPath version` == "OpenSSL 1.1.1k  25 Mar 2021" ]]; then
         echo -e "${PASS}[*] OpenSSL has the latest $($OpenSSLPath version) version${NC}"
         OPENSSL=`dirname $OpenSSLPath`
      fi
   else
      echo -e "${ERROR}[x] OPENSSL is not installed. Exited...${NC}"
      exit
   fi
   
   echo -e "${PASS}[*] Searching whether Docker is installed${NC}"
   set +e
   DockerPath=`which docker`
   set -e
   DOCKER=
   if [[ ! -z "$DockerPath" ]]; then
      echo -e "${PASS}[*] Docker Found${NC}"
      DOCKER=`dirname $DockerPath`
   else
      echo -e "${EROR}[x] Docker Not Installed. Exited...${NC}"
      exit
   fi
else
   echo -e "${ERROR}[x] This scripts operates only when GoQuorum Node JSON-RPC security is enabled.${NC}"
   exit
fi

echo -e "${PASS}[*] Cleaning up temporary data directories${NC}"
rm -rf PostgreSQL/

echo -e "${PASS}[*] Creating PostgreSQL CA ${NC}"
CA

echo -e "${PASS}[*] Creating PostgreSQL TLS ${NC}"
POSTGRESQL_DIR="${currentDir}/PostgreSQL"
POSTGRESQL_TLSDIR="${POSTGRESQL_DIR}/tls"
TLS ${POSTGRESQL_TLSDIR} PostgreSQL

echo -e "${PASS}[*] Creating PostgreSQL SSL configuration file ${NC}"
DOCKERFILE_PATH="${POSTGRESQL_DIR}/Dockerfile"
sslconfig ${DOCKERFILE_PATH}
prepareDockerfile_TLS ${POSTGRESQL_TLSDIR} ${DOCKERFILE_PATH}

echo -e "${PASS}[*] Creating Hydra TLS client file ${NC}"
HYDRA_PATH="${currentDir}/Hydra"
prepareHydraClient_TLS ${POSTGRESQL_TLSDIR} ${HYDRA_PATH}

echo -e "${PASS}[*] Creating PostgreSQL Dockerfile ${NC}"
createDockerfile ${DOCKERFILE_PATH}

echo -e "${PASS}[*] Building PostgreSQL Dockerfile ${NC}"
build ${DOCKERFILE_PATH}

echo -e "${PASS}[*] Running PostgreSQL Dockerfile ${NC}"
mkdir -p ${POSTGRESQL_DIR}/logs
run ${POSTGRESQL_DIR}

echo -e "${PASS}[*] Running Hydra ${NC}"
HYDRA_PATH_CLIENT="${HYDRA_PATH}/client"
HYDRA_PATH_TLS="${HYDRA_PATH}/tls"
HYDRA_PATH_LOGS="${HYDRA_PATH}/logs"
mkdir -p ${HYDRA_PATH_LOGS}
hydraSelfSignedTLS ${HYDRA_PATH_TLS}
hydra ${HYDRA_PATH_CLIENT} ${HYDRA_PATH_TLS} ${HYDRA_PATH_LOGS}











