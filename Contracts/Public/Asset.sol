/*
    @Author: George Misiakoulis (Ubitech)
*/
/*
    "SPDX-License-Identifier: UNLICENSED"
*/
pragma solidity ^0.7.6;
pragma experimental ABIEncoderV2;

contract Asset {

    struct Asset_Configuration_Public {
        string DatasetID;
		Configuration configuration;
		Policy policy;
    }
    
    struct Configuration {
        string Price;
        string LicenseType;
        string StandardLicense;
        string OwnLicense;
    }
    
    struct Policy {
    	string[] Sector;
    	string[] Continent;
    	string[] Country;
    	string[] Type;
    	string[] Size;
    }

    uint size = 0;
    mapping(uint => Asset_Configuration_Public) private asset_Configuration_Public;

    function _isExists(string memory _id) public view returns (bool) {
        uint j=0;
        for (j=0; j<size; j++){
            if(keccak256(bytes(asset_Configuration_Public[j].DatasetID)) == keccak256(bytes(_id))){
                return true;
            }
        }
        return false;
    }
    
    //Create
    function create(Asset_Configuration_Public memory _asset_Configuration_Public) public {

        Configuration memory _configuration = Configuration({
            Price: _asset_Configuration_Public.configuration.Price,
            LicenseType: _asset_Configuration_Public.configuration.LicenseType,
            StandardLicense: _asset_Configuration_Public.configuration.StandardLicense,
            OwnLicense: _asset_Configuration_Public.configuration.OwnLicense
        });
        
        Policy memory _policy = Policy({
			Sector: _asset_Configuration_Public.policy.Sector,
			Continent: _asset_Configuration_Public.policy.Continent,
			Country: _asset_Configuration_Public.policy.Country,
			Type: _asset_Configuration_Public.policy.Type,
			Size: _asset_Configuration_Public.policy.Size
        });
        
        asset_Configuration_Public[size] = Asset_Configuration_Public({
        	DatasetID: _asset_Configuration_Public.DatasetID,
        	configuration: _configuration,
        	policy: _policy
        });

      size++;
    }
    
    //Read
    function read(string memory _id) public view returns (Asset_Configuration_Public memory) {
        uint index = 0;
        uint j=0;
        for (j=0; j<size; j++){
            if(keccak256(bytes(asset_Configuration_Public[j].DatasetID)) == keccak256(bytes(_id))){
                index=j;
                break;
            }
        }
        return asset_Configuration_Public[index];
    }
    
    //ReadAll
    function readAll() public view returns (Asset_Configuration_Public[] memory) {
       Asset_Configuration_Public[] memory _dataToBeReturned = new Asset_Configuration_Public[](size);
       for (uint j=0; j<size; j++){
          _dataToBeReturned[j] = asset_Configuration_Public[j];
        }
       return _dataToBeReturned;
    }
          
}
