/*
    @Author: George Misiakoulis (Ubitech)
*/
/*
    "SPDX-License-Identifier: UNLICENSED"
*/
pragma solidity ^0.7.6;
pragma experimental ABIEncoderV2;

contract Data_Trading {

    struct Configuration {
        string DatasetID;
        string DataSeekerID;
        string Price;
        string LicenseType;
        string StandardLicense;
    }
    
    uint size = 0;
    mapping(uint => Configuration) private configuration;

    function _isDatasetIDExists(string memory _datasetID) public view returns (bool) {
        uint j=0;
        for (j=0; j<size; j++){
            if(keccak256(bytes(configuration[j].DatasetID)) == keccak256(bytes(_datasetID))){
                return true;
            }
        }
        return false;
    }
    
    function _isDataSeekerIDExists(string memory _dataSeekerID) public view returns (bool) {
        uint j=0;
        for (j=0; j<size; j++){
            if(keccak256(bytes(configuration[j].DataSeekerID)) == keccak256(bytes(_dataSeekerID))){
                return true;
            }
        }
        return false;
    }
    
    //Create
    function create(Configuration memory _configuration) public {
        
        configuration[size] = Configuration({
        	DatasetID: _configuration.DatasetID,
            DataSeekerID: _configuration.DataSeekerID,
            Price: _configuration.Price,
            LicenseType: _configuration.LicenseType,
            StandardLicense: _configuration.StandardLicense
        });

      size++;
    }
    
    //Read Dataset
    function readDataset(string memory _datasetID) public view returns (Configuration memory) {
        uint index = 0;
        uint j=0;
        for (j=0; j<size; j++){
            if(keccak256(bytes(configuration[j].DatasetID)) == keccak256(bytes(_datasetID))){
                index=j;
                break;
            }
        }
        return configuration[index];
    }
    
    //Read DataSeeker
    function readDataSeeker(string memory _dataSeekerID) public view returns (Configuration memory) {
        uint index = 0;
        uint j=0;
        for (j=0; j<size; j++){
            if(keccak256(bytes(configuration[j].DataSeekerID)) == keccak256(bytes(_dataSeekerID))){
                index=j;
                break;
            }
        }
        return configuration[index];
    }
          
}
