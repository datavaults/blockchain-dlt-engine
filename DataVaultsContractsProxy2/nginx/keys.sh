#!/bin/bash
set -u
set -e
currentDir=${PWD}

function checking() {
 echo -e "${PASS}[*] Searching whether OpenSSL is installed${NC}"
 set +e
 OpenSSLPath=`which openssl`
 set -e
 OPENSSL=
 if [[ ! -z "$OpenSSLPath" ]]; then
    echo -e "${PASS}[*] OpenSSL Found${NC}"
    echo -e "${PASS}[*] Checking the OpenSSL Version${NC}"
    if [[ `$OpenSSLPath version` == "OpenSSL 1.1.1k  25 Mar 2021" ]]; then
       echo -e "${PASS}[*] OpenSSL has the latest $($OpenSSLPath version) version${NC}"
       OPENSSL=`dirname $OpenSSLPath`
    fi
 else
    echo -e "${ERROR}[x] OPENSSL is not installed. Exited...${NC}"
    exit
 fi
 
 echo -e "${PASS}[*] Searching whether Keytool is installed${NC}"
 set +e
 KeytoolPath=`which keytool`
 set -e
 KEYTOOL=
 if [[ ! -z "$KeytoolPath" ]]; then
    echo -e "${PASS}[*] Keytool Found.${NC}"
    KEYTOOL=`dirname $KeytoolPath`
 else
    echo -e "${ERROR}[x] Keytool is not installed. Exited...${NC}"
    exit
 fi
}

function CA() {
 IP=$1
 
 #TODO:: add input for type of EC key and certificate subject options
 echo -e "${PASS}[*] Generating Certificate Authority (CA)${NC}"
 DDIR="${currentDir}/"
 mkdir -p ${DDIR}/CA
 pushd ${DDIR}/CA
   echo "CApassword" > password.enc
   $OPENSSL/openssl genpkey -out CAKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
   $OPENSSL/openssl req -new -x509 -nodes -key CAKey.pem -sha256 -days 1024 -out CAcert.pem --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorumCA/OU=DataVaultsCA/CN=${IP}" -addext "subjectAltName = DNS:${IP}, IP:${IP}" -passin file:password.enc
 popd
}

function TLS() {
 TLSDIR=$1
 NODE=$2
 IP=$3
 
 #TODO:: add input for type of EC key and certificate subject options
 CADIR="${currentDir}/CA"
 mkdir -p ${TLSDIR}/server ${TLSDIR}/client
 cp ${CADIR}/CAcert.pem ${TLSDIR}/server
 cp ${CADIR}/CAcert.pem ${TLSDIR}/client
       
 echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Server${NC}"
 pushd ${TLSDIR}/server
    echo "${NODE}ServerTLSPassword" > password.enc
    echo "subjectAltName = DNS:${IP}, IP:${IP}" > server.ext
    $OPENSSL/openssl genpkey -out serverKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc 
    $OPENSSL/openssl req -new -key serverKey.pem -sha256 -out server.csr --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorum/OU=DataVaults/CN=${IP}" -passin file:password.enc 
    $OPENSSL/openssl x509 -req -days 1024 -in server.csr -CA ${TLSDIR}/server/CAcert.pem -CAkey ${CADIR}/CAKey.pem -CAcreateserial -out serverCert.pem -extfile server.ext -passin file:${CADIR}/password.enc 
    rm *.csr *.srl
    $OPENSSL/openssl pkcs8 -topk8 -inform pem -in serverKey.pem -passin file:password.enc -outform pem -nocrypt -out serversKey.pem
    $OPENSSL/openssl pkcs12 -export -in serverCert.pem -inkey serversKey.pem -out serverKeystore.p12 -name serverKeystore -CAfile CAcert.pem -caname CA -password file:password.enc
    $KEYTOOL/keytool -importkeystore -deststorepass $(cat password.enc) -destkeypass $(cat password.enc) -destkeystore serverKeystore.jks -srckeystore serverKeystore.p12 -srcstoretype PKCS12 -srcstorepass $(cat password.enc) -alias serverKeystore
    echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Server is Finished${NC}"
 popd
      
 echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Client${NC}"
 pushd ${TLSDIR}/client
    echo "${NODE}ClientTLSPassword" > password.enc
    echo "subjectAltName = DNS:${IP}, IP:${IP}" > client.ext
    $OPENSSL/openssl genpkey -out clientKey.pem -algorithm EC -pkeyopt ec_paramgen_curve:P-521 -aes256 -pass file:password.enc
    $OPENSSL/openssl req -new -key clientKey.pem -sha256 -out client.csr --subj "/C=GR/ST=Athens/L=Athens/O=GoQuorum/OU=DataVaults/CN=${IP}" -passin file:password.enc
    $OPENSSL/openssl x509 -req -days 1024 -in client.csr -CA ${TLSDIR}/client/CAcert.pem -CAkey ${CADIR}/CAKey.pem -CAcreateserial -out clientCert.pem -extfile client.ext -passin file:${CADIR}/password.enc 
    rm *.csr *.srl
    $OPENSSL/openssl pkcs8 -topk8 -inform pem -in clientKey.pem -passin file:password.enc -outform pem -nocrypt -out clientsKey.pem
    $OPENSSL/openssl pkcs12 -export -in clientCert.pem -inkey clientsKey.pem -out clientKeystore.p12 -name clientKeystore -CAfile CAcert.pem -caname CA -password file:password.enc
    $KEYTOOL/keytool -importkeystore -deststorepass $(cat password.enc) -destkeypass $(cat password.enc) -destkeystore clientKeystore.jks -srckeystore clientKeystore.p12 -srcstoretype PKCS12 -srcstorepass $(cat password.enc) -alias clientKeystore
    echo -e "${PASS}[*] Generating TLS Certificates for ${NODE} Client is Finished${NC}"
 popd
}

PASS="\033[0;32m"
ERROR="\033[0;31m"
WARNING="\033[0;33m"
NC="\033[0m" # No Color


checking
DDIR="${currentDir}/"

SERVER_IP=$1
CA ${SERVER_IP}

TLS ${DDIR} NGINX ${SERVER_IP}

