/*
    @Author: George Misiakoulis (Ubitech)
*/
/*
    "SPDX-License-Identifier: UNLICENSED"
*/
pragma solidity ^0.7.6;
pragma experimental ABIEncoderV2;

contract Public {

    struct Keys {
        string EncryptedHashedClientID;
        string EncryptedPublicKey;
    }

    uint size = 0;
    mapping(uint => Keys) private keys;

    function _isExists(string memory _encryptedHashedClientID) public view returns (bool) {
        uint j=0;
        for (j=0; j<size; j++){
            if(keccak256(bytes(keys[j].EncryptedHashedClientID)) == keccak256(bytes(_encryptedHashedClientID))){
                return true;
            }
        }
        return false;
    }
    
    //Create
    function create(Keys memory _keys) public {
        
        keys[size] = Keys({
        	EncryptedHashedClientID: _keys.EncryptedHashedClientID,
        	EncryptedPublicKey: _keys.EncryptedPublicKey
        });

      size++;
    }
    
    //Read
    function read(string memory _encryptedHashedClientID) public view returns (Keys memory) {
        uint index = 0;
        uint j=0;
        for (j=0; j<size; j++){
            if(keccak256(bytes(keys[j].EncryptedHashedClientID)) == keccak256(bytes(_encryptedHashedClientID))){
                index=j;
                break;
            }
        }
        return keys[index];
    }
          
}
