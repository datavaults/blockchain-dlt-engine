/*
    @Author: George Misiakoulis (Ubitech)
*/
/*
    "SPDX-License-Identifier: UNLICENSED"
*/
pragma solidity ^0.7.6;
pragma experimental ABIEncoderV2;

contract Tenant {

    struct Data_Sharing_Configuration {
        string DatasetID;
        string Asset;
        string Name;
        string Description;
        string[] Keywords;
        Configuration configuration;
        Policy policy;
        ABE[] abe;
    }
    
    struct Configuration {
        Anonymisation anonymisation;
        string Price;
        string LicenseType;
        string StandardLicense;
        string OwnLicense;
        bool Encryption;
        bool Persona;
        bool UseTPM;
    }
    
    struct Anonymisation {
        bool Anonymise;
        string PseudoId;
        string ExistingPseudoId;
        Levels levels;
    }
    
    struct Levels {
        string LevelOfAnonymity;
        uint Value;
    }
    
    struct Policy {
    	string[] Sector;
    	string[] Continent;
    	string[] Country;
    	string[] Type;
    	string[] Size;
    	uint Reputation;
    }

	struct ABE {
    	string Key;
    	string[] ABEpolicy;
    }

    uint size = 0;
    mapping(uint => Data_Sharing_Configuration) private data_Sharing_Configuration;

    function _isExists(string memory _id) public view returns (bool) {
        uint j=0;
        for (j=0; j<size; j++){
            if(keccak256(bytes(data_Sharing_Configuration[j].DatasetID)) == keccak256(bytes(_id))){
                return true;
            }
        }
        return false;
    }
    
    //Create
    function create(Data_Sharing_Configuration memory _data_Sharing_Configuration) public {

        Levels memory _levels = Levels({
            LevelOfAnonymity: _data_Sharing_Configuration.configuration.anonymisation.levels.LevelOfAnonymity,
            Value: _data_Sharing_Configuration.configuration.anonymisation.levels.Value
        });
        
        Anonymisation memory _anonymisation = Anonymisation({
            Anonymise: _data_Sharing_Configuration.configuration.anonymisation.Anonymise,
            PseudoId: _data_Sharing_Configuration.configuration.anonymisation.PseudoId,
            ExistingPseudoId: _data_Sharing_Configuration.configuration.anonymisation.ExistingPseudoId,
            levels: _levels
        });
        
        Configuration memory _configuration = Configuration({
            anonymisation:  _anonymisation,
            Price: _data_Sharing_Configuration.configuration.Price,
            LicenseType: _data_Sharing_Configuration.configuration.LicenseType,
            StandardLicense: _data_Sharing_Configuration.configuration.StandardLicense,
            OwnLicense: _data_Sharing_Configuration.configuration.OwnLicense,
            Encryption: _data_Sharing_Configuration.configuration.Encryption,
            Persona: _data_Sharing_Configuration.configuration.Persona,
            UseTPM: _data_Sharing_Configuration.configuration.UseTPM
        });
        
        Policy memory _policy = Policy({
            Sector: _data_Sharing_Configuration.policy.Sector,
            Continent: _data_Sharing_Configuration.policy.Continent,
            Country: _data_Sharing_Configuration.policy.Country,
            Type: _data_Sharing_Configuration.policy.Type,
            Size: _data_Sharing_Configuration.policy.Size,
            Reputation: _data_Sharing_Configuration.policy.Reputation
        });
        
        data_Sharing_Configuration[size].DatasetID = _data_Sharing_Configuration.DatasetID;
		data_Sharing_Configuration[size].Asset = _data_Sharing_Configuration.Asset;
		data_Sharing_Configuration[size].Name = _data_Sharing_Configuration.Name;
		data_Sharing_Configuration[size].Description = _data_Sharing_Configuration.Description;
		data_Sharing_Configuration[size].Keywords =  _data_Sharing_Configuration.Keywords;
		data_Sharing_Configuration[size].configuration = _configuration;
		data_Sharing_Configuration[size].policy = _policy;
        
        uint _ABEsize = _data_Sharing_Configuration.abe.length;
        for (uint k=0; k < _ABEsize; k++) {
         	data_Sharing_Configuration[size].abe.push(ABE({
        		Key: _data_Sharing_Configuration.abe[k].Key,
        		ABEpolicy: _data_Sharing_Configuration.abe[k].ABEpolicy
        	}));
        }
      
      size++;
    }
    
    //Read
    function read(string memory _id) public view returns (Data_Sharing_Configuration memory) {
        uint index = 0;
        uint j=0;
        for (j=0; j<size; j++){
            if(keccak256(bytes(data_Sharing_Configuration[j].DatasetID)) == keccak256(bytes(_id))){
                index=j;
                break;
            }
        }
        return data_Sharing_Configuration[index];
    }   
}
