module API

go 1.16

replace (
	github.com/ethereum/go-ethereum => github.com/Consensys/quorum v1.2.2-0.20210518093622-1d7926a19a1e
	github.com/ethereum/go-ethereum/crypto/secp256k1 => github.com/ConsenSys/quorum/crypto/secp256k1 v0.0.0-20210518093622-1d7926a19a1e
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/ethereum/go-ethereum v1.10.3
)
