/*
	@Author: George Misiakoulis (Ubitech)
*/
package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"runtime"
	"time"
	"fmt"
	"log"
	

	"github.com/davecgh/go-spew/spew"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
    "github.com/ethereum/go-ethereum/common"
    "github.com/ethereum/go-ethereum/rpc"
)

var (
	port string = "8083"
	host string = "localhost"
	caCert string = "./CA/CAcert.pem"
	serverCert string = "./TLS/server/serverCert.pem"
	serverKey string = "./TLS/server/serverUnecryptedKey.pem"

    goQuorumNodeURL string = "https://localhost:22000/?PSI="
    tokenURL string = "https://localhost:4444/oauth2/token"
    audience string = "Node1"

    tesseraThirdPartyEndpoint string = "https://localhost:9081"
	
	RootCertificateGoQuorumNodePath string = "../nodes/node1/tls/cert.pem"
	RootCertificateHydraPath string = "../Hydra/tls/cert.pem"
	
	ethKeyPath string = "../nodes/node1/keystore"
	keyPassword string = "node1"
	
	RootCertificateThirdPartyPath string = "../nodes/tessera1/tls/ThirdParty/client/CAcert.pem"
	ClientCertThirdPartyPath      string = "../nodes/tessera1/tls/ThirdParty/client/clientCert.pem"
	ClientKeyThirdPartyPath       string = "../nodes/tessera1/tls/ThirdParty/client/clientsKey.pem"
)

//////////////////////////////////////////////////////////////////////////////////////////////////
type ClientDeployCreate struct {
	ClientId     	string
	ClientSecret 	string
	ContractAddress string
	DataInput		Data_TradingConfiguration
}

type ClientRead struct {
	ClientId     	string
	ClientSecret 	string
	ContractAddress string
	DataInput		string
}

func deployDataTrading(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientDeployCreate
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	} 
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		
		// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)

    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	defer authenticatedClient.Close()
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    
    	trOpts := initPrivateTransaction(ethKeyPath, keyPassword)

    	trOpts.GasLimit = 47000000
    	
    	publicAddr, _, _, err := DeployDataTrading(trOpts, tesseraClient)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
		
		w.Write([]byte("Contract Address is: " + publicAddr.Hex()))
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

func createDataTrading(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientDeployCreate
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    	} 
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		contractAddr := client.ContractAddress
		input := client.DataInput
		if input == (Data_TradingConfiguration{}) {
    		w.Write([]byte("DataInput must NOT be empty."))
    		return
    	}
		
		// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)
    	
    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	defer authenticatedClient.Close()
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    	
    	trOpts := initPrivateTransaction(ethKeyPath, keyPassword)

    	trOpts.GasLimit = 47000000

    	trading, err := NewDataTrading(common.HexToAddress(contractAddr), tesseraClient)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	/*isDatasetExists, err := trading.DataTradingCaller.IsDatasetIDExists(&bind.CallOpts{}, string(input.DatasetID))
    	spew.Dump(isDatasetExists)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	
    	if(isDatasetExists){
    		w.Write([]byte("DatasetID is already exists."))
    		return
    	}*/ 
   
    	_, err = trading.DataTradingTransactor.Create(trOpts, input)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
		w.Write([]byte("A new record is successfully added."))
		
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

func readDataTradingDataset(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientRead
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	} 
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		contractAddr := client.ContractAddress
		input := client.DataInput
		if input == "" {
    		w.Write([]byte("DataInput must NOT be empty."))
    		return
    	}

		// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)
    	
    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	defer authenticatedClient.Close()
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    	
    	trading, err := NewDataTrading(common.HexToAddress(contractAddr), tesseraClient)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	
    	isDatasetExists, err := trading.DataTradingCaller.IsDatasetIDExists(&bind.CallOpts{}, string(input))
    	spew.Dump(isDatasetExists)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	
    	if(!isDatasetExists){
    		w.Write([]byte("There are no structs available."))
    		return
    	}
    	
    	readDataset, err := trading.DataTradingCaller.ReadDataset(&bind.CallOpts{}, string(input)) 
    	if err != nil {
    		println(err.Error())
    	   	w.Write([]byte(err.Error()))
    	}
    	
    	output, err := json.Marshal(readDataset)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
		w.Write([]byte(output))
		
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}

func readDataTradingDataSeeker(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received %s request for host %s from IP address %s and X-FORWARDED-FOR %s", r.Method, r.Host, r.RemoteAddr, r.Header.Get("X-FORWARDED-FOR"))
	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			body = []byte(fmt.Sprintf("Error reading request body: %s", err))
		    w.Write([]byte(body))
		}

		var client ClientRead
	    dec := json.NewDecoder(strings.NewReader(string(body)))
	    err = dec.Decode(&client)
	    if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	} 
		clientId := client.ClientId
		clientSecret := client.ClientSecret
		contractAddr := client.ContractAddress
		input := client.DataInput
		if input == "" {
    		w.Write([]byte("DataInput must NOT be empty."))
    		return
    	}

		// Connect to goQuorum Node
    	goQuorumClient := connectToGoQuorumNode(goQuorumNodeURL + clientId, RootCertificateGoQuorumNodePath)
    	
    	// Refresh Token
    	var f rpc.HttpCredentialsProviderFunc = func(ctx context.Context) (string, error) {
    		var token string
    		token, err = requestHydraToken(tokenURL, clientId, clientSecret, audience, RootCertificateHydraPath)
    		if err != nil {
    	    	w.Write([]byte(err.Error()))
    		}
    		return token, nil
    	}
    
    	// Attach token to the headers
    	authenticatedClient := goQuorumClient.WithHTTPCredentials(f)
    	defer authenticatedClient.Close()
    	tesseraClient := connectToTesseraThirdPartyNode(authenticatedClient, tesseraThirdPartyEndpoint, ClientCertThirdPartyPath, ClientKeyThirdPartyPath, RootCertificateThirdPartyPath)
    	
    	trading, err := NewDataTrading(common.HexToAddress(contractAddr), tesseraClient)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	
    	isDataSeekerExists, err := trading.DataTradingCaller.IsDataSeekerIDExists(&bind.CallOpts{}, string(input))
    	spew.Dump(isDataSeekerExists)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
    	
    	if(!isDataSeekerExists){
    		w.Write([]byte("There are no structs available."))
    		return
    	}
    	
    	readDataSeeker, err := trading.DataTradingCaller.ReadDataSeeker(&bind.CallOpts{}, string(input)) 
    	if err != nil {
    		println(err.Error())
    	   	w.Write([]byte(err.Error()))
    	}
    	
    	output, err := json.Marshal(readDataSeeker)
    	if err != nil {
    		println(err.Error())
    		w.Write([]byte(err.Error()))
    	}
		w.Write([]byte(output))
		
	} else {
	 	w.Write([]byte("Only POST requests are allowed."))
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////
// Main function
func main() {
    log.SetFlags(log.Lshortfile)
    maxProcs := runtime.NumCPU()
    runtime.GOMAXPROCS(maxProcs)
    
    server := &http.Server{
		Addr:         ":" + port,
		ReadTimeout:  5 * time.Minute,
		WriteTimeout: 10 * time.Second,
		TLSConfig:    getTLSConfig(host, caCert),
	}

	http.HandleFunc("/deploy", deployDataTrading)
	http.HandleFunc("/create", createDataTrading)
	http.HandleFunc("/readDataset", readDataTradingDataset)
	http.HandleFunc("/readDataSeeker", readDataTradingDataSeeker)
	
	log.Printf("Starting HTTPS server on host %s and port %s", host, port)
	if err := server.ListenAndServeTLS(serverCert, serverKey); err != nil {
		log.Fatal(err)
	}
}
